var gulp = require('../gulp.js'),
	//gutil = require('gulp-util'),
	watch = require('gulp-watch'),
	rename = require('gulp-rename'),
	browserSync = require('browser-sync'),
	exec = require('child_process').exec;
;

gulp.task('server:start', function () {
	if (config.server.vorlon.enable) {
		exec(__dirname + '/../../node_modules/.bin/vorlon');
	}

	browserSync(config.server.browserSync);
});