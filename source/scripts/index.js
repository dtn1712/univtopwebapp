/**
 * Some in-house plugins/libraries
 */
require('./includes/jquery.adr-dropdown');
require('./includes/swig');

//temporarily expose Application to global for debugging purpose
var Application = require('./core/application');

/**
 * register modules
 */
Application.register('ajax', require('./modules/ajax'));

if ($('.adr.header').length) {
    Application.register('header', require('./modules/header'), data.header);
}

Application.register('footer', require('./modules/footer'));
Application.register('modal', require('./modules/modal'));
Application.register('window', require('./modules/window'));

if ($('.page-home').length) {
    Application.register('pageHome',require('./pages/home'));
}

if($('.page-contact').length){
    Application.register('pageContact', require('./pages/contact'));
}

if($('.page-questions').length){
    Application.register('pageQuestions', require('./pages/questions'));
}

Application.start();

window.UnivtopApplication = Application;