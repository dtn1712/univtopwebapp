'use strict';

var pageQuestions = function(sandbox){
    var _this = this;

    _this.handleFormSearch = function(){
        var $input = $('#form_ask_question__input');

        var inputMagicSuggest = $input.magicSuggest({
            data: [
                {
                    name: 'Question 1',
                    href: '/pages/contact-us.html'
                },
                {
                    name: 'Question 2',
                    href: '/pages/question.html'
                }
            ],
            hideTrigger: true,
            selectFirst: true,
            placeholder: '',
            renderer: function(data){
                return '<a href="'+data.href+'">'+data.name+'</a>';
            }
        });

        $(inputMagicSuggest).on('selectionchange', function(){
            var selectedItems = this.getSelection();
            if(selectedItems.length){
                var selectedItem = selectedItems[0];
                if(selectedItem.href){
                    window.location.assign(selectedItem.href);
                }
            }
        });
    }

    _this.handleTabs = function(){
        _this.templates.tab = multiline(function(){/*
         <div class="list questions">
         {% for question in questions %}
            <div class="item question" data-id="{{question.id}}" data-is-expanded="0">
                <div class="univtop card">
                    <div class="thumbnail">
                        <img src="{{question.user_post.avatar}}" alt="">
                        <a href="profile.html" class="meta name">{{question.user_post.first_name}} {{question.user_post.last_name}}</a>
                    </div>
                    <div class="content">
                        <div class="segment">
                            <div class="header">
                                <div class="actions">
                                    <a href="#" data-action="follow" data-question-id="{{question.id}}" data-is-followed="0">Follow</a>
                                </div>
                                <div class="meta time">{{question.create_date}}</div>
                                <div class="tags">
                                {% for tag in question.tags %}
                                    <a class="tag" data-id="{{tag.id}}">{{tag.value}}</a>
                                {% endfor %}
                                </div>
                            </div>

                            <div class="body">
                                <div class="summary">{{question.title}}</div>
                                <div class="extra text">
                                    {{question.content}}
                                    {% if question.latest_answer %}<a href="#" data-action="expand">more</a>{% endif %}
                                </div>
                            </div><!-- end .body -->

                            <div class="footer">
                                <a class="univtop label">
                                    <i class="univtop icon heart"></i>70
                                </a>

                                <a class="univtop label">
                                    <i class="univtop icon chat-bubble"></i>70
                                </a>
                            </div><!-- end .footer -->
                        </div>
                    </div>
                    {% if question.latest_answer %}
                    <div class="answers">
                        <div class="item">
                            <div class="univtop card">
                                <div class="thumbnail">
                                    <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="56" height="56">
                                    <a href="#" class="meta name">{{question.latest_answer.user_post_fullname}}</a>
                                </div>
                                <div class="content">
                                    <div class="segment">
                                        <div class="body">
                                            <div class="summary">{{question.latest_answer.content}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}

                    <form class="univtop form" data-form="reply" data-question-id="{{question.id}}">
                        <div class="inline fields">
                            <div class="field avatar">
                                <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="72" height="72"/>
                                <div class="meta name">YOU</div>
                            </div>
                            <div class="field input">
                                <div class="fields">
                                    <div class="field">
                                        <textarea name="content" placeholder="Add your answer"></textarea>
                                    </div>
                                    <div class="field text-right">
                                        <button type="submit" class="univtop button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        {% endfor %}
        </div>
        */console.log});

        var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_2__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                //load tab content
                //get data
                switch($target.data('loading-status')){
                    case 'success':
                        //do nothing
                        break;
                    case 'not-yet':
                    case 'error':
                    default:
                        var data = {"meta":{"limit":100,"next":null,"offset":0,"previous":null,"total_count":16},"objects":[{"content":"Hello world!! I am gonna make the most out of my life! yolo!!","create_date":"2016-03-10T18:45:22.632918","edit_date":null,"id":158,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUfffASdMkgV8J7S8Vhjgz6H559","tags":[{"id":52,"resource_uri":"/api/v1/tag/TGVUxs5X66u8fw4wUZG7FZYp328","unique_id":"TGVUxs5X66u8fw4wUZG7FZYp328","value":"yolo!!"}],"title":"\nWhat happens if I don't get selected after the first H1B raffle?","total_answer":0,"total_vote":0,"unique_id":"QUfffASdMkgV8J7S8Vhjgz6H559","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/10/lnhgiang91.jpg","first_name":"Chelsea","is_activated":false,"last_name":"Vo","resource_uri":"/api/v1/user/lnhgiang91","total_answers":0,"total_asked_questions":2,"total_following_questions":0,"username":"lnhgiang91","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":339,"occupation":"","resource_uri":"/api/v1/profile/339","user":"/api/v1/user/lnhgiang91"}}},{"content":"","create_date":"2016-02-28T21:22:23.616467","edit_date":null,"id":157,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"where did you go to school ?","create_date":"2016-02-29T02:35:22.253654","edit_date":null,"question":"QUPeQ5mUsLRaNaSHn9pmDiPv595","unique_id":"AN5qMMdHXYJKFq5JagFX4F4Q973","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUPeQ5mUsLRaNaSHn9pmDiPv595","tags":[],"title":"What is it like to go to an elite school in US ? #college","total_answer":2,"total_vote":4,"unique_id":"QUPeQ5mUsLRaNaSHn9pmDiPv595","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/hyhieu.jpg","first_name":"Hieu","is_activated":false,"last_name":"Pham","resource_uri":"/api/v1/user/hyhieu","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"hyhieu","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":550,"occupation":"","resource_uri":"/api/v1/profile/550","user":"/api/v1/user/hyhieu"}}},{"content":"","create_date":"2016-02-27T17:37:38.035665","edit_date":null,"id":156,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"you should prepare more","create_date":"2016-02-28T18:29:04.453979","edit_date":null,"question":"QUX88q25XKxfAAGrCSXEmQLD027","unique_id":"AN8ouQJM2UdrRWJpq4E53aym398","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUX88q25XKxfAAGrCSXEmQLD027","tags":[{"id":51,"resource_uri":"/api/v1/tag/TGkv8kWBrGVxVVZavkHSgRZp167","unique_id":"TGkv8kWBrGVxVVZavkHSgRZp167","value":"entrepreneurship"}],"title":"What should I do to prepare for creating new company since I am still in US school #entrepreneurship","total_answer":1,"total_vote":2,"unique_id":"QUX88q25XKxfAAGrCSXEmQLD027","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/26/teamunivtop.jpg","first_name":"Team","is_activated":false,"last_name":"Univtop","resource_uri":"/api/v1/user/teamunivtop","total_answers":6,"total_asked_questions":1,"total_following_questions":8,"username":"teamunivtop","userprofile":{"bio":"I am univtop star","facebook_id":null,"gender":"m","id":429,"occupation":"I am in California now","resource_uri":"/api/v1/profile/429","user":"/api/v1/user/teamunivtop"}}},{"content":"i am freshman want to apply for amazon internship","create_date":"2016-02-26T00:48:54.536816","edit_date":null,"id":155,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"I prepared with details side project, networking with school.","create_date":"2016-02-29T02:32:30.279928","edit_date":null,"question":"QUTAGTzTufXzGx4SFx7Sipmw770","unique_id":"ANuJSngKE7EDKoM9Y7kus7qc173","user_post_fullname":"Dang Nguyen","user_post_username":"dtn17121","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUTAGTzTufXzGx4SFx7Sipmw770","tags":[{"id":49,"resource_uri":"/api/v1/tag/TG9uMDKv3VubhqabCJ9Mn7cK549","unique_id":"TG9uMDKv3VubhqabCJ9Mn7cK549","value":"amazon"},{"id":50,"resource_uri":"/api/v1/tag/TG57UtGQiiM5NEj8ZGyFQzhn666","unique_id":"TG57UtGQiiM5NEj8ZGyFQzhn666","value":"dtn1712"}],"title":"how did you get your first job in #amazon #dtn1712","total_answer":2,"total_vote":3,"unique_id":"QUTAGTzTufXzGx4SFx7Sipmw770","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/vuthanhduc92.jpg","first_name":"Duc","is_activated":false,"last_name":"Vu","resource_uri":"/api/v1/user/vuthanhduc92","total_answers":9,"total_asked_questions":2,"total_following_questions":15,"username":"vuthanhduc92","userprofile":{"bio":"dreamer currently in bay area","facebook_id":null,"gender":"m","id":438,"occupation":"drexel university","resource_uri":"/api/v1/profile/438","user":"/api/v1/user/vuthanhduc92"}}},{"content":"Whats up? @yolo @drexel","create_date":"2016-02-18T09:20:55.171679","edit_date":null,"id":151,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"the tag should write like #yolo #hellodrexel","create_date":"2016-02-26T00:44:04.511522","edit_date":null,"question":"QUYqwbNkX8HgPFsDZuGVvNp5692","unique_id":"ANbRoFju78L2ByWF7VXWPJ6b510","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUYqwbNkX8HgPFsDZuGVvNp5692","tags":[],"title":"what should I do to take first final exam in Drexel ?","total_answer":1,"total_vote":1,"unique_id":"QUYqwbNkX8HgPFsDZuGVvNp5692","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"anh","is_activated":false,"last_name":"le","resource_uri":"/api/v1/user/quynh-anh","total_answers":1,"total_asked_questions":1,"total_following_questions":0,"username":"quynh-anh","userprofile":{"bio":"computer science","facebook_id":null,"gender":"m","id":439,"occupation":"drexel university ","resource_uri":"/api/v1/profile/439","user":"/api/v1/user/quynh-anh"}}},{"content":"i am not good at chemistry ...","create_date":"2016-02-16T22:22:43.739029","edit_date":null,"id":150,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T00:48:05.543588","edit_date":null,"question":"QUck6j9DTFhsySbut8sYQFEh190","unique_id":"ANU2GvaQUJi8FSXNofXVSkxz634","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUck6j9DTFhsySbut8sYQFEh190","tags":[{"id":47,"resource_uri":"/api/v1/tag/TGQqmsABJ7nuiDUQRRW4WR4h361","unique_id":"TGQqmsABJ7nuiDUQRRW4WR4h361","value":"chemistry"}],"title":"how to study midterm effectively #chemistry","total_answer":2,"total_vote":1,"unique_id":"QUck6j9DTFhsySbut8sYQFEh190","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/vuthanhduc92.jpg","first_name":"Duc","is_activated":false,"last_name":"Vu","resource_uri":"/api/v1/user/vuthanhduc92","total_answers":9,"total_asked_questions":2,"total_following_questions":15,"username":"vuthanhduc92","userprofile":{"bio":"dreamer currently in bay area","facebook_id":null,"gender":"m","id":438,"occupation":"drexel university","resource_uri":"/api/v1/profile/438","user":"/api/v1/user/vuthanhduc92"}}},{"content":"Hello, my name is An","create_date":"2016-02-16T13:46:23.172266","edit_date":null,"id":149,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Is testing good hello ? Yes","create_date":"2016-02-29T00:59:59.677469","edit_date":null,"question":"QUA3LXuyE7aXgiqriDh3A5kx250","unique_id":"ANxnwt3Y89pJRooY6peL3Dkc236","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUA3LXuyE7aXgiqriDh3A5kx250","tags":[],"title":"Hello","total_answer":2,"total_vote":2,"unique_id":"QUA3LXuyE7aXgiqriDh3A5kx250","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"An","is_activated":false,"last_name":"Ho","resource_uri":"/api/v1/user/phattutuhanh","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"phattutuhanh","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":515,"occupation":"","resource_uri":"/api/v1/profile/515","user":"/api/v1/user/phattutuhanh"}}},{"content":"Hello world","create_date":"2016-02-16T00:33:23.394859","edit_date":null,"id":148,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"should not be cs major, so stressful :(","create_date":"2016-02-16T00:34:03.237369","edit_date":null,"question":"QUUkx6LM486mkNVX3viSFm2m992","unique_id":"ANBNcUUZUxwmUFH7ZZSKWYpk589","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":1},"question_type":"normal","resource_uri":"/api/v1/question/QUUkx6LM486mkNVX3viSFm2m992","tags":[],"title":"What is the best major","total_answer":1,"total_vote":1,"unique_id":"QUUkx6LM486mkNVX3viSFm2m992","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/15/dtn17121.jpg","first_name":"Dang","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/dtn17121","total_answers":2,"total_asked_questions":1,"total_following_questions":1,"username":"dtn17121","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":419,"occupation":"","resource_uri":"/api/v1/profile/419","user":"/api/v1/user/dtn17121"}}},{"content":"@Visa @US","create_date":"2016-02-14T14:39:44.107979","edit_date":null,"id":147,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"h1b applies for 6 years employment and you can transfer your h1b ","create_date":"2016-02-16T20:22:18.170942","edit_date":null,"question":"QUZ6yfUaLp8MbeC8SqiEcGXH599","unique_id":"ANma8Y3uGLxUzuBvVWiQujwe654","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUZ6yfUaLp8MbeC8SqiEcGXH599","tags":[],"title":"What is H1B visa and its requirements?","total_answer":1,"total_vote":1,"unique_id":"QUZ6yfUaLp8MbeC8SqiEcGXH599","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"Anh","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/nguyen_duong_ha_anh","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"nguyen_duong_ha_anh","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":486,"occupation":"","resource_uri":"/api/v1/profile/486","user":"/api/v1/user/nguyen_duong_ha_anh"}}},{"content":"I want get to know more co-worker to discuss about chemistry.","create_date":"2016-02-14T01:00:04.942579","edit_date":null,"id":146,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUwpoo3Kvd8NnNRCv96a8Z2q331","tags":[{"id":47,"resource_uri":"/api/v1/tag/TGQqmsABJ7nuiDUQRRW4WR4h361","unique_id":"TGQqmsABJ7nuiDUQRRW4WR4h361","value":"chemistry"},{"id":48,"resource_uri":"/api/v1/tag/TGNSLqDzsp7YPrLpYknUSpEj501","unique_id":"TGNSLqDzsp7YPrLpYknUSpEj501","value":"expert"}],"title":"How can i meet more #chemistry #expert","total_answer":0,"total_vote":3,"unique_id":"QUwpoo3Kvd8NnNRCv96a8Z2q331","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/14/luuhuynhan2306.jpg","first_name":"Kevin","is_activated":false,"last_name":"Luu","resource_uri":"/api/v1/user/luuhuynhan2306","total_answers":0,"total_asked_questions":1,"total_following_questions":1,"username":"luuhuynhan2306","userprofile":{"bio":"Chemistry","facebook_id":null,"gender":"m","id":485,"occupation":"Stanford University","resource_uri":"/api/v1/profile/485","user":"/api/v1/user/luuhuynhan2306"}}},{"content":"","create_date":"2016-02-13T14:07:33.574459","edit_date":"2016-03-14T01:21:10","id":145,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T01:38:28.387122","edit_date":null,"question":"QUhLhCF5BcKVaDdBSrMWio3a219","unique_id":"AN6Bm2RbjMoQD8fediBDQVDn795","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUhLhCF5BcKVaDdBSrMWio3a219","tags":[],"title":"How do you make friends on campus? ","total_answer":1,"total_vote":2,"unique_id":"QUhLhCF5BcKVaDdBSrMWio3a219","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/13/Anh_luong93.jpg","first_name":"Anh ","is_activated":false,"last_name":"L.","resource_uri":"/api/v1/user/Anh_luong93","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"Anh_luong93","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":480,"occupation":"","resource_uri":"/api/v1/profile/480","user":"/api/v1/user/Anh_luong93"}}},{"content":"","create_date":"2016-01-13T20:33:23.657709","edit_date":null,"id":130,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello hao ","create_date":"2016-02-29T01:38:42.348901","edit_date":null,"question":"QUypiZLQhaYGahxKcnm2rppQ786","unique_id":"ANHcz328WwL8djHhavhwg7kk171","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUypiZLQhaYGahxKcnm2rppQ786","tags":[],"title":"Hao asked. ","total_answer":2,"total_vote":1,"unique_id":"QUypiZLQhaYGahxKcnm2rppQ786","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/19/withal_02.jpg","first_name":"Hao","is_activated":false,"last_name":"Wu","resource_uri":"/api/v1/user/withal_02","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"withal_02","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":340,"occupation":"","resource_uri":"/api/v1/profile/340","user":"/api/v1/user/withal_02"}}},{"content":"I don't know how to use this app.","create_date":"2016-01-11T10:20:40.889188","edit_date":null,"id":83,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ???","create_date":"2016-02-29T01:39:21.047520","edit_date":null,"question":"QUUTNZqTJBHaLBfDpCK679BW323","unique_id":"ANCnH6M6ae3oxdjyh5nfPrsT638","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUUTNZqTJBHaLBfDpCK679BW323","tags":[],"title":"How to use this app?","total_answer":3,"total_vote":2,"unique_id":"QUUTNZqTJBHaLBfDpCK679BW323","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/06/hangtran135.jpg","first_name":"Hailey","is_activated":false,"last_name":"Tran","resource_uri":"/api/v1/user/hangtran135","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"hangtran135","userprofile":{"bio":"Graphic Designer and Photographer","facebook_id":null,"gender":"m","id":331,"occupation":"Columbia College Chicago","resource_uri":"/api/v1/profile/331","user":"/api/v1/user/hangtran135"}}},{"content":"Hello","create_date":"2015-12-25T09:49:23.217614","edit_date":null,"id":78,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T00:32:14.398138","edit_date":null,"question":"QUk7zreeuEnG96QnAhnMx4pm876","unique_id":"ANW2sGY5ZCsxDWyMehAEJ65E013","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUk7zreeuEnG96QnAhnMx4pm876","tags":[],"title":"Hello","total_answer":2,"total_vote":1,"unique_id":"QUk7zreeuEnG96QnAhnMx4pm876","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"Yingshi","is_activated":false,"last_name":"Zhang","resource_uri":"/api/v1/user/yingshizhang7","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"yingshizhang7","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":357,"occupation":"","resource_uri":"/api/v1/profile/357","user":"/api/v1/user/yingshizhang7"}}},{"content":"Question details ...","create_date":"2015-12-18T22:57:20.982198","edit_date":null,"id":51,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUwQ5qTxUrtuHg5G7L5WZiAH383","tags":[{"id":1,"resource_uri":"/api/v1/tag/TGHYnLKVtbQCdPQYCHsP4kPF931","unique_id":"TGHYnLKVtbQCdPQYCHsP4kPF931","value":"unitedstates"}],"title":"how can  #unitedstates","total_answer":0,"total_vote":2,"unique_id":"QUwQ5qTxUrtuHg5G7L5WZiAH383","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/19/anguyen.jpg","first_name":"Anh","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/anguyen","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"anguyen","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":346,"occupation":"","resource_uri":"/api/v1/profile/346","user":"/api/v1/user/anguyen"}}},{"content":"","create_date":"2015-12-17T14:58:14.672825","edit_date":null,"id":49,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QU2KxhDnQYY4y9mxr7iNSgfH713","tags":[],"title":"Hi!! Would love to learn more about the co-op programs at Drexel and Northeastern!","total_answer":0,"total_vote":2,"unique_id":"QU2KxhDnQYY4y9mxr7iNSgfH713","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/10/lnhgiang91.jpg","first_name":"Chelsea","is_activated":false,"last_name":"Vo","resource_uri":"/api/v1/user/lnhgiang91","total_answers":0,"total_asked_questions":2,"total_following_questions":0,"username":"lnhgiang91","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":339,"occupation":"","resource_uri":"/api/v1/profile/339","user":"/api/v1/user/lnhgiang91"}}}]}
                        sandbox.emit('ajax/request', {
                            ajaxOptions: {
                                url: 'http://development.univtop.com/api/v1/question?format=json',
                                data: {limit: 10}
                            },
                            callback: function(data){
                                if('success' === data.status){
                                    //render
                                    var html = swig.render(_this.templates.tab, {locals: {questions: data.objects}});
                                    $target.html(html);

                                    $target.data('loading-status', 'success');
                                } else {
                                    $target.html("There's an error while fetching the content")
                                }
                            }
                        });
                        break;
                }


            }
        });

        $menu.children().first().trigger('click');
    }

    _this.init = function(data){
        _this.data = {};
        _this.templates = {};

        _this.handleFormSearch();
        _this.handleTabs();
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = pageQuestions;