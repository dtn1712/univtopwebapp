/**
 * @section header
 */
/**
 *  scripts for header
 * @todo initHeaderSegmentChooseLocation
 * @todo initPopupOrderTracking
 * @todo initPopupRegisterLogin
 * @todo initPopupCart
 * @todo initHeaderSegmentUserInfo (for already logged user)
 * @done initStickyHeader
 */
var header = function (sandbox) {
    var _this = this;

    /**
     * @module header
     * @function init
     * @param {Object} options
     */
    _this.init = function(options){

    };

    _this.destroy = function(){};


    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = header;