'use strict';

var moduleWindow = function(sandbox){
    var _this = this;

    _this.init = function(data){
        var $body = $('body');

        $body
            .on('click', '[data-target=modal-login]', function(){
                sandbox.emit('modal/login/show');
            })
            .on('click', '[data-target=modal-register]', function(){
                sandbox.emit('modal/register/show');
            })
            .on('click', '[data-target=modal-forgot-password]', function(){
                sandbox.emit('modal/forgotPassword/show');
            })
            .on('click', '[data-target=modal-sign-up]', function(){
                sandbox.emit('modal/signUp/show');
            })
            .on('click', '.question [data-action="expand"]', function(event){
                event.preventDefault();

                var $this = $(this),
                    $currentQuestion = $this.closest('.question');

                if($currentQuestion.length){
                    $currentQuestion.attr('data-is-expanded', 1);
                    $this.addClass('hide');
                }
            });
        ;

    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleWindow;