var ADRSandbox = require('./../core/sandbox'),
    ADRApplication = new scaleApp.Core(ADRSandbox)
    ;

ADRApplication.use(scaleApp.plugins.ls);
ADRApplication.use(scaleApp.plugins.util);
ADRApplication.use(scaleApp.plugins.submodule, {
    inherit: true,             // use all plugins from the parent's Core
    use: ['ls','submodule', 'util'],        // use some additional plugins
    useGlobalMediator: true,   // emit and receive all events from the parent's Core
});

module.exports = ADRApplication;