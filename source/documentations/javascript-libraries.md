### Đánh giá code hiện tại
#### 1. Các thư viện hiện tại đang dùng

| Tên thư viện | Phiên bản hiện tại | Phiên bản mới nhất | Hỗ trợ | |
| --- | --- | --- | --- | --- |
| [`jQuery`](https://github.com/jquery/jquery) | 1.11.1 |  | IE7+ | |
| [`jQuery UI`](https://github.com/jquery/jquery-ui) | 1.10.4 | | IE7+ | Hiện chỉ dùng cho autocomplete trên header nhưng đang load cả thư viện |
| [`Bootstrap`](https://github.com/twbs/bootstrap) | 3.3.1 (modded) | 3.3.5 | IE9 | Bổ sung một số CSS nhỏ để hiển thị được trên IE7, 8 |
| [`menuAim`](https://github.com/kamens/jQuery-menu-aim) | ? | ? | IE7+ | |
| [`OwlCarousel`](https://github.com/OwlFonk/OwlCarousel) | 1.3.3 | 2.0.0-beta3 | IE7+ | Chuyển sang dùng [`bản 2`](https://github.com/smashingboxes/OwlCarousel2) |
| [`uniform`](https://github.com/pixelmatrix/uniform) | 1.7.5 | 1.7.5 | ? | Sẽ bỏ
| [`mousewheel`](https://github.com/jquery/jquery-mousewheel) | 3.1.12 | 3.1.13 | ? | |
| [`malihu-custom-scrollbar-plugin`](https://github.com/malihu/malihu-custom-scrollbar-plugin) | 3.0.6 | 3.0.9 | IE7+ | Cân nhắc bỏ, chuyển sang dùng [`niceScroll`](https://github.com/inuyaksa/jquery.nicescroll) vì không chèn thêm DOM vào giữa các element |
| [`elevateZoom`](https://github.com/elevateweb/elevatezoom) | 3.0.8 | 3.0.8 | IE7+ | Cân nhắc chuyển sang dùng [`ez-plus`](https://www.npmjs.com/package/ez-plus) vì có cải tiến về hiệu năng |
| [`select2`](https://github.com/select2/select2) | 3.5.2 | 4.0.0 | IE7+ | |
| [`Bootstrap Notify`](https://github.com/mouse0270/bootstrap-notify) | 3.0.0 | 3.1.5 | IE7+ | |
| [`instaFilta`](https://github.com/chromawoods/instaFilta/) | 1.4.4 | 1.4.4 | IE7+ | |
| [`jQuery lazyload`](https://github.com/eisbehr-/jquery.lazy) | 0.5.3 | 0.5.4 | IE6+ | |
| [`jQuery Validate`]() | 1.11.1 | 1.14.0 | ? | |
| [`encoder`](http://www.strictly-software.com/htmlencode) | 2011-07-14 | 2012-09-23 | ? | |
| [`bootbox`](https://github.com/makeusabrew/bootbox) | 4.4.0 | 4.4.0 | IE7+ | |

  * Nhiều thư viện đã cũ, cần cập nhật lên phiên bản mới (thường là giữ nguyên API, chỉ cải tiến về mặt hiệu năng)
  * CSS của Bootstrap đã bị sửa thẳng vào source, không thể cập nhật trực tiếp

#### 2. CSS
  * Được viết bằng LESS rồi compile ra CSS
  * Đã được phân thành module hóa, nhưng hiện đã có nhiều code thừa, không còn dùng nữa

#### 3. JS
  * Lộn xộn, nhiều code không còn được dùng nữa
  * Do chạy theo tiến độ nên chưa theo một chuẩn viết thống nhất, chưa có review code
  * Logic nhiều chỗ chưa clear, khó debug
  * File thư viện được đặt lên trên thẻ head, gây load chậm hơn
  * Tất cả viết chung vào 1/vài file, chưa module hóa, hầu hết hàm và biến đều được expose ra global
  * Chưa có một cơ chế quản lý các plugin/thư viện và dependencies

#### 4. HTML Template
  * Mất đồng bộ giữa bản implement trên server và bản HTML tĩnh, nên tốn nhiều thời gian hơn để đối chiếu khi implement
  * Khó triển khai các phiên bản prototype giao diện trên HTML tĩnh

### Quá trình Refactor code bao gồm:
#### 1. Cập nhật các thư viện đang dùng lên phiên bản mới nhất, bỏ đi/hoặc cân nhắc sử dụng
#### 2. Đồng bộ lại HTML từ trên server xuống HTML tĩnh
#### 3. Bổ sung comment cho các hàm JS hiện tại
#### 4. Chia riêng source & build, module hóa JS, CSS, HTML theo cùng một cấu trúc chính như sau:

```
source/
├── styles/
│   ├── overrides
│   ├── settings
│   ├── components
│   ├── modules
│   ├── layouts
│   ├── pages
│   └── style.less
├── scripts/
│   ├── includes
│   ├── modules
│   ├── pages
│   └── index.js
├── templates/
│   ├── layouts
│   ├── modules
│   ├── pages
│   └── index.swig
└── vendors/
    ├── bootstrap
    ├── jquery
    ├── jquery-ui
    └── ...
```

#### 5. Xây dựng build tool để tự động compile:
  * Các file JS thư viện thành 1 file vendors.js
  * Các module JS thành 1 file index.js
  * Các modules LESS thành 1 file style.css
  * Các file template .swig thành các file .html
```
build/
├── styles/
│   ├── style_page_1.css       Chứa toàn bộ CSS, chia làm 2/nhiều file nếu quá 4096 selector
│   └── style_page_2.css       
├── scripts/
│   ├── vendors.js      Chứa toàn bộ các thư viện
│   └── index.js        Chứa toàn bộ JS
└── templates/
     ├── pages
     │   ├── *.html     Chứa toàn bộ 
     └── index.html      Liệt kê danh sách các trang tĩnh
```

##### 6. Danh sách các build tool
1. `nodejs`: Môi trường chạy các Build Tool
2. `bower`/`npm` để cài đặt các thư viện
3. `gulp`: Tự động chạy các task build
  * Build JS
    Build file vendors.js mỗi khi cài/cập nhật thư viện
    Build file JS từ các file module (sử dụng `browserify/webpack`)
  * Build CSS
    Từ file LESS rồi cắt thành 2/nhiều file (IE7 không hỗ trợ file CSS có quá 4096 selector)
  * Build HTML (version prototype) để demo
    Từ file template của từng module (sử dụng ngôn ngữ `swig`)
  * Có thể setup để tự động chạy mỗi khi có thay đổi
  * Tự động thay đổi các đường dẫn trong CSS về domain của server


#### 7. Tối ưu hóa code
1. Loại bỏ các code không còn dùng nữa
2. Đề xuất một chuẩn coding style chung: [`Google`](http://google.github.io/styleguide/javascriptguide.xml) hoặc [`AirBNB`](https://github.com/airbnb/javascript/tree/master/es5). Prefer `AirBNB` vì có tài liệu giải thích rõ ràng hơn
3. Đưa các code hiện tại về theo cấu trúc & style guide mới. 
4. Giảm thiểu các biến global: (VD: ParamDetails)