(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var ADRSandbox = require('./../core/sandbox'),
    ADRApplication = new scaleApp.Core(ADRSandbox)
    ;

ADRApplication.use(scaleApp.plugins.ls);
ADRApplication.use(scaleApp.plugins.util);
ADRApplication.use(scaleApp.plugins.submodule, {
    inherit: true,             // use all plugins from the parent's Core
    use: ['ls','submodule', 'util'],        // use some additional plugins
    useGlobalMediator: true,   // emit and receive all events from the parent's Core
});

module.exports = ADRApplication;
},{"./../core/sandbox":2}],2:[function(require,module,exports){
var ADRSandbox = function(core, instanceId, options, moduleId) {

    // define your API
    this.namespace = "ADR";

    // e.g. provide the Mediator methods 'on', 'emit', etc.
    core._mediator.installTo(this);

    // ... or define your custom communication methods
    this.myEmit = function(channel, data){
        core.emit(channel + '/' + instanceId, data);
    };

    // maybe you'd like to expose the instance ID
    this.id = instanceId;

    return this;
};

module.exports = ADRSandbox;
},{}],3:[function(require,module,exports){
'use strict';

(function ($) {
    /**
     * jQuery.ADRDropdown
     * @param options
     * @returns {$.fn.ADRDropdown}
     * @constructor
     */
    $.fn.ADRDropdown = function (options) {
        var defaultSettings = {
            textReplace: false
        };

        var settings = $.extend({},defaultSettings, options);

        this.each(function(){
            var $this = $(this),
                $window = $(window)
            ;

            $this.on('click',function(event){
                $this.toggleClass('open');

                var $target = $(event.target);
                if($.contains($this[0],event.target)){
                    var $text = $this.children('.text:first');
                    //check if it is a button
                    var $textButton = $text.children('.button:first');

                    if(settings.textReplace){
                        if($textButton.length > 0){
                            $textButton.html($target.text());
                        } else {
                            $text.html($target.text());
                        }
                    }
                } else {
                    $this.removeClass('open');
                }

                if($this.hasClass('open')){
                    setTimeout(function(){
                        $window.one('click',function(){
                            $this.removeClass('open');
                        });
                    });
                }
            });
        });

        return this;
    };

}(jQuery));

},{}],4:[function(require,module,exports){
swig.setFilter('calendarTime', function (input) {
    return moment(input).calendar();
});
},{}],5:[function(require,module,exports){
/**
 * Some in-house plugins/libraries
 */
require('./includes/jquery.adr-dropdown');
require('./includes/swig');

//temporarily expose Application to global for debugging purpose
var Application = require('./core/application');

/**
 * register modules
 */
Application.register('ajax', require('./modules/ajax'));

if ($('.adr.header').length) {
    Application.register('header', require('./modules/header'), data.header);
}

Application.register('footer', require('./modules/footer'));
Application.register('modal', require('./modules/modal'));
Application.register('window', require('./modules/window'));

if ($('.page-home').length) {
    Application.register('pageHome',require('./pages/home'));
}

if($('.page-contact').length){
    Application.register('pageContact', require('./pages/contact'));
}

if($('.page-questions').length){
    Application.register('pageQuestions', require('./pages/questions'));
}

Application.start();

window.UnivtopApplication = Application;
},{"./core/application":1,"./includes/jquery.adr-dropdown":3,"./includes/swig":4,"./modules/ajax":6,"./modules/footer":7,"./modules/header":8,"./modules/modal":9,"./modules/window":10,"./pages/contact":11,"./pages/home":12,"./pages/questions":13}],6:[function(require,module,exports){
var ajax = function (sandbox) {
    var _this = this;

    _this.init = function (data) {
        _this.data = data;
    };

    _this.destroy = function () { };

    /**
     * @module handleAjaxCall
     * @description handle all ajax calls
     */
    _this.handleAjaxCall = function (options) {

        var defaultOptions = {
            ajaxOptions: {
                type: 'GET'
                //headers: {
                //    'RequestVerificationToken': globalData.tokenHeaderValue
                //},
            },
            channels: {
                request: 'ajax/request',
                response: 'ajax/response'
            }
        };

        options = $.extend(true, {}, defaultOptions, options);

        $
            .ajax(options.ajaxOptions)
            .always(function(response, status){
                var data = {
                    response: response,
                    status: status
                };

                //run callback
                if('function' === typeof options.callback){
                    return options.callback(data);
                }

                //response channel
                if(options.channels && 'string' === typeof options.channels.response){
                    return sandbox.emit(options.channels.response, data);
                }
            })
        ;
    };

    /**
     * register channels
     */
    sandbox.on('ajax/request', function (options) {

        _this.handleAjaxCall(options);
    });

    return {
        init: _this.init,
        destroy: _this.destroy
    }
};

module.exports = ajax;
},{}],7:[function(require,module,exports){
/**
 *  scripts for footer
 * @todo initFormNewsletter
 * @todo handleIconScrollToTop
 */
var footer = function (sandbox) {
    var _this = this;

    /**
     * @module footer/init
     * @param options
     */
    _this.init = function(options){
    };

    /**
     * @module footer/destroy
     */
    _this.destroy = function(){

    };

    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = footer;
},{}],8:[function(require,module,exports){
/**
 * @section header
 */
/**
 *  scripts for header
 * @todo initHeaderSegmentChooseLocation
 * @todo initPopupOrderTracking
 * @todo initPopupRegisterLogin
 * @todo initPopupCart
 * @todo initHeaderSegmentUserInfo (for already logged user)
 * @done initStickyHeader
 */
var header = function (sandbox) {
    var _this = this;

    /**
     * @module header
     * @function init
     * @param {Object} options
     */
    _this.init = function(options){

    };

    _this.destroy = function(){};


    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = header;
},{}],9:[function(require,module,exports){
'use strict';

var moduleModal = function(sandbox){
    var _this = this;

    _this.hideAllModals = function(){
        bootbox.hideAll();
    }

    _this.showModalLogin = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Log in',
                subtitle: 'Welcome back!',
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_login__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_login__form__input_username">Username</label>
                        <input type="text" id="modal_login__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_login__form__input_password">Password</label>
                        <input type="password" id="modal_login__form__input_password" name="password"/>
                        <div class="text-right"><a data-target="modal-forgot-password">Forgot your password?</a></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Log In!</button>
                    </div>
                    <div class="field">
                        <p>
                            Don't have an account?<br/>
                            <a data-target="modal-sign-up">Sign Up</a>
                        </p>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-login',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_login__form');

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                debug: true,
                rules: {
                    "username": "required",
                    "password": {
                        required: true
                    }
                },
                messages: {
                    username: "Please specify your name",
                    "password": {
                        required: "Please input your password"
                    }
                },
                errorContainer: '#modal_login__form .fields .field.error',
                errorLabelContainer: '#modal_login__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function() { alert("Submitted!") }

            });
        });
    };

    _this.showModalForgotPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_forgot_password__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_forgot_password__form__input_email">Type your email here</label>
                        <input type="text" id="modal_forgot_password__form__input_email" name="email"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    _this.showModalSignUp = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_sign_up__form">
                <div class="inline fields">
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_first_name">First Name</label>
                        <input type="text" id="modal_sign_up__form__input_first_name" name="firstName"/>
                    </div>
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_last_name">Last Name</label>
                        <input type="text" id="modal_sign_up__form__input_last_name" name="lastName"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_sign_up__form__input_email">Email</label>
                        <input type="text" id="modal_sign_up__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_username">Username</label>
                        <input type="text" id="modal_sign_up__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password">Password</label>
                        <input type="password" id="modal_sign_up__form__input_password" name="password"/>
                        <div class="text-right">Already on Univtop? <a data-target="modal-login">Sign In</a></div>
                    </div>
                </div>

                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Let's go!</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    sandbox.on('modal/login/show', function(){
        _this.hideAllModals();
        _this.showModalLogin();
    });

    sandbox.on('modal/forgotPassword/show', function(){
        _this.hideAllModals();
        _this.showModalForgotPassword();
    });

    sandbox.on('modal/signUp/show', function(){
        _this.hideAllModals();
        _this.showModalSignUp();
    });

    _this.init = function(data){
        _this.data = {};
        _this.data.titleTemplate = multiline(function(){/*!@preserve
             <div class="title">{{title}}</div>
             <div class="subtitle">{{subtitle}}</div>
         */console.log});
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleModal;
},{}],10:[function(require,module,exports){
'use strict';

var moduleWindow = function(sandbox){
    var _this = this;

    _this.init = function(data){
        var $body = $('body');

        $body
            .on('click', '[data-target=modal-login]', function(){
                sandbox.emit('modal/login/show');
            })
            .on('click', '[data-target=modal-register]', function(){
                sandbox.emit('modal/register/show');
            })
            .on('click', '[data-target=modal-forgot-password]', function(){
                sandbox.emit('modal/forgotPassword/show');
            })
            .on('click', '[data-target=modal-sign-up]', function(){
                sandbox.emit('modal/signUp/show');
            })
            .on('click', '.question [data-action="expand"]', function(event){
                event.preventDefault();

                var $this = $(this),
                    $currentQuestion = $this.closest('.question');

                if($currentQuestion.length){
                    $currentQuestion.attr('data-is-expanded', 1);
                    $this.addClass('hide');
                }
            });
        ;

    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleWindow;
},{}],11:[function(require,module,exports){
'use strict';

var pageContact = function(sandbox){
    var _this = this;

    _this.handleForm = function(){
        var $form = $('#segment_2__form'),
            $listErrors = $form.find('.group.error')
        ;

        $form.on('submit', function(event){
            event.preventDefault();
        });

        $form.validate({
            debug: true,
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "subject": "required",
                "message": {
                    required: true
                }
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                "subject": "Please specify your subject",
                "message": "Please specify your message"
            },
            errorContainer: '#segment_2__form .group.error',
            errorLabelContainer: '#segment_2__form .group.error .list',
            errorElement: 'div',
            errorClass: 'item',
            //errorPlacement: function(error, element){
            //    $listErrors.html('');
            //    $('<div class="item"></div>').html($(error).text()).appendTo($listErrors);
            //    element.addClass('error');
            //},
            submitHandler: function() { alert("Submitted!") }

        });
    }

    _this.init = function(data){
        _this.handleForm();
    }
    _this.destroy = function(){}
}

module.exports = pageContact;
},{}],12:[function(require,module,exports){
/**
 * @module pageHome
 */
var pageHome = function(sandbox){
    var _this = this;

    /**
     * @module pageHome
     * @function init
     * @param options
     */
    _this.init = function(options){
        _this.objects = {};
        _this.objects.$segment1 = $('#segment_1');
        _this.objects.$segment2 = $('#segment_2');
        _this.objects.$segment1ToggleScrollDown = $('#segment_1__toggle_scroll_down');

        _this.handleAskForm();
        _this.handleSegment1();
    };

    /**
     * @module pageHome
     * @function destroy
     */
    _this.destroy = function(){}

    /**
     * @module pageHome
     * @function handleAskForm
     */
    _this.handleAskForm = function(){
        
    }

    _this.handleSegment1 = function(){
        _this.objects.$segment1.find('[data-toggle=scrollDown]').on('click', function(){
            $("html, body").animate({
                scrollTop: _this.objects.$segment2.offset().top
            }, 300);
        });
    }

    return ({
        init: _this.init,
        destroy: _this.destroy
    })
};

module.exports = pageHome;
},{}],13:[function(require,module,exports){
'use strict';

var pageQuestions = function(sandbox){
    var _this = this;

    _this.handleFormSearch = function(){
        var $input = $('#form_ask_question__input');

        var inputMagicSuggest = $input.magicSuggest({
            data: [
                {
                    name: 'Question 1',
                    href: '/pages/contact-us.html'
                },
                {
                    name: 'Question 2',
                    href: '/pages/question.html'
                }
            ],
            hideTrigger: true,
            selectFirst: true,
            placeholder: '',
            renderer: function(data){
                return '<a href="'+data.href+'">'+data.name+'</a>';
            }
        });

        $(inputMagicSuggest).on('selectionchange', function(){
            var selectedItems = this.getSelection();
            if(selectedItems.length){
                var selectedItem = selectedItems[0];
                if(selectedItem.href){
                    window.location.assign(selectedItem.href);
                }
            }
        });
    }

    _this.handleTabs = function(){
        _this.templates.tab = multiline(function(){/*
         <div class="list questions">
         {% for question in questions %}
            <div class="item question" data-id="{{question.id}}" data-is-expanded="0">
                <div class="univtop card">
                    <div class="thumbnail">
                        <img src="{{question.user_post.avatar}}" alt="">
                        <a href="profile.html" class="meta name">{{question.user_post.first_name}} {{question.user_post.last_name}}</a>
                    </div>
                    <div class="content">
                        <div class="segment">
                            <div class="header">
                                <div class="actions">
                                    <a href="#" data-action="follow" data-question-id="{{question.id}}" data-is-followed="0">Follow</a>
                                </div>
                                <div class="meta time">{{question.create_date}}</div>
                                <div class="tags">
                                {% for tag in question.tags %}
                                    <a class="tag" data-id="{{tag.id}}">{{tag.value}}</a>
                                {% endfor %}
                                </div>
                            </div>

                            <div class="body">
                                <div class="summary">{{question.title}}</div>
                                <div class="extra text">
                                    {{question.content}}
                                    {% if question.latest_answer %}<a href="#" data-action="expand">more</a>{% endif %}
                                </div>
                            </div><!-- end .body -->

                            <div class="footer">
                                <a class="univtop label">
                                    <i class="univtop icon heart"></i>70
                                </a>

                                <a class="univtop label">
                                    <i class="univtop icon chat-bubble"></i>70
                                </a>
                            </div><!-- end .footer -->
                        </div>
                    </div>
                    {% if question.latest_answer %}
                    <div class="answers">
                        <div class="item">
                            <div class="univtop card">
                                <div class="thumbnail">
                                    <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="56" height="56">
                                    <a href="#" class="meta name">{{question.latest_answer.user_post_fullname}}</a>
                                </div>
                                <div class="content">
                                    <div class="segment">
                                        <div class="body">
                                            <div class="summary">{{question.latest_answer.content}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}

                    <form class="univtop form" data-form="reply" data-question-id="{{question.id}}">
                        <div class="inline fields">
                            <div class="field avatar">
                                <img src="../assets/images/demo/pages/question/segment-2.avatar-1.png" alt="" width="72" height="72"/>
                                <div class="meta name">YOU</div>
                            </div>
                            <div class="field input">
                                <div class="fields">
                                    <div class="field">
                                        <textarea name="content" placeholder="Add your answer"></textarea>
                                    </div>
                                    <div class="field text-right">
                                        <button type="submit" class="univtop button">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        {% endfor %}
        </div>
        */console.log});

        var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_2__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;

                //load tab content
                //get data
                switch($target.data('loading-status')){
                    case 'success':
                        //do nothing
                        break;
                    case 'not-yet':
                    case 'error':
                    default:
                        var data = {"meta":{"limit":100,"next":null,"offset":0,"previous":null,"total_count":16},"objects":[{"content":"Hello world!! I am gonna make the most out of my life! yolo!!","create_date":"2016-03-10T18:45:22.632918","edit_date":null,"id":158,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUfffASdMkgV8J7S8Vhjgz6H559","tags":[{"id":52,"resource_uri":"/api/v1/tag/TGVUxs5X66u8fw4wUZG7FZYp328","unique_id":"TGVUxs5X66u8fw4wUZG7FZYp328","value":"yolo!!"}],"title":"\nWhat happens if I don't get selected after the first H1B raffle?","total_answer":0,"total_vote":0,"unique_id":"QUfffASdMkgV8J7S8Vhjgz6H559","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/10/lnhgiang91.jpg","first_name":"Chelsea","is_activated":false,"last_name":"Vo","resource_uri":"/api/v1/user/lnhgiang91","total_answers":0,"total_asked_questions":2,"total_following_questions":0,"username":"lnhgiang91","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":339,"occupation":"","resource_uri":"/api/v1/profile/339","user":"/api/v1/user/lnhgiang91"}}},{"content":"","create_date":"2016-02-28T21:22:23.616467","edit_date":null,"id":157,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"where did you go to school ?","create_date":"2016-02-29T02:35:22.253654","edit_date":null,"question":"QUPeQ5mUsLRaNaSHn9pmDiPv595","unique_id":"AN5qMMdHXYJKFq5JagFX4F4Q973","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUPeQ5mUsLRaNaSHn9pmDiPv595","tags":[],"title":"What is it like to go to an elite school in US ? #college","total_answer":2,"total_vote":4,"unique_id":"QUPeQ5mUsLRaNaSHn9pmDiPv595","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/hyhieu.jpg","first_name":"Hieu","is_activated":false,"last_name":"Pham","resource_uri":"/api/v1/user/hyhieu","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"hyhieu","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":550,"occupation":"","resource_uri":"/api/v1/profile/550","user":"/api/v1/user/hyhieu"}}},{"content":"","create_date":"2016-02-27T17:37:38.035665","edit_date":null,"id":156,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"you should prepare more","create_date":"2016-02-28T18:29:04.453979","edit_date":null,"question":"QUX88q25XKxfAAGrCSXEmQLD027","unique_id":"AN8ouQJM2UdrRWJpq4E53aym398","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUX88q25XKxfAAGrCSXEmQLD027","tags":[{"id":51,"resource_uri":"/api/v1/tag/TGkv8kWBrGVxVVZavkHSgRZp167","unique_id":"TGkv8kWBrGVxVVZavkHSgRZp167","value":"entrepreneurship"}],"title":"What should I do to prepare for creating new company since I am still in US school #entrepreneurship","total_answer":1,"total_vote":2,"unique_id":"QUX88q25XKxfAAGrCSXEmQLD027","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/26/teamunivtop.jpg","first_name":"Team","is_activated":false,"last_name":"Univtop","resource_uri":"/api/v1/user/teamunivtop","total_answers":6,"total_asked_questions":1,"total_following_questions":8,"username":"teamunivtop","userprofile":{"bio":"I am univtop star","facebook_id":null,"gender":"m","id":429,"occupation":"I am in California now","resource_uri":"/api/v1/profile/429","user":"/api/v1/user/teamunivtop"}}},{"content":"i am freshman want to apply for amazon internship","create_date":"2016-02-26T00:48:54.536816","edit_date":null,"id":155,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"I prepared with details side project, networking with school.","create_date":"2016-02-29T02:32:30.279928","edit_date":null,"question":"QUTAGTzTufXzGx4SFx7Sipmw770","unique_id":"ANuJSngKE7EDKoM9Y7kus7qc173","user_post_fullname":"Dang Nguyen","user_post_username":"dtn17121","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUTAGTzTufXzGx4SFx7Sipmw770","tags":[{"id":49,"resource_uri":"/api/v1/tag/TG9uMDKv3VubhqabCJ9Mn7cK549","unique_id":"TG9uMDKv3VubhqabCJ9Mn7cK549","value":"amazon"},{"id":50,"resource_uri":"/api/v1/tag/TG57UtGQiiM5NEj8ZGyFQzhn666","unique_id":"TG57UtGQiiM5NEj8ZGyFQzhn666","value":"dtn1712"}],"title":"how did you get your first job in #amazon #dtn1712","total_answer":2,"total_vote":3,"unique_id":"QUTAGTzTufXzGx4SFx7Sipmw770","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/vuthanhduc92.jpg","first_name":"Duc","is_activated":false,"last_name":"Vu","resource_uri":"/api/v1/user/vuthanhduc92","total_answers":9,"total_asked_questions":2,"total_following_questions":15,"username":"vuthanhduc92","userprofile":{"bio":"dreamer currently in bay area","facebook_id":null,"gender":"m","id":438,"occupation":"drexel university","resource_uri":"/api/v1/profile/438","user":"/api/v1/user/vuthanhduc92"}}},{"content":"Whats up? @yolo @drexel","create_date":"2016-02-18T09:20:55.171679","edit_date":null,"id":151,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"the tag should write like #yolo #hellodrexel","create_date":"2016-02-26T00:44:04.511522","edit_date":null,"question":"QUYqwbNkX8HgPFsDZuGVvNp5692","unique_id":"ANbRoFju78L2ByWF7VXWPJ6b510","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUYqwbNkX8HgPFsDZuGVvNp5692","tags":[],"title":"what should I do to take first final exam in Drexel ?","total_answer":1,"total_vote":1,"unique_id":"QUYqwbNkX8HgPFsDZuGVvNp5692","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"anh","is_activated":false,"last_name":"le","resource_uri":"/api/v1/user/quynh-anh","total_answers":1,"total_asked_questions":1,"total_following_questions":0,"username":"quynh-anh","userprofile":{"bio":"computer science","facebook_id":null,"gender":"m","id":439,"occupation":"drexel university ","resource_uri":"/api/v1/profile/439","user":"/api/v1/user/quynh-anh"}}},{"content":"i am not good at chemistry ...","create_date":"2016-02-16T22:22:43.739029","edit_date":null,"id":150,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T00:48:05.543588","edit_date":null,"question":"QUck6j9DTFhsySbut8sYQFEh190","unique_id":"ANU2GvaQUJi8FSXNofXVSkxz634","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUck6j9DTFhsySbut8sYQFEh190","tags":[{"id":47,"resource_uri":"/api/v1/tag/TGQqmsABJ7nuiDUQRRW4WR4h361","unique_id":"TGQqmsABJ7nuiDUQRRW4WR4h361","value":"chemistry"}],"title":"how to study midterm effectively #chemistry","total_answer":2,"total_vote":1,"unique_id":"QUck6j9DTFhsySbut8sYQFEh190","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/28/vuthanhduc92.jpg","first_name":"Duc","is_activated":false,"last_name":"Vu","resource_uri":"/api/v1/user/vuthanhduc92","total_answers":9,"total_asked_questions":2,"total_following_questions":15,"username":"vuthanhduc92","userprofile":{"bio":"dreamer currently in bay area","facebook_id":null,"gender":"m","id":438,"occupation":"drexel university","resource_uri":"/api/v1/profile/438","user":"/api/v1/user/vuthanhduc92"}}},{"content":"Hello, my name is An","create_date":"2016-02-16T13:46:23.172266","edit_date":null,"id":149,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Is testing good hello ? Yes","create_date":"2016-02-29T00:59:59.677469","edit_date":null,"question":"QUA3LXuyE7aXgiqriDh3A5kx250","unique_id":"ANxnwt3Y89pJRooY6peL3Dkc236","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUA3LXuyE7aXgiqriDh3A5kx250","tags":[],"title":"Hello","total_answer":2,"total_vote":2,"unique_id":"QUA3LXuyE7aXgiqriDh3A5kx250","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"An","is_activated":false,"last_name":"Ho","resource_uri":"/api/v1/user/phattutuhanh","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"phattutuhanh","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":515,"occupation":"","resource_uri":"/api/v1/profile/515","user":"/api/v1/user/phattutuhanh"}}},{"content":"Hello world","create_date":"2016-02-16T00:33:23.394859","edit_date":null,"id":148,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"should not be cs major, so stressful :(","create_date":"2016-02-16T00:34:03.237369","edit_date":null,"question":"QUUkx6LM486mkNVX3viSFm2m992","unique_id":"ANBNcUUZUxwmUFH7ZZSKWYpk589","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":1},"question_type":"normal","resource_uri":"/api/v1/question/QUUkx6LM486mkNVX3viSFm2m992","tags":[],"title":"What is the best major","total_answer":1,"total_vote":1,"unique_id":"QUUkx6LM486mkNVX3viSFm2m992","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/15/dtn17121.jpg","first_name":"Dang","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/dtn17121","total_answers":2,"total_asked_questions":1,"total_following_questions":1,"username":"dtn17121","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":419,"occupation":"","resource_uri":"/api/v1/profile/419","user":"/api/v1/user/dtn17121"}}},{"content":"@Visa @US","create_date":"2016-02-14T14:39:44.107979","edit_date":null,"id":147,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"h1b applies for 6 years employment and you can transfer your h1b ","create_date":"2016-02-16T20:22:18.170942","edit_date":null,"question":"QUZ6yfUaLp8MbeC8SqiEcGXH599","unique_id":"ANma8Y3uGLxUzuBvVWiQujwe654","user_post_fullname":"Duc Vu","user_post_username":"vuthanhduc92","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUZ6yfUaLp8MbeC8SqiEcGXH599","tags":[],"title":"What is H1B visa and its requirements?","total_answer":1,"total_vote":1,"unique_id":"QUZ6yfUaLp8MbeC8SqiEcGXH599","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"Anh","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/nguyen_duong_ha_anh","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"nguyen_duong_ha_anh","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":486,"occupation":"","resource_uri":"/api/v1/profile/486","user":"/api/v1/user/nguyen_duong_ha_anh"}}},{"content":"I want get to know more co-worker to discuss about chemistry.","create_date":"2016-02-14T01:00:04.942579","edit_date":null,"id":146,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUwpoo3Kvd8NnNRCv96a8Z2q331","tags":[{"id":47,"resource_uri":"/api/v1/tag/TGQqmsABJ7nuiDUQRRW4WR4h361","unique_id":"TGQqmsABJ7nuiDUQRRW4WR4h361","value":"chemistry"},{"id":48,"resource_uri":"/api/v1/tag/TGNSLqDzsp7YPrLpYknUSpEj501","unique_id":"TGNSLqDzsp7YPrLpYknUSpEj501","value":"expert"}],"title":"How can i meet more #chemistry #expert","total_answer":0,"total_vote":3,"unique_id":"QUwpoo3Kvd8NnNRCv96a8Z2q331","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/14/luuhuynhan2306.jpg","first_name":"Kevin","is_activated":false,"last_name":"Luu","resource_uri":"/api/v1/user/luuhuynhan2306","total_answers":0,"total_asked_questions":1,"total_following_questions":1,"username":"luuhuynhan2306","userprofile":{"bio":"Chemistry","facebook_id":null,"gender":"m","id":485,"occupation":"Stanford University","resource_uri":"/api/v1/profile/485","user":"/api/v1/user/luuhuynhan2306"}}},{"content":"","create_date":"2016-02-13T14:07:33.574459","edit_date":"2016-03-14T01:21:10","id":145,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T01:38:28.387122","edit_date":null,"question":"QUhLhCF5BcKVaDdBSrMWio3a219","unique_id":"AN6Bm2RbjMoQD8fediBDQVDn795","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUhLhCF5BcKVaDdBSrMWio3a219","tags":[],"title":"How do you make friends on campus? ","total_answer":1,"total_vote":2,"unique_id":"QUhLhCF5BcKVaDdBSrMWio3a219","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/13/Anh_luong93.jpg","first_name":"Anh ","is_activated":false,"last_name":"L.","resource_uri":"/api/v1/user/Anh_luong93","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"Anh_luong93","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":480,"occupation":"","resource_uri":"/api/v1/profile/480","user":"/api/v1/user/Anh_luong93"}}},{"content":"","create_date":"2016-01-13T20:33:23.657709","edit_date":null,"id":130,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello hao ","create_date":"2016-02-29T01:38:42.348901","edit_date":null,"question":"QUypiZLQhaYGahxKcnm2rppQ786","unique_id":"ANHcz328WwL8djHhavhwg7kk171","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUypiZLQhaYGahxKcnm2rppQ786","tags":[],"title":"Hao asked. ","total_answer":2,"total_vote":1,"unique_id":"QUypiZLQhaYGahxKcnm2rppQ786","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/19/withal_02.jpg","first_name":"Hao","is_activated":false,"last_name":"Wu","resource_uri":"/api/v1/user/withal_02","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"withal_02","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":340,"occupation":"","resource_uri":"/api/v1/profile/340","user":"/api/v1/user/withal_02"}}},{"content":"I don't know how to use this app.","create_date":"2016-01-11T10:20:40.889188","edit_date":null,"id":83,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ???","create_date":"2016-02-29T01:39:21.047520","edit_date":null,"question":"QUUTNZqTJBHaLBfDpCK679BW323","unique_id":"ANCnH6M6ae3oxdjyh5nfPrsT638","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUUTNZqTJBHaLBfDpCK679BW323","tags":[],"title":"How to use this app?","total_answer":3,"total_vote":2,"unique_id":"QUUTNZqTJBHaLBfDpCK679BW323","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/06/hangtran135.jpg","first_name":"Hailey","is_activated":false,"last_name":"Tran","resource_uri":"/api/v1/user/hangtran135","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"hangtran135","userprofile":{"bio":"Graphic Designer and Photographer","facebook_id":null,"gender":"m","id":331,"occupation":"Columbia College Chicago","resource_uri":"/api/v1/profile/331","user":"/api/v1/user/hangtran135"}}},{"content":"Hello","create_date":"2015-12-25T09:49:23.217614","edit_date":null,"id":78,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":{"content":"Hello world ","create_date":"2016-02-29T00:32:14.398138","edit_date":null,"question":"QUk7zreeuEnG96QnAhnMx4pm876","unique_id":"ANW2sGY5ZCsxDWyMehAEJ65E013","user_post_fullname":"Team Univtop","user_post_username":"teamunivtop","votes":0},"question_type":"normal","resource_uri":"/api/v1/question/QUk7zreeuEnG96QnAhnMx4pm876","tags":[],"title":"Hello","total_answer":2,"total_vote":1,"unique_id":"QUk7zreeuEnG96QnAhnMx4pm876","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/default/img/user/male_icon.png","first_name":"Yingshi","is_activated":false,"last_name":"Zhang","resource_uri":"/api/v1/user/yingshizhang7","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"yingshizhang7","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":357,"occupation":"","resource_uri":"/api/v1/profile/357","user":"/api/v1/user/yingshizhang7"}}},{"content":"Question details ...","create_date":"2015-12-18T22:57:20.982198","edit_date":null,"id":51,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QUwQ5qTxUrtuHg5G7L5WZiAH383","tags":[{"id":1,"resource_uri":"/api/v1/tag/TGHYnLKVtbQCdPQYCHsP4kPF931","unique_id":"TGHYnLKVtbQCdPQYCHsP4kPF931","value":"unitedstates"}],"title":"how can  #unitedstates","total_answer":0,"total_vote":2,"unique_id":"QUwQ5qTxUrtuHg5G7L5WZiAH383","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2015/12/19/anguyen.jpg","first_name":"Anh","is_activated":false,"last_name":"Nguyen","resource_uri":"/api/v1/user/anguyen","total_answers":0,"total_asked_questions":1,"total_following_questions":0,"username":"anguyen","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":346,"occupation":"","resource_uri":"/api/v1/profile/346","user":"/api/v1/user/anguyen"}}},{"content":"","create_date":"2015-12-17T14:58:14.672825","edit_date":null,"id":49,"is_user_login_answer":false,"is_user_login_follow":false,"is_user_login_vote":false,"latest_answer":null,"question_type":"normal","resource_uri":"/api/v1/question/QU2KxhDnQYY4y9mxr7iNSgfH713","tags":[],"title":"Hi!! Would love to learn more about the co-op programs at Drexel and Northeastern!","total_answer":0,"total_vote":2,"unique_id":"QU2KxhDnQYY4y9mxr7iNSgfH713","user_login_answer_content":null,"user_login_answer_unique_id":null,"user_post":{"avatar":"https://univtop.s3.amazonaws.com/upload_storage/photo/2016/02/10/lnhgiang91.jpg","first_name":"Chelsea","is_activated":false,"last_name":"Vo","resource_uri":"/api/v1/user/lnhgiang91","total_answers":0,"total_asked_questions":2,"total_following_questions":0,"username":"lnhgiang91","userprofile":{"bio":"","facebook_id":null,"gender":"m","id":339,"occupation":"","resource_uri":"/api/v1/profile/339","user":"/api/v1/user/lnhgiang91"}}}]}
                        sandbox.emit('ajax/request', {
                            ajaxOptions: {
                                url: 'http://development.univtop.com/api/v1/question?format=json',
                                data: {limit: 10}
                            },
                            callback: function(data){
                                if('success' === data.status){
                                    //render
                                    var html = swig.render(_this.templates.tab, {locals: {questions: data.objects}});
                                    $target.html(html);

                                    $target.data('loading-status', 'success');
                                } else {
                                    $target.html("There's an error while fetching the content")
                                }
                            }
                        });
                        break;
                }


            }
        });

        $menu.children().first().trigger('click');
    }

    _this.init = function(data){
        _this.data = {};
        _this.templates = {};

        _this.handleFormSearch();
        _this.handleTabs();
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = pageQuestions;
},{}]},{},[5])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb3JlL2FwcGxpY2F0aW9uLmpzIiwic291cmNlL3NjcmlwdHMvY29yZS9zYW5kYm94LmpzIiwic291cmNlL3NjcmlwdHMvaW5jbHVkZXMvanF1ZXJ5LmFkci1kcm9wZG93bi5qcyIsInNvdXJjZS9zY3JpcHRzL2luY2x1ZGVzL3N3aWcuanMiLCJzb3VyY2Uvc2NyaXB0cy9pbmRleC5qcyIsInNvdXJjZS9zY3JpcHRzL21vZHVsZXMvYWpheC5qcyIsInNvdXJjZS9zY3JpcHRzL21vZHVsZXMvZm9vdGVyLmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy9oZWFkZXIuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2R1bGVzL21vZGFsLmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy93aW5kb3cuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9jb250YWN0LmpzIiwic291cmNlL3NjcmlwdHMvcGFnZXMvaG9tZS5qcyIsInNvdXJjZS9zY3JpcHRzL3BhZ2VzL3F1ZXN0aW9ucy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdkRBO0FBQ0E7QUFDQTs7QUNGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNwQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcE9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDM0NBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN4REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJ2YXIgQURSU2FuZGJveCA9IHJlcXVpcmUoJy4vLi4vY29yZS9zYW5kYm94JyksXHJcbiAgICBBRFJBcHBsaWNhdGlvbiA9IG5ldyBzY2FsZUFwcC5Db3JlKEFEUlNhbmRib3gpXHJcbiAgICA7XHJcblxyXG5BRFJBcHBsaWNhdGlvbi51c2Uoc2NhbGVBcHAucGx1Z2lucy5scyk7XHJcbkFEUkFwcGxpY2F0aW9uLnVzZShzY2FsZUFwcC5wbHVnaW5zLnV0aWwpO1xyXG5BRFJBcHBsaWNhdGlvbi51c2Uoc2NhbGVBcHAucGx1Z2lucy5zdWJtb2R1bGUsIHtcclxuICAgIGluaGVyaXQ6IHRydWUsICAgICAgICAgICAgIC8vIHVzZSBhbGwgcGx1Z2lucyBmcm9tIHRoZSBwYXJlbnQncyBDb3JlXHJcbiAgICB1c2U6IFsnbHMnLCdzdWJtb2R1bGUnLCAndXRpbCddLCAgICAgICAgLy8gdXNlIHNvbWUgYWRkaXRpb25hbCBwbHVnaW5zXHJcbiAgICB1c2VHbG9iYWxNZWRpYXRvcjogdHJ1ZSwgICAvLyBlbWl0IGFuZCByZWNlaXZlIGFsbCBldmVudHMgZnJvbSB0aGUgcGFyZW50J3MgQ29yZVxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQURSQXBwbGljYXRpb247IiwidmFyIEFEUlNhbmRib3ggPSBmdW5jdGlvbihjb3JlLCBpbnN0YW5jZUlkLCBvcHRpb25zLCBtb2R1bGVJZCkge1xyXG5cclxuICAgIC8vIGRlZmluZSB5b3VyIEFQSVxyXG4gICAgdGhpcy5uYW1lc3BhY2UgPSBcIkFEUlwiO1xyXG5cclxuICAgIC8vIGUuZy4gcHJvdmlkZSB0aGUgTWVkaWF0b3IgbWV0aG9kcyAnb24nLCAnZW1pdCcsIGV0Yy5cclxuICAgIGNvcmUuX21lZGlhdG9yLmluc3RhbGxUbyh0aGlzKTtcclxuXHJcbiAgICAvLyAuLi4gb3IgZGVmaW5lIHlvdXIgY3VzdG9tIGNvbW11bmljYXRpb24gbWV0aG9kc1xyXG4gICAgdGhpcy5teUVtaXQgPSBmdW5jdGlvbihjaGFubmVsLCBkYXRhKXtcclxuICAgICAgICBjb3JlLmVtaXQoY2hhbm5lbCArICcvJyArIGluc3RhbmNlSWQsIGRhdGEpO1xyXG4gICAgfTtcclxuXHJcbiAgICAvLyBtYXliZSB5b3UnZCBsaWtlIHRvIGV4cG9zZSB0aGUgaW5zdGFuY2UgSURcclxuICAgIHRoaXMuaWQgPSBpbnN0YW5jZUlkO1xyXG5cclxuICAgIHJldHVybiB0aGlzO1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBBRFJTYW5kYm94OyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbihmdW5jdGlvbiAoJCkge1xyXG4gICAgLyoqXHJcbiAgICAgKiBqUXVlcnkuQURSRHJvcGRvd25cclxuICAgICAqIEBwYXJhbSBvcHRpb25zXHJcbiAgICAgKiBAcmV0dXJucyB7JC5mbi5BRFJEcm9wZG93bn1cclxuICAgICAqIEBjb25zdHJ1Y3RvclxyXG4gICAgICovXHJcbiAgICAkLmZuLkFEUkRyb3Bkb3duID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcclxuICAgICAgICB2YXIgZGVmYXVsdFNldHRpbmdzID0ge1xyXG4gICAgICAgICAgICB0ZXh0UmVwbGFjZTogZmFsc2VcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB2YXIgc2V0dGluZ3MgPSAkLmV4dGVuZCh7fSxkZWZhdWx0U2V0dGluZ3MsIG9wdGlvbnMpO1xyXG5cclxuICAgICAgICB0aGlzLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICR3aW5kb3cgPSAkKHdpbmRvdylcclxuICAgICAgICAgICAgO1xyXG5cclxuICAgICAgICAgICAgJHRoaXMub24oJ2NsaWNrJyxmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICAkdGhpcy50b2dnbGVDbGFzcygnb3BlbicpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciAkdGFyZ2V0ID0gJChldmVudC50YXJnZXQpO1xyXG4gICAgICAgICAgICAgICAgaWYoJC5jb250YWlucygkdGhpc1swXSxldmVudC50YXJnZXQpKXtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgJHRleHQgPSAkdGhpcy5jaGlsZHJlbignLnRleHQ6Zmlyc3QnKTtcclxuICAgICAgICAgICAgICAgICAgICAvL2NoZWNrIGlmIGl0IGlzIGEgYnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgdmFyICR0ZXh0QnV0dG9uID0gJHRleHQuY2hpbGRyZW4oJy5idXR0b246Zmlyc3QnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoc2V0dGluZ3MudGV4dFJlcGxhY2Upe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZigkdGV4dEJ1dHRvbi5sZW5ndGggPiAwKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0ZXh0QnV0dG9uLmh0bWwoJHRhcmdldC50ZXh0KCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRleHQuaHRtbCgkdGFyZ2V0LnRleHQoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICR0aGlzLnJlbW92ZUNsYXNzKCdvcGVuJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoJHRoaXMuaGFzQ2xhc3MoJ29wZW4nKSl7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkd2luZG93Lm9uZSgnY2xpY2snLGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGhpcy5yZW1vdmVDbGFzcygnb3BlbicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfTtcclxuXHJcbn0oalF1ZXJ5KSk7XHJcbiIsInN3aWcuc2V0RmlsdGVyKCdjYWxlbmRhclRpbWUnLCBmdW5jdGlvbiAoaW5wdXQpIHtcclxuICAgIHJldHVybiBtb21lbnQoaW5wdXQpLmNhbGVuZGFyKCk7XHJcbn0pOyIsIi8qKlxyXG4gKiBTb21lIGluLWhvdXNlIHBsdWdpbnMvbGlicmFyaWVzXHJcbiAqL1xyXG5yZXF1aXJlKCcuL2luY2x1ZGVzL2pxdWVyeS5hZHItZHJvcGRvd24nKTtcclxucmVxdWlyZSgnLi9pbmNsdWRlcy9zd2lnJyk7XHJcblxyXG4vL3RlbXBvcmFyaWx5IGV4cG9zZSBBcHBsaWNhdGlvbiB0byBnbG9iYWwgZm9yIGRlYnVnZ2luZyBwdXJwb3NlXHJcbnZhciBBcHBsaWNhdGlvbiA9IHJlcXVpcmUoJy4vY29yZS9hcHBsaWNhdGlvbicpO1xyXG5cclxuLyoqXHJcbiAqIHJlZ2lzdGVyIG1vZHVsZXNcclxuICovXHJcbkFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdhamF4JywgcmVxdWlyZSgnLi9tb2R1bGVzL2FqYXgnKSk7XHJcblxyXG5pZiAoJCgnLmFkci5oZWFkZXInKS5sZW5ndGgpIHtcclxuICAgIEFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdoZWFkZXInLCByZXF1aXJlKCcuL21vZHVsZXMvaGVhZGVyJyksIGRhdGEuaGVhZGVyKTtcclxufVxyXG5cclxuQXBwbGljYXRpb24ucmVnaXN0ZXIoJ2Zvb3RlcicsIHJlcXVpcmUoJy4vbW9kdWxlcy9mb290ZXInKSk7XHJcbkFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdtb2RhbCcsIHJlcXVpcmUoJy4vbW9kdWxlcy9tb2RhbCcpKTtcclxuQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3dpbmRvdycsIHJlcXVpcmUoJy4vbW9kdWxlcy93aW5kb3cnKSk7XHJcblxyXG5pZiAoJCgnLnBhZ2UtaG9tZScpLmxlbmd0aCkge1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VIb21lJyxyZXF1aXJlKCcuL3BhZ2VzL2hvbWUnKSk7XHJcbn1cclxuXHJcbmlmKCQoJy5wYWdlLWNvbnRhY3QnKS5sZW5ndGgpe1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VDb250YWN0JywgcmVxdWlyZSgnLi9wYWdlcy9jb250YWN0JykpO1xyXG59XHJcblxyXG5pZigkKCcucGFnZS1xdWVzdGlvbnMnKS5sZW5ndGgpe1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VRdWVzdGlvbnMnLCByZXF1aXJlKCcuL3BhZ2VzL3F1ZXN0aW9ucycpKTtcclxufVxyXG5cclxuQXBwbGljYXRpb24uc3RhcnQoKTtcclxuXHJcbndpbmRvdy5Vbml2dG9wQXBwbGljYXRpb24gPSBBcHBsaWNhdGlvbjsiLCJ2YXIgYWpheCA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XHJcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgIF90aGlzLmRhdGEgPSBkYXRhO1xyXG4gICAgfTtcclxuXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24gKCkgeyB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQG1vZHVsZSBoYW5kbGVBamF4Q2FsbFxyXG4gICAgICogQGRlc2NyaXB0aW9uIGhhbmRsZSBhbGwgYWpheCBjYWxsc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5oYW5kbGVBamF4Q2FsbCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcblxyXG4gICAgICAgIHZhciBkZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgYWpheE9wdGlvbnM6IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdHRVQnXHJcbiAgICAgICAgICAgICAgICAvL2hlYWRlcnM6IHtcclxuICAgICAgICAgICAgICAgIC8vICAgICdSZXF1ZXN0VmVyaWZpY2F0aW9uVG9rZW4nOiBnbG9iYWxEYXRhLnRva2VuSGVhZGVyVmFsdWVcclxuICAgICAgICAgICAgICAgIC8vfSxcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY2hhbm5lbHM6IHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3Q6ICdhamF4L3JlcXVlc3QnLFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2U6ICdhamF4L3Jlc3BvbnNlJ1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcblxyXG4gICAgICAgICRcclxuICAgICAgICAgICAgLmFqYXgob3B0aW9ucy5hamF4T3B0aW9ucylcclxuICAgICAgICAgICAgLmFsd2F5cyhmdW5jdGlvbihyZXNwb25zZSwgc3RhdHVzKXtcclxuICAgICAgICAgICAgICAgIHZhciBkYXRhID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlOiByZXNwb25zZSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHN0YXR1c1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICAvL3J1biBjYWxsYmFja1xyXG4gICAgICAgICAgICAgICAgaWYoJ2Z1bmN0aW9uJyA9PT0gdHlwZW9mIG9wdGlvbnMuY2FsbGJhY2spe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBvcHRpb25zLmNhbGxiYWNrKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIC8vcmVzcG9uc2UgY2hhbm5lbFxyXG4gICAgICAgICAgICAgICAgaWYob3B0aW9ucy5jaGFubmVscyAmJiAnc3RyaW5nJyA9PT0gdHlwZW9mIG9wdGlvbnMuY2hhbm5lbHMucmVzcG9uc2Upe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzYW5kYm94LmVtaXQob3B0aW9ucy5jaGFubmVscy5yZXNwb25zZSwgZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgO1xyXG4gICAgfTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIHJlZ2lzdGVyIGNoYW5uZWxzXHJcbiAgICAgKi9cclxuICAgIHNhbmRib3gub24oJ2FqYXgvcmVxdWVzdCcsIGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcblxyXG4gICAgICAgIF90aGlzLmhhbmRsZUFqYXhDYWxsKG9wdGlvbnMpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBpbml0OiBfdGhpcy5pbml0LFxyXG4gICAgICAgIGRlc3Ryb3k6IF90aGlzLmRlc3Ryb3lcclxuICAgIH1cclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gYWpheDsiLCIvKipcclxuICogIHNjcmlwdHMgZm9yIGZvb3RlclxyXG4gKiBAdG9kbyBpbml0Rm9ybU5ld3NsZXR0ZXJcclxuICogQHRvZG8gaGFuZGxlSWNvblNjcm9sbFRvVG9wXHJcbiAqL1xyXG52YXIgZm9vdGVyID0gZnVuY3Rpb24gKHNhbmRib3gpIHtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIGZvb3Rlci9pbml0XHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcbiAgICB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQG1vZHVsZSBmb290ZXIvZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9O1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBmb290ZXI7IiwiLyoqXHJcbiAqIEBzZWN0aW9uIGhlYWRlclxyXG4gKi9cclxuLyoqXHJcbiAqICBzY3JpcHRzIGZvciBoZWFkZXJcclxuICogQHRvZG8gaW5pdEhlYWRlclNlZ21lbnRDaG9vc2VMb2NhdGlvblxyXG4gKiBAdG9kbyBpbml0UG9wdXBPcmRlclRyYWNraW5nXHJcbiAqIEB0b2RvIGluaXRQb3B1cFJlZ2lzdGVyTG9naW5cclxuICogQHRvZG8gaW5pdFBvcHVwQ2FydFxyXG4gKiBAdG9kbyBpbml0SGVhZGVyU2VnbWVudFVzZXJJbmZvIChmb3IgYWxyZWFkeSBsb2dnZWQgdXNlcilcclxuICogQGRvbmUgaW5pdFN0aWNreUhlYWRlclxyXG4gKi9cclxudmFyIGhlYWRlciA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XHJcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQG1vZHVsZSBoZWFkZXJcclxuICAgICAqIEBmdW5jdGlvbiBpbml0XHJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9O1xyXG5cclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXHJcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxyXG4gICAgfTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gaGVhZGVyOyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBtb2R1bGVNb2RhbCA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5oaWRlQWxsTW9kYWxzID0gZnVuY3Rpb24oKXtcclxuICAgICAgICBib290Ym94LmhpZGVBbGwoKTtcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5zaG93TW9kYWxMb2dpbiA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIHRpdGxlID0gc3dpZy5yZW5kZXIoX3RoaXMuZGF0YS50aXRsZVRlbXBsYXRlLCB7bG9jYWxzOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0xvZyBpbicsXHJcbiAgICAgICAgICAgICAgICBzdWJ0aXRsZTogJ1dlbGNvbWUgYmFjayEnLFxyXG4gICAgICAgICAgICB9fSksXHJcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGlkPVwibW9kYWxfbG9naW5fX2Zvcm1cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF91c2VybmFtZVwiPlVzZXJuYW1lPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9sb2dpbl9fZm9ybV9faW5wdXRfdXNlcm5hbWVcIiBuYW1lPVwidXNlcm5hbWVcIi8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9sb2dpbl9fZm9ybV9faW5wdXRfcGFzc3dvcmRcIj5QYXNzd29yZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9wYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZFwiLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtcmlnaHRcIj48YSBkYXRhLXRhcmdldD1cIm1vZGFsLWZvcmdvdC1wYXNzd29yZFwiPkZvcmdvdCB5b3VyIHBhc3N3b3JkPzwvYT48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdFwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5saW5lIGZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwidW5pdnRvcCBidXR0b25cIiB0eXBlPVwic3VibWl0XCI+TG9nIEluITwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvbid0IGhhdmUgYW4gYWNjb3VudD88YnIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1zaWduLXVwXCI+U2lnbiBVcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1sb2dpbicsXHJcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgbWVzc2FnZTogbWVzc2FnZSxcclxuICAgICAgICAgICAgb25Fc2NhcGU6IHRydWUsXHJcbiAgICAgICAgICAgIGJhY2tkcm9wOiB0cnVlXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRkaWFsb2cub24oJ3Nob3duLmJzLm1vZGFsJywgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgdmFyICRmb3JtID0gJGRpYWxvZy5maW5kKCcjbW9kYWxfbG9naW5fX2Zvcm0nKTtcclxuXHJcbiAgICAgICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICRmb3JtLnZhbGlkYXRlKHtcclxuICAgICAgICAgICAgICAgIGRlYnVnOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcnVsZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBcInVzZXJuYW1lXCI6IFwicmVxdWlyZWRcIixcclxuICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VybmFtZTogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG5hbWVcIixcclxuICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGlucHV0IHlvdXIgcGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNtb2RhbF9sb2dpbl9fZm9ybSAuZmllbGRzIC5maWVsZC5lcnJvcicsXHJcbiAgICAgICAgICAgICAgICBlcnJvckxhYmVsQ29udGFpbmVyOiAnI21vZGFsX2xvZ2luX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yIC5saXN0JyxcclxuICAgICAgICAgICAgICAgIGVycm9yRWxlbWVudDogJ2RpdicsXHJcbiAgICAgICAgICAgICAgICBlcnJvckNsYXNzOiAnaXRlbScsXHJcblxyXG4gICAgICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oKSB7IGFsZXJ0KFwiU3VibWl0dGVkIVwiKSB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3RoaXMuc2hvd01vZGFsRm9yZ290UGFzc3dvcmQgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciB0aXRsZSA9IHN3aWcucmVuZGVyKF90aGlzLmRhdGEudGl0bGVUZW1wbGF0ZSwge2xvY2Fsczoge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdGb3Jnb3QgeW91ciBwYXNzd29yZD8nXHJcbiAgICAgICAgICAgIH19KSxcclxuICAgICAgICAgICAgbWVzc2FnZVRlbXBsYXRlID0gbXVsdGlsaW5lKGZ1bmN0aW9uKCl7LyohQHByZXNlcnZlXHJcbiAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwidW5pdnRvcCBmb3JtXCIgaWQ9XCJtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybV9faW5wdXRfZW1haWxcIj5UeXBlIHlvdXIgZW1haWwgaGVyZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwibW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtX19pbnB1dF9lbWFpbFwiIG5hbWU9XCJlbWFpbFwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdFwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkIHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJ1bml2dG9wIGJ1dHRvblwiIHR5cGU9XCJzdWJtaXRcIj5SZXNldCBQYXNzd29yZDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1mb3Jnb3QtcGFzc3dvcmQnLFxyXG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIG9uRXNjYXBlOiB0cnVlLFxyXG4gICAgICAgICAgICBiYWNrZHJvcDogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICRkaWFsb2cuZmluZCgnI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybScpO1xyXG5cclxuICAgICAgICAgICAgJGZvcm0ub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBhbGVydCgyMjIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3RoaXMuc2hvd01vZGFsU2lnblVwID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgdGl0bGUgPSBzd2lnLnJlbmRlcihfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUsIHtsb2NhbHM6IHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiAnRm9yZ290IHlvdXIgcGFzc3dvcmQ/J1xyXG4gICAgICAgICAgICB9fSksXHJcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZSBmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2l4IHdpZGUgZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2ZpcnN0X25hbWVcIj5GaXJzdCBOYW1lPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9maXJzdF9uYW1lXCIgbmFtZT1cImZpcnN0TmFtZVwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2l4IHdpZGUgZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2xhc3RfbmFtZVwiPkxhc3QgTmFtZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfbGFzdF9uYW1lXCIgbmFtZT1cImxhc3ROYW1lXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9lbWFpbFwiPkVtYWlsPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9lbWFpbFwiIG5hbWU9XCJlbWFpbFwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3VzZXJuYW1lXCI+VXNlcm5hbWU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3VzZXJuYW1lXCIgbmFtZT1cInVzZXJuYW1lXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfcGFzc3dvcmRcIj5QYXNzd29yZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3Bhc3N3b3JkXCIgbmFtZT1cInBhc3N3b3JkXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1yaWdodFwiPkFscmVhZHkgb24gVW5pdnRvcD8gPGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1sb2dpblwiPlNpZ24gSW48L2E+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVycm9yIGZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsaXN0XCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInVuaXZ0b3AgYnV0dG9uXCIgdHlwZT1cInN1Ym1pdFwiPkxldCdzIGdvITwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1mb3Jnb3QtcGFzc3dvcmQnLFxyXG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIG9uRXNjYXBlOiB0cnVlLFxyXG4gICAgICAgICAgICBiYWNrZHJvcDogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICRkaWFsb2cuZmluZCgnI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybScpO1xyXG5cclxuICAgICAgICAgICAgJGZvcm0ub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBhbGVydCgyMjIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgc2FuZGJveC5vbignbW9kYWwvbG9naW4vc2hvdycsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgX3RoaXMuaGlkZUFsbE1vZGFscygpO1xyXG4gICAgICAgIF90aGlzLnNob3dNb2RhbExvZ2luKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBzYW5kYm94Lm9uKCdtb2RhbC9mb3Jnb3RQYXNzd29yZC9zaG93JywgZnVuY3Rpb24oKXtcclxuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XHJcbiAgICAgICAgX3RoaXMuc2hvd01vZGFsRm9yZ290UGFzc3dvcmQoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHNhbmRib3gub24oJ21vZGFsL3NpZ25VcC9zaG93JywgZnVuY3Rpb24oKXtcclxuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XHJcbiAgICAgICAgX3RoaXMuc2hvd01vZGFsU2lnblVwKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgX3RoaXMuZGF0YSA9IHt9O1xyXG4gICAgICAgIF90aGlzLmRhdGEudGl0bGVUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpdGxlXCI+e3t0aXRsZX19PC9kaXY+XHJcbiAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VidGl0bGVcIj57e3N1YnRpdGxlfX08L2Rpdj5cclxuICAgICAgICAgKi9jb25zb2xlLmxvZ30pO1xyXG4gICAgfVxyXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gbW9kdWxlTW9kYWw7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIG1vZHVsZVdpbmRvdyA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgdmFyICRib2R5ID0gJCgnYm9keScpO1xyXG5cclxuICAgICAgICAkYm9keVxyXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJ1tkYXRhLXRhcmdldD1tb2RhbC1sb2dpbl0nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9sb2dpbi9zaG93Jyk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbignY2xpY2snLCAnW2RhdGEtdGFyZ2V0PW1vZGFsLXJlZ2lzdGVyXScsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ21vZGFsL3JlZ2lzdGVyL3Nob3cnKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtZm9yZ290LXBhc3N3b3JkXScsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ21vZGFsL2ZvcmdvdFBhc3N3b3JkL3Nob3cnKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtc2lnbi11cF0nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9zaWduVXAvc2hvdycpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJy5xdWVzdGlvbiBbZGF0YS1hY3Rpb249XCJleHBhbmRcIl0nLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgJGN1cnJlbnRRdWVzdGlvbiA9ICR0aGlzLmNsb3Nlc3QoJy5xdWVzdGlvbicpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKCRjdXJyZW50UXVlc3Rpb24ubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICAgICAkY3VycmVudFF1ZXN0aW9uLmF0dHIoJ2RhdGEtaXMtZXhwYW5kZWQnLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICAkdGhpcy5hZGRDbGFzcygnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICA7XHJcblxyXG4gICAgfVxyXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gbW9kdWxlV2luZG93OyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBwYWdlQ29udGFjdCA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5oYW5kbGVGb3JtID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgJGZvcm0gPSAkKCcjc2VnbWVudF8yX19mb3JtJyksXHJcbiAgICAgICAgICAgICRsaXN0RXJyb3JzID0gJGZvcm0uZmluZCgnLmdyb3VwLmVycm9yJylcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRmb3JtLnZhbGlkYXRlKHtcclxuICAgICAgICAgICAgZGVidWc6IHRydWUsXHJcbiAgICAgICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJlbWFpbFwiOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IHRydWVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcInN1YmplY3RcIjogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJtZXNzYWdlXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtZXNzYWdlczoge1xyXG4gICAgICAgICAgICAgICAgbmFtZTogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG5hbWVcIixcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiV2UgbmVlZCB5b3VyIGVtYWlsIGFkZHJlc3MgdG8gY29udGFjdCB5b3VcIixcclxuICAgICAgICAgICAgICAgICAgICBlbWFpbDogXCJZb3VyIGVtYWlsIGFkZHJlc3MgbXVzdCBiZSBpbiB0aGUgZm9ybWF0IG9mIG5hbWVAZG9tYWluLmNvbVwiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXCJzdWJqZWN0XCI6IFwiUGxlYXNlIHNwZWNpZnkgeW91ciBzdWJqZWN0XCIsXHJcbiAgICAgICAgICAgICAgICBcIm1lc3NhZ2VcIjogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG1lc3NhZ2VcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNzZWdtZW50XzJfX2Zvcm0gLmdyb3VwLmVycm9yJyxcclxuICAgICAgICAgICAgZXJyb3JMYWJlbENvbnRhaW5lcjogJyNzZWdtZW50XzJfX2Zvcm0gLmdyb3VwLmVycm9yIC5saXN0JyxcclxuICAgICAgICAgICAgZXJyb3JFbGVtZW50OiAnZGl2JyxcclxuICAgICAgICAgICAgZXJyb3JDbGFzczogJ2l0ZW0nLFxyXG4gICAgICAgICAgICAvL2Vycm9yUGxhY2VtZW50OiBmdW5jdGlvbihlcnJvciwgZWxlbWVudCl7XHJcbiAgICAgICAgICAgIC8vICAgICRsaXN0RXJyb3JzLmh0bWwoJycpO1xyXG4gICAgICAgICAgICAvLyAgICAkKCc8ZGl2IGNsYXNzPVwiaXRlbVwiPjwvZGl2PicpLmh0bWwoJChlcnJvcikudGV4dCgpKS5hcHBlbmRUbygkbGlzdEVycm9ycyk7XHJcbiAgICAgICAgICAgIC8vICAgIGVsZW1lbnQuYWRkQ2xhc3MoJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgIC8vfSxcclxuICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oKSB7IGFsZXJ0KFwiU3VibWl0dGVkIVwiKSB9XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbihkYXRhKXtcclxuICAgICAgICBfdGhpcy5oYW5kbGVGb3JtKCk7XHJcbiAgICB9XHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gcGFnZUNvbnRhY3Q7IiwiLyoqXHJcbiAqIEBtb2R1bGUgcGFnZUhvbWVcclxuICovXHJcbnZhciBwYWdlSG9tZSA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEBtb2R1bGUgcGFnZUhvbWVcclxuICAgICAqIEBmdW5jdGlvbiBpbml0XHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcbiAgICAgICAgX3RoaXMub2JqZWN0cyA9IHt9O1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQxID0gJCgnI3NlZ21lbnRfMScpO1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQyID0gJCgnI3NlZ21lbnRfMicpO1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQxVG9nZ2xlU2Nyb2xsRG93biA9ICQoJyNzZWdtZW50XzFfX3RvZ2dsZV9zY3JvbGxfZG93bicpO1xyXG5cclxuICAgICAgICBfdGhpcy5oYW5kbGVBc2tGb3JtKCk7XHJcbiAgICAgICAgX3RoaXMuaGFuZGxlU2VnbWVudDEoKTtcclxuICAgIH07XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIHBhZ2VIb21lXHJcbiAgICAgKiBAZnVuY3Rpb24gZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIHBhZ2VIb21lXHJcbiAgICAgKiBAZnVuY3Rpb24gaGFuZGxlQXNrRm9ybVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5oYW5kbGVBc2tGb3JtID0gZnVuY3Rpb24oKXtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5oYW5kbGVTZWdtZW50MSA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgX3RoaXMub2JqZWN0cy4kc2VnbWVudDEuZmluZCgnW2RhdGEtdG9nZ2xlPXNjcm9sbERvd25dJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IF90aGlzLm9iamVjdHMuJHNlZ21lbnQyLm9mZnNldCgpLnRvcFxyXG4gICAgICAgICAgICB9LCAzMDApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoe1xyXG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXHJcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxyXG4gICAgfSlcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gcGFnZUhvbWU7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHBhZ2VRdWVzdGlvbnMgPSBmdW5jdGlvbihzYW5kYm94KXtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgX3RoaXMuaGFuZGxlRm9ybVNlYXJjaCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyICRpbnB1dCA9ICQoJyNmb3JtX2Fza19xdWVzdGlvbl9faW5wdXQnKTtcclxuXHJcbiAgICAgICAgdmFyIGlucHV0TWFnaWNTdWdnZXN0ID0gJGlucHV0Lm1hZ2ljU3VnZ2VzdCh7XHJcbiAgICAgICAgICAgIGRhdGE6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnUXVlc3Rpb24gMScsXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZjogJy9wYWdlcy9jb250YWN0LXVzLmh0bWwnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdRdWVzdGlvbiAyJyxcclxuICAgICAgICAgICAgICAgICAgICBocmVmOiAnL3BhZ2VzL3F1ZXN0aW9uLmh0bWwnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIGhpZGVUcmlnZ2VyOiB0cnVlLFxyXG4gICAgICAgICAgICBzZWxlY3RGaXJzdDogdHJ1ZSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6ICcnLFxyXG4gICAgICAgICAgICByZW5kZXJlcjogZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCInK2RhdGEuaHJlZisnXCI+JytkYXRhLm5hbWUrJzwvYT4nO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoaW5wdXRNYWdpY1N1Z2dlc3QpLm9uKCdzZWxlY3Rpb25jaGFuZ2UnLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICB2YXIgc2VsZWN0ZWRJdGVtcyA9IHRoaXMuZ2V0U2VsZWN0aW9uKCk7XHJcbiAgICAgICAgICAgIGlmKHNlbGVjdGVkSXRlbXMubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgIHZhciBzZWxlY3RlZEl0ZW0gPSBzZWxlY3RlZEl0ZW1zWzBdO1xyXG4gICAgICAgICAgICAgICAgaWYoc2VsZWN0ZWRJdGVtLmhyZWYpe1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5hc3NpZ24oc2VsZWN0ZWRJdGVtLmhyZWYpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3RoaXMuaGFuZGxlVGFicyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgX3RoaXMudGVtcGxhdGVzLnRhYiA9IG11bHRpbGluZShmdW5jdGlvbigpey8qXHJcbiAgICAgICAgIDxkaXYgY2xhc3M9XCJsaXN0IHF1ZXN0aW9uc1wiPlxyXG4gICAgICAgICB7JSBmb3IgcXVlc3Rpb24gaW4gcXVlc3Rpb25zICV9XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtIHF1ZXN0aW9uXCIgZGF0YS1pZD1cInt7cXVlc3Rpb24uaWR9fVwiIGRhdGEtaXMtZXhwYW5kZWQ9XCIwXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidW5pdnRvcCBjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRodW1ibmFpbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cInt7cXVlc3Rpb24udXNlcl9wb3N0LmF2YXRhcn19XCIgYWx0PVwiXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJwcm9maWxlLmh0bWxcIiBjbGFzcz1cIm1ldGEgbmFtZVwiPnt7cXVlc3Rpb24udXNlcl9wb3N0LmZpcnN0X25hbWV9fSB7e3F1ZXN0aW9uLnVzZXJfcG9zdC5sYXN0X25hbWV9fTwvYT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2VnbWVudFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhY3Rpb25zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgZGF0YS1hY3Rpb249XCJmb2xsb3dcIiBkYXRhLXF1ZXN0aW9uLWlkPVwie3txdWVzdGlvbi5pZH19XCIgZGF0YS1pcy1mb2xsb3dlZD1cIjBcIj5Gb2xsb3c8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1ldGEgdGltZVwiPnt7cXVlc3Rpb24uY3JlYXRlX2RhdGV9fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0YWdzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyUgZm9yIHRhZyBpbiBxdWVzdGlvbi50YWdzICV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwidGFnXCIgZGF0YS1pZD1cInt7dGFnLmlkfX1cIj57e3RhZy52YWx1ZX19PC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHslIGVuZGZvciAlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImJvZHlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VtbWFyeVwiPnt7cXVlc3Rpb24udGl0bGV9fTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJleHRyYSB0ZXh0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHt7cXVlc3Rpb24uY29udGVudH19XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHslIGlmIHF1ZXN0aW9uLmxhdGVzdF9hbnN3ZXIgJX08YSBocmVmPVwiI1wiIGRhdGEtYWN0aW9uPVwiZXhwYW5kXCI+bW9yZTwvYT57JSBlbmRpZiAlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBlbmQgLmJvZHkgLS0+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvb3RlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwidW5pdnRvcCBsYWJlbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cInVuaXZ0b3AgaWNvbiBoZWFydFwiPjwvaT43MFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYT5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgY2xhc3M9XCJ1bml2dG9wIGxhYmVsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwidW5pdnRvcCBpY29uIGNoYXQtYnViYmxlXCI+PC9pPjcwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+PCEtLSBlbmQgLmZvb3RlciAtLT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyUgaWYgcXVlc3Rpb24ubGF0ZXN0X2Fuc3dlciAlfVxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhbnN3ZXJzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidW5pdnRvcCBjYXJkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRodW1ibmFpbFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi4uL2Fzc2V0cy9pbWFnZXMvZGVtby9wYWdlcy9xdWVzdGlvbi9zZWdtZW50LTIuYXZhdGFyLTEucG5nXCIgYWx0PVwiXCIgd2lkdGg9XCI1NlwiIGhlaWdodD1cIjU2XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XCIjXCIgY2xhc3M9XCJtZXRhIG5hbWVcIj57e3F1ZXN0aW9uLmxhdGVzdF9hbnN3ZXIudXNlcl9wb3N0X2Z1bGxuYW1lfX08L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNlZ21lbnRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInN1bW1hcnlcIj57e3F1ZXN0aW9uLmxhdGVzdF9hbnN3ZXIuY29udGVudH19PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgeyUgZW5kaWYgJX1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPGZvcm0gY2xhc3M9XCJ1bml2dG9wIGZvcm1cIiBkYXRhLWZvcm09XCJyZXBseVwiIGRhdGEtcXVlc3Rpb24taWQ9XCJ7e3F1ZXN0aW9uLmlkfX1cIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZSBmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZCBhdmF0YXJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cIi4uL2Fzc2V0cy9pbWFnZXMvZGVtby9wYWdlcy9xdWVzdGlvbi9zZWdtZW50LTIuYXZhdGFyLTEucG5nXCIgYWx0PVwiXCIgd2lkdGg9XCI3MlwiIGhlaWdodD1cIjcyXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtZXRhIG5hbWVcIj5ZT1U8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkIGlucHV0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDx0ZXh0YXJlYSBuYW1lPVwiY29udGVudFwiIHBsYWNlaG9sZGVyPVwiQWRkIHlvdXIgYW5zd2VyXCI+PC90ZXh0YXJlYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZCB0ZXh0LXJpZ2h0XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIiBjbGFzcz1cInVuaXZ0b3AgYnV0dG9uXCI+U3VibWl0PC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICB7JSBlbmRmb3IgJX1cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICAqL2NvbnNvbGUubG9nfSk7XHJcblxyXG4gICAgICAgIHZhciAkbWVudSA9ICQoJyNzZWdtZW50XzJfX21lbnUnKSxcclxuICAgICAgICAgICAgJHRhYnMgPSAkKCcjc2VnbWVudF8yX190YWJzJylcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgICRtZW51Lm9uKCdjbGljaycsICc+Lml0ZW0nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgJHRhcmdldCA9ICQoJHRoaXMuZGF0YSgndGFyZ2V0JykpXHJcbiAgICAgICAgICAgIDtcclxuXHJcbiAgICAgICAgICAgICR0aGlzXHJcbiAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAuc2libGluZ3MoJy5hY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICA7XHJcblxyXG4gICAgICAgICAgICBpZigkdGFyZ2V0Lmxlbmd0aCl7XHJcbiAgICAgICAgICAgICAgICAkdGFyZ2V0XHJcbiAgICAgICAgICAgICAgICAgICAgLmFkZENsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zaWJsaW5ncygnLmFjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAgICAgLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG4gICAgICAgICAgICAgICAgO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vbG9hZCB0YWIgY29udGVudFxyXG4gICAgICAgICAgICAgICAgLy9nZXQgZGF0YVxyXG4gICAgICAgICAgICAgICAgc3dpdGNoKCR0YXJnZXQuZGF0YSgnbG9hZGluZy1zdGF0dXMnKSl7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnc3VjY2Vzcyc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vZG8gbm90aGluZ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdub3QteWV0JzpcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdlcnJvcic6XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRhdGEgPSB7XCJtZXRhXCI6e1wibGltaXRcIjoxMDAsXCJuZXh0XCI6bnVsbCxcIm9mZnNldFwiOjAsXCJwcmV2aW91c1wiOm51bGwsXCJ0b3RhbF9jb3VudFwiOjE2fSxcIm9iamVjdHNcIjpbe1wiY29udGVudFwiOlwiSGVsbG8gd29ybGQhISBJIGFtIGdvbm5hIG1ha2UgdGhlIG1vc3Qgb3V0IG9mIG15IGxpZmUhIHlvbG8hIVwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDMtMTBUMTg6NDU6MjIuNjMyOTE4XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwiaWRcIjoxNTgsXCJpc191c2VyX2xvZ2luX2Fuc3dlclwiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl9mb2xsb3dcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fdm90ZVwiOmZhbHNlLFwibGF0ZXN0X2Fuc3dlclwiOm51bGwsXCJxdWVzdGlvbl90eXBlXCI6XCJub3JtYWxcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9xdWVzdGlvbi9RVWZmZkFTZE1rZ1Y4SjdTOFZoamd6Nkg1NTlcIixcInRhZ3NcIjpbe1wiaWRcIjo1MixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS90YWcvVEdWVXhzNVg2NnU4Znc0d1VaRzdGWllwMzI4XCIsXCJ1bmlxdWVfaWRcIjpcIlRHVlV4czVYNjZ1OGZ3NHdVWkc3RlpZcDMyOFwiLFwidmFsdWVcIjpcInlvbG8hIVwifV0sXCJ0aXRsZVwiOlwiXFxuV2hhdCBoYXBwZW5zIGlmIEkgZG9uJ3QgZ2V0IHNlbGVjdGVkIGFmdGVyIHRoZSBmaXJzdCBIMUIgcmFmZmxlP1wiLFwidG90YWxfYW5zd2VyXCI6MCxcInRvdGFsX3ZvdGVcIjowLFwidW5pcXVlX2lkXCI6XCJRVWZmZkFTZE1rZ1Y4SjdTOFZoamd6Nkg1NTlcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNi8wMi8xMC9sbmhnaWFuZzkxLmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiQ2hlbHNlYVwiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIlZvXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdXNlci9sbmhnaWFuZzkxXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MCxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjIsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJsbmhnaWFuZzkxXCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6MzM5LFwib2NjdXBhdGlvblwiOlwiXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS8zMzlcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9sbmhnaWFuZzkxXCJ9fX0se1wiY29udGVudFwiOlwiXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0yOFQyMToyMjoyMy42MTY0NjdcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjE1NyxcImlzX3VzZXJfbG9naW5fYW5zd2VyXCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX2ZvbGxvd1wiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl92b3RlXCI6ZmFsc2UsXCJsYXRlc3RfYW5zd2VyXCI6e1wiY29udGVudFwiOlwid2hlcmUgZGlkIHlvdSBnbyB0byBzY2hvb2wgP1wiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMjlUMDI6MzU6MjIuMjUzNjU0XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwicXVlc3Rpb25cIjpcIlFVUGVRNW1Vc0xSYU5hU0huOXBtRGlQdjU5NVwiLFwidW5pcXVlX2lkXCI6XCJBTjVxTU1kSFhZSktGcTVKYWdGWDRGNFE5NzNcIixcInVzZXJfcG9zdF9mdWxsbmFtZVwiOlwiRHVjIFZ1XCIsXCJ1c2VyX3Bvc3RfdXNlcm5hbWVcIjpcInZ1dGhhbmhkdWM5MlwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVUGVRNW1Vc0xSYU5hU0huOXBtRGlQdjU5NVwiLFwidGFnc1wiOltdLFwidGl0bGVcIjpcIldoYXQgaXMgaXQgbGlrZSB0byBnbyB0byBhbiBlbGl0ZSBzY2hvb2wgaW4gVVMgPyAjY29sbGVnZVwiLFwidG90YWxfYW5zd2VyXCI6MixcInRvdGFsX3ZvdGVcIjo0LFwidW5pcXVlX2lkXCI6XCJRVVBlUTVtVXNMUmFOYVNIbjlwbURpUHY1OTVcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNi8wMi8yOC9oeWhpZXUuanBnXCIsXCJmaXJzdF9uYW1lXCI6XCJIaWV1XCIsXCJpc19hY3RpdmF0ZWRcIjpmYWxzZSxcImxhc3RfbmFtZVwiOlwiUGhhbVwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvaHloaWV1XCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MCxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJoeWhpZXVcIixcInVzZXJwcm9maWxlXCI6e1wiYmlvXCI6XCJcIixcImZhY2Vib29rX2lkXCI6bnVsbCxcImdlbmRlclwiOlwibVwiLFwiaWRcIjo1NTAsXCJvY2N1cGF0aW9uXCI6XCJcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9wcm9maWxlLzU1MFwiLFwidXNlclwiOlwiL2FwaS92MS91c2VyL2h5aGlldVwifX19LHtcImNvbnRlbnRcIjpcIlwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMjdUMTc6Mzc6MzguMDM1NjY1XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwiaWRcIjoxNTYsXCJpc191c2VyX2xvZ2luX2Fuc3dlclwiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl9mb2xsb3dcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fdm90ZVwiOmZhbHNlLFwibGF0ZXN0X2Fuc3dlclwiOntcImNvbnRlbnRcIjpcInlvdSBzaG91bGQgcHJlcGFyZSBtb3JlXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0yOFQxODoyOTowNC40NTM5NzlcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJxdWVzdGlvblwiOlwiUVVYODhxMjVYS3hmQUFHckNTWEVtUUxEMDI3XCIsXCJ1bmlxdWVfaWRcIjpcIkFOOG91UUpNMlVkclJXSnBxNEU1M2F5bTM5OFwiLFwidXNlcl9wb3N0X2Z1bGxuYW1lXCI6XCJEdWMgVnVcIixcInVzZXJfcG9zdF91c2VybmFtZVwiOlwidnV0aGFuaGR1YzkyXCIsXCJ2b3Rlc1wiOjB9LFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVVYODhxMjVYS3hmQUFHckNTWEVtUUxEMDI3XCIsXCJ0YWdzXCI6W3tcImlkXCI6NTEsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdGFnL1RHa3Y4a1dCckdWeFZWWmF2a0hTZ1JacDE2N1wiLFwidW5pcXVlX2lkXCI6XCJUR2t2OGtXQnJHVnhWVlphdmtIU2dSWnAxNjdcIixcInZhbHVlXCI6XCJlbnRyZXByZW5ldXJzaGlwXCJ9XSxcInRpdGxlXCI6XCJXaGF0IHNob3VsZCBJIGRvIHRvIHByZXBhcmUgZm9yIGNyZWF0aW5nIG5ldyBjb21wYW55IHNpbmNlIEkgYW0gc3RpbGwgaW4gVVMgc2Nob29sICNlbnRyZXByZW5ldXJzaGlwXCIsXCJ0b3RhbF9hbnN3ZXJcIjoxLFwidG90YWxfdm90ZVwiOjIsXCJ1bmlxdWVfaWRcIjpcIlFVWDg4cTI1WEt4ZkFBR3JDU1hFbVFMRDAyN1wiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS91cGxvYWRfc3RvcmFnZS9waG90by8yMDE2LzAyLzI2L3RlYW11bml2dG9wLmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiVGVhbVwiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIlVuaXZ0b3BcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS91c2VyL3RlYW11bml2dG9wXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6NixcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6OCxcInVzZXJuYW1lXCI6XCJ0ZWFtdW5pdnRvcFwiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcIkkgYW0gdW5pdnRvcCBzdGFyXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6NDI5LFwib2NjdXBhdGlvblwiOlwiSSBhbSBpbiBDYWxpZm9ybmlhIG5vd1wiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvNDI5XCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIvdGVhbXVuaXZ0b3BcIn19fSx7XCJjb250ZW50XCI6XCJpIGFtIGZyZXNobWFuIHdhbnQgdG8gYXBwbHkgZm9yIGFtYXpvbiBpbnRlcm5zaGlwXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0yNlQwMDo0ODo1NC41MzY4MTZcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjE1NSxcImlzX3VzZXJfbG9naW5fYW5zd2VyXCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX2ZvbGxvd1wiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl92b3RlXCI6ZmFsc2UsXCJsYXRlc3RfYW5zd2VyXCI6e1wiY29udGVudFwiOlwiSSBwcmVwYXJlZCB3aXRoIGRldGFpbHMgc2lkZSBwcm9qZWN0LCBuZXR3b3JraW5nIHdpdGggc2Nob29sLlwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMjlUMDI6MzI6MzAuMjc5OTI4XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwicXVlc3Rpb25cIjpcIlFVVEFHVHpUdWZYekd4NFNGeDdTaXBtdzc3MFwiLFwidW5pcXVlX2lkXCI6XCJBTnVKU25nS0U3RURLb005WTdrdXM3cWMxNzNcIixcInVzZXJfcG9zdF9mdWxsbmFtZVwiOlwiRGFuZyBOZ3V5ZW5cIixcInVzZXJfcG9zdF91c2VybmFtZVwiOlwiZHRuMTcxMjFcIixcInZvdGVzXCI6MH0sXCJxdWVzdGlvbl90eXBlXCI6XCJub3JtYWxcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9xdWVzdGlvbi9RVVRBR1R6VHVmWHpHeDRTRng3U2lwbXc3NzBcIixcInRhZ3NcIjpbe1wiaWRcIjo0OSxcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS90YWcvVEc5dU1ES3YzVnViaHFhYkNKOU1uN2NLNTQ5XCIsXCJ1bmlxdWVfaWRcIjpcIlRHOXVNREt2M1Z1YmhxYWJDSjlNbjdjSzU0OVwiLFwidmFsdWVcIjpcImFtYXpvblwifSx7XCJpZFwiOjUwLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3RhZy9URzU3VXRHUWlpTTVORWo4Wkd5RlF6aG42NjZcIixcInVuaXF1ZV9pZFwiOlwiVEc1N1V0R1FpaU01TkVqOFpHeUZRemhuNjY2XCIsXCJ2YWx1ZVwiOlwiZHRuMTcxMlwifV0sXCJ0aXRsZVwiOlwiaG93IGRpZCB5b3UgZ2V0IHlvdXIgZmlyc3Qgam9iIGluICNhbWF6b24gI2R0bjE3MTJcIixcInRvdGFsX2Fuc3dlclwiOjIsXCJ0b3RhbF92b3RlXCI6MyxcInVuaXF1ZV9pZFwiOlwiUVVUQUdUelR1Zlh6R3g0U0Z4N1NpcG13NzcwXCIsXCJ1c2VyX2xvZ2luX2Fuc3dlcl9jb250ZW50XCI6bnVsbCxcInVzZXJfbG9naW5fYW5zd2VyX3VuaXF1ZV9pZFwiOm51bGwsXCJ1c2VyX3Bvc3RcIjp7XCJhdmF0YXJcIjpcImh0dHBzOi8vdW5pdnRvcC5zMy5hbWF6b25hd3MuY29tL3VwbG9hZF9zdG9yYWdlL3Bob3RvLzIwMTYvMDIvMjgvdnV0aGFuaGR1YzkyLmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiRHVjXCIsXCJpc19hY3RpdmF0ZWRcIjpmYWxzZSxcImxhc3RfbmFtZVwiOlwiVnVcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS91c2VyL3Z1dGhhbmhkdWM5MlwiLFwidG90YWxfYW5zd2Vyc1wiOjksXCJ0b3RhbF9hc2tlZF9xdWVzdGlvbnNcIjoyLFwidG90YWxfZm9sbG93aW5nX3F1ZXN0aW9uc1wiOjE1LFwidXNlcm5hbWVcIjpcInZ1dGhhbmhkdWM5MlwiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcImRyZWFtZXIgY3VycmVudGx5IGluIGJheSBhcmVhXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6NDM4LFwib2NjdXBhdGlvblwiOlwiZHJleGVsIHVuaXZlcnNpdHlcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9wcm9maWxlLzQzOFwiLFwidXNlclwiOlwiL2FwaS92MS91c2VyL3Z1dGhhbmhkdWM5MlwifX19LHtcImNvbnRlbnRcIjpcIldoYXRzIHVwPyBAeW9sbyBAZHJleGVsXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0xOFQwOToyMDo1NS4xNzE2NzlcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjE1MSxcImlzX3VzZXJfbG9naW5fYW5zd2VyXCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX2ZvbGxvd1wiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl92b3RlXCI6ZmFsc2UsXCJsYXRlc3RfYW5zd2VyXCI6e1wiY29udGVudFwiOlwidGhlIHRhZyBzaG91bGQgd3JpdGUgbGlrZSAjeW9sbyAjaGVsbG9kcmV4ZWxcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTI2VDAwOjQ0OjA0LjUxMTUyMlwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcInF1ZXN0aW9uXCI6XCJRVVlxd2JOa1g4SGdQRnNEWnVHVnZOcDU2OTJcIixcInVuaXF1ZV9pZFwiOlwiQU5iUm9GanU3OEwyQnlXRjdWWFdQSjZiNTEwXCIsXCJ1c2VyX3Bvc3RfZnVsbG5hbWVcIjpcIkR1YyBWdVwiLFwidXNlcl9wb3N0X3VzZXJuYW1lXCI6XCJ2dXRoYW5oZHVjOTJcIixcInZvdGVzXCI6MH0sXCJxdWVzdGlvbl90eXBlXCI6XCJub3JtYWxcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9xdWVzdGlvbi9RVVlxd2JOa1g4SGdQRnNEWnVHVnZOcDU2OTJcIixcInRhZ3NcIjpbXSxcInRpdGxlXCI6XCJ3aGF0IHNob3VsZCBJIGRvIHRvIHRha2UgZmlyc3QgZmluYWwgZXhhbSBpbiBEcmV4ZWwgP1wiLFwidG90YWxfYW5zd2VyXCI6MSxcInRvdGFsX3ZvdGVcIjoxLFwidW5pcXVlX2lkXCI6XCJRVVlxd2JOa1g4SGdQRnNEWnVHVnZOcDU2OTJcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vZGVmYXVsdC9pbWcvdXNlci9tYWxlX2ljb24ucG5nXCIsXCJmaXJzdF9uYW1lXCI6XCJhbmhcIixcImlzX2FjdGl2YXRlZFwiOmZhbHNlLFwibGFzdF9uYW1lXCI6XCJsZVwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvcXV5bmgtYW5oXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MSxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJxdXluaC1hbmhcIixcInVzZXJwcm9maWxlXCI6e1wiYmlvXCI6XCJjb21wdXRlciBzY2llbmNlXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6NDM5LFwib2NjdXBhdGlvblwiOlwiZHJleGVsIHVuaXZlcnNpdHkgXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS80MzlcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9xdXluaC1hbmhcIn19fSx7XCJjb250ZW50XCI6XCJpIGFtIG5vdCBnb29kIGF0IGNoZW1pc3RyeSAuLi5cIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTE2VDIyOjIyOjQzLjczOTAyOVwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcImlkXCI6MTUwLFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjp7XCJjb250ZW50XCI6XCJIZWxsbyB3b3JsZCBcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTI5VDAwOjQ4OjA1LjU0MzU4OFwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcInF1ZXN0aW9uXCI6XCJRVWNrNmo5RFRGaHN5U2J1dDhzWVFGRWgxOTBcIixcInVuaXF1ZV9pZFwiOlwiQU5VMkd2YVFVSmk4RlNYTm9mWFZTa3h6NjM0XCIsXCJ1c2VyX3Bvc3RfZnVsbG5hbWVcIjpcIlRlYW0gVW5pdnRvcFwiLFwidXNlcl9wb3N0X3VzZXJuYW1lXCI6XCJ0ZWFtdW5pdnRvcFwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVY2s2ajlEVEZoc3lTYnV0OHNZUUZFaDE5MFwiLFwidGFnc1wiOlt7XCJpZFwiOjQ3LFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3RhZy9UR1FxbXNBQko3bnVpRFVRUlJXNFdSNGgzNjFcIixcInVuaXF1ZV9pZFwiOlwiVEdRcW1zQUJKN251aURVUVJSVzRXUjRoMzYxXCIsXCJ2YWx1ZVwiOlwiY2hlbWlzdHJ5XCJ9XSxcInRpdGxlXCI6XCJob3cgdG8gc3R1ZHkgbWlkdGVybSBlZmZlY3RpdmVseSAjY2hlbWlzdHJ5XCIsXCJ0b3RhbF9hbnN3ZXJcIjoyLFwidG90YWxfdm90ZVwiOjEsXCJ1bmlxdWVfaWRcIjpcIlFVY2s2ajlEVEZoc3lTYnV0OHNZUUZFaDE5MFwiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS91cGxvYWRfc3RvcmFnZS9waG90by8yMDE2LzAyLzI4L3Z1dGhhbmhkdWM5Mi5qcGdcIixcImZpcnN0X25hbWVcIjpcIkR1Y1wiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIlZ1XCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdXNlci92dXRoYW5oZHVjOTJcIixcInRvdGFsX2Fuc3dlcnNcIjo5LFwidG90YWxfYXNrZWRfcXVlc3Rpb25zXCI6MixcInRvdGFsX2ZvbGxvd2luZ19xdWVzdGlvbnNcIjoxNSxcInVzZXJuYW1lXCI6XCJ2dXRoYW5oZHVjOTJcIixcInVzZXJwcm9maWxlXCI6e1wiYmlvXCI6XCJkcmVhbWVyIGN1cnJlbnRseSBpbiBiYXkgYXJlYVwiLFwiZmFjZWJvb2tfaWRcIjpudWxsLFwiZ2VuZGVyXCI6XCJtXCIsXCJpZFwiOjQzOCxcIm9jY3VwYXRpb25cIjpcImRyZXhlbCB1bml2ZXJzaXR5XCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS80MzhcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci92dXRoYW5oZHVjOTJcIn19fSx7XCJjb250ZW50XCI6XCJIZWxsbywgbXkgbmFtZSBpcyBBblwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMTZUMTM6NDY6MjMuMTcyMjY2XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwiaWRcIjoxNDksXCJpc191c2VyX2xvZ2luX2Fuc3dlclwiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl9mb2xsb3dcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fdm90ZVwiOmZhbHNlLFwibGF0ZXN0X2Fuc3dlclwiOntcImNvbnRlbnRcIjpcIklzIHRlc3RpbmcgZ29vZCBoZWxsbyA/IFllc1wiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMjlUMDA6NTk6NTkuNjc3NDY5XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwicXVlc3Rpb25cIjpcIlFVQTNMWHV5RTdhWGdpcXJpRGgzQTVreDI1MFwiLFwidW5pcXVlX2lkXCI6XCJBTnhud3QzWTg5cEpSb29ZNnBlTDNEa2MyMzZcIixcInVzZXJfcG9zdF9mdWxsbmFtZVwiOlwiVGVhbSBVbml2dG9wXCIsXCJ1c2VyX3Bvc3RfdXNlcm5hbWVcIjpcInRlYW11bml2dG9wXCIsXCJ2b3Rlc1wiOjB9LFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVVBM0xYdXlFN2FYZ2lxcmlEaDNBNWt4MjUwXCIsXCJ0YWdzXCI6W10sXCJ0aXRsZVwiOlwiSGVsbG9cIixcInRvdGFsX2Fuc3dlclwiOjIsXCJ0b3RhbF92b3RlXCI6MixcInVuaXF1ZV9pZFwiOlwiUVVBM0xYdXlFN2FYZ2lxcmlEaDNBNWt4MjUwXCIsXCJ1c2VyX2xvZ2luX2Fuc3dlcl9jb250ZW50XCI6bnVsbCxcInVzZXJfbG9naW5fYW5zd2VyX3VuaXF1ZV9pZFwiOm51bGwsXCJ1c2VyX3Bvc3RcIjp7XCJhdmF0YXJcIjpcImh0dHBzOi8vdW5pdnRvcC5zMy5hbWF6b25hd3MuY29tL2RlZmF1bHQvaW1nL3VzZXIvbWFsZV9pY29uLnBuZ1wiLFwiZmlyc3RfbmFtZVwiOlwiQW5cIixcImlzX2FjdGl2YXRlZFwiOmZhbHNlLFwibGFzdF9uYW1lXCI6XCJIb1wiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvcGhhdHR1dHVoYW5oXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MCxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJwaGF0dHV0dWhhbmhcIixcInVzZXJwcm9maWxlXCI6e1wiYmlvXCI6XCJcIixcImZhY2Vib29rX2lkXCI6bnVsbCxcImdlbmRlclwiOlwibVwiLFwiaWRcIjo1MTUsXCJvY2N1cGF0aW9uXCI6XCJcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9wcm9maWxlLzUxNVwiLFwidXNlclwiOlwiL2FwaS92MS91c2VyL3BoYXR0dXR1aGFuaFwifX19LHtcImNvbnRlbnRcIjpcIkhlbGxvIHdvcmxkXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0xNlQwMDozMzoyMy4zOTQ4NTlcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjE0OCxcImlzX3VzZXJfbG9naW5fYW5zd2VyXCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX2ZvbGxvd1wiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl92b3RlXCI6ZmFsc2UsXCJsYXRlc3RfYW5zd2VyXCI6e1wiY29udGVudFwiOlwic2hvdWxkIG5vdCBiZSBjcyBtYWpvciwgc28gc3RyZXNzZnVsIDooXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0xNlQwMDozNDowMy4yMzczNjlcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJxdWVzdGlvblwiOlwiUVVVa3g2TE00ODZta05WWDN2aVNGbTJtOTkyXCIsXCJ1bmlxdWVfaWRcIjpcIkFOQk5jVVVaVXh3bVVGSDdaWlNLV1lwazU4OVwiLFwidXNlcl9wb3N0X2Z1bGxuYW1lXCI6XCJEdWMgVnVcIixcInVzZXJfcG9zdF91c2VybmFtZVwiOlwidnV0aGFuaGR1YzkyXCIsXCJ2b3Rlc1wiOjF9LFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVVVa3g2TE00ODZta05WWDN2aVNGbTJtOTkyXCIsXCJ0YWdzXCI6W10sXCJ0aXRsZVwiOlwiV2hhdCBpcyB0aGUgYmVzdCBtYWpvclwiLFwidG90YWxfYW5zd2VyXCI6MSxcInRvdGFsX3ZvdGVcIjoxLFwidW5pcXVlX2lkXCI6XCJRVVVreDZMTTQ4Nm1rTlZYM3ZpU0ZtMm05OTJcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNi8wMi8xNS9kdG4xNzEyMS5qcGdcIixcImZpcnN0X25hbWVcIjpcIkRhbmdcIixcImlzX2FjdGl2YXRlZFwiOmZhbHNlLFwibGFzdF9uYW1lXCI6XCJOZ3V5ZW5cIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS91c2VyL2R0bjE3MTIxXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MixcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MSxcInVzZXJuYW1lXCI6XCJkdG4xNzEyMVwiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcIlwiLFwiZmFjZWJvb2tfaWRcIjpudWxsLFwiZ2VuZGVyXCI6XCJtXCIsXCJpZFwiOjQxOSxcIm9jY3VwYXRpb25cIjpcIlwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvNDE5XCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIvZHRuMTcxMjFcIn19fSx7XCJjb250ZW50XCI6XCJAVmlzYSBAVVNcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTE0VDE0OjM5OjQ0LjEwNzk3OVwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcImlkXCI6MTQ3LFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjp7XCJjb250ZW50XCI6XCJoMWIgYXBwbGllcyBmb3IgNiB5ZWFycyBlbXBsb3ltZW50IGFuZCB5b3UgY2FuIHRyYW5zZmVyIHlvdXIgaDFiIFwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDItMTZUMjA6MjI6MTguMTcwOTQyXCIsXCJlZGl0X2RhdGVcIjpudWxsLFwicXVlc3Rpb25cIjpcIlFVWjZ5ZlVhTHA4TWJlQzhTcWlFY0dYSDU5OVwiLFwidW5pcXVlX2lkXCI6XCJBTm1hOFkzdUdMeFV6dUJ2VldpUXVqd2U2NTRcIixcInVzZXJfcG9zdF9mdWxsbmFtZVwiOlwiRHVjIFZ1XCIsXCJ1c2VyX3Bvc3RfdXNlcm5hbWVcIjpcInZ1dGhhbmhkdWM5MlwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVWjZ5ZlVhTHA4TWJlQzhTcWlFY0dYSDU5OVwiLFwidGFnc1wiOltdLFwidGl0bGVcIjpcIldoYXQgaXMgSDFCIHZpc2EgYW5kIGl0cyByZXF1aXJlbWVudHM/XCIsXCJ0b3RhbF9hbnN3ZXJcIjoxLFwidG90YWxfdm90ZVwiOjEsXCJ1bmlxdWVfaWRcIjpcIlFVWjZ5ZlVhTHA4TWJlQzhTcWlFY0dYSDU5OVwiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS9kZWZhdWx0L2ltZy91c2VyL21hbGVfaWNvbi5wbmdcIixcImZpcnN0X25hbWVcIjpcIkFuaFwiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIk5ndXllblwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvbmd1eWVuX2R1b25nX2hhX2FuaFwiLFwidG90YWxfYW5zd2Vyc1wiOjAsXCJ0b3RhbF9hc2tlZF9xdWVzdGlvbnNcIjoxLFwidG90YWxfZm9sbG93aW5nX3F1ZXN0aW9uc1wiOjAsXCJ1c2VybmFtZVwiOlwibmd1eWVuX2R1b25nX2hhX2FuaFwiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcIlwiLFwiZmFjZWJvb2tfaWRcIjpudWxsLFwiZ2VuZGVyXCI6XCJtXCIsXCJpZFwiOjQ4NixcIm9jY3VwYXRpb25cIjpcIlwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvNDg2XCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIvbmd1eWVuX2R1b25nX2hhX2FuaFwifX19LHtcImNvbnRlbnRcIjpcIkkgd2FudCBnZXQgdG8ga25vdyBtb3JlIGNvLXdvcmtlciB0byBkaXNjdXNzIGFib3V0IGNoZW1pc3RyeS5cIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTE0VDAxOjAwOjA0Ljk0MjU3OVwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcImlkXCI6MTQ2LFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjpudWxsLFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVV3cG9vM0t2ZDhObk5SQ3Y5NmE4WjJxMzMxXCIsXCJ0YWdzXCI6W3tcImlkXCI6NDcsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdGFnL1RHUXFtc0FCSjdudWlEVVFSUlc0V1I0aDM2MVwiLFwidW5pcXVlX2lkXCI6XCJUR1FxbXNBQko3bnVpRFVRUlJXNFdSNGgzNjFcIixcInZhbHVlXCI6XCJjaGVtaXN0cnlcIn0se1wiaWRcIjo0OCxcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS90YWcvVEdOU0xxRHpzcDdZUHJMcFlrblVTcEVqNTAxXCIsXCJ1bmlxdWVfaWRcIjpcIlRHTlNMcUR6c3A3WVByTHBZa25VU3BFajUwMVwiLFwidmFsdWVcIjpcImV4cGVydFwifV0sXCJ0aXRsZVwiOlwiSG93IGNhbiBpIG1lZXQgbW9yZSAjY2hlbWlzdHJ5ICNleHBlcnRcIixcInRvdGFsX2Fuc3dlclwiOjAsXCJ0b3RhbF92b3RlXCI6MyxcInVuaXF1ZV9pZFwiOlwiUVV3cG9vM0t2ZDhObk5SQ3Y5NmE4WjJxMzMxXCIsXCJ1c2VyX2xvZ2luX2Fuc3dlcl9jb250ZW50XCI6bnVsbCxcInVzZXJfbG9naW5fYW5zd2VyX3VuaXF1ZV9pZFwiOm51bGwsXCJ1c2VyX3Bvc3RcIjp7XCJhdmF0YXJcIjpcImh0dHBzOi8vdW5pdnRvcC5zMy5hbWF6b25hd3MuY29tL3VwbG9hZF9zdG9yYWdlL3Bob3RvLzIwMTYvMDIvMTQvbHV1aHV5bmhhbjIzMDYuanBnXCIsXCJmaXJzdF9uYW1lXCI6XCJLZXZpblwiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIkx1dVwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvbHV1aHV5bmhhbjIzMDZcIixcInRvdGFsX2Fuc3dlcnNcIjowLFwidG90YWxfYXNrZWRfcXVlc3Rpb25zXCI6MSxcInRvdGFsX2ZvbGxvd2luZ19xdWVzdGlvbnNcIjoxLFwidXNlcm5hbWVcIjpcImx1dWh1eW5oYW4yMzA2XCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiQ2hlbWlzdHJ5XCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6NDg1LFwib2NjdXBhdGlvblwiOlwiU3RhbmZvcmQgVW5pdmVyc2l0eVwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvNDg1XCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIvbHV1aHV5bmhhbjIzMDZcIn19fSx7XCJjb250ZW50XCI6XCJcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTEzVDE0OjA3OjMzLjU3NDQ1OVwiLFwiZWRpdF9kYXRlXCI6XCIyMDE2LTAzLTE0VDAxOjIxOjEwXCIsXCJpZFwiOjE0NSxcImlzX3VzZXJfbG9naW5fYW5zd2VyXCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX2ZvbGxvd1wiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl92b3RlXCI6ZmFsc2UsXCJsYXRlc3RfYW5zd2VyXCI6e1wiY29udGVudFwiOlwiSGVsbG8gd29ybGQgXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMi0yOVQwMTozODoyOC4zODcxMjJcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJxdWVzdGlvblwiOlwiUVVoTGhDRjVCY0tWYURkQlNyTVdpbzNhMjE5XCIsXCJ1bmlxdWVfaWRcIjpcIkFONkJtMlJiak1vUUQ4ZmVkaUJEUVZEbjc5NVwiLFwidXNlcl9wb3N0X2Z1bGxuYW1lXCI6XCJUZWFtIFVuaXZ0b3BcIixcInVzZXJfcG9zdF91c2VybmFtZVwiOlwidGVhbXVuaXZ0b3BcIixcInZvdGVzXCI6MH0sXCJxdWVzdGlvbl90eXBlXCI6XCJub3JtYWxcIixcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS9xdWVzdGlvbi9RVWhMaENGNUJjS1ZhRGRCU3JNV2lvM2EyMTlcIixcInRhZ3NcIjpbXSxcInRpdGxlXCI6XCJIb3cgZG8geW91IG1ha2UgZnJpZW5kcyBvbiBjYW1wdXM/IFwiLFwidG90YWxfYW5zd2VyXCI6MSxcInRvdGFsX3ZvdGVcIjoyLFwidW5pcXVlX2lkXCI6XCJRVWhMaENGNUJjS1ZhRGRCU3JNV2lvM2EyMTlcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNi8wMi8xMy9BbmhfbHVvbmc5My5qcGdcIixcImZpcnN0X25hbWVcIjpcIkFuaCBcIixcImlzX2FjdGl2YXRlZFwiOmZhbHNlLFwibGFzdF9uYW1lXCI6XCJMLlwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvQW5oX2x1b25nOTNcIixcInRvdGFsX2Fuc3dlcnNcIjowLFwidG90YWxfYXNrZWRfcXVlc3Rpb25zXCI6MSxcInRvdGFsX2ZvbGxvd2luZ19xdWVzdGlvbnNcIjowLFwidXNlcm5hbWVcIjpcIkFuaF9sdW9uZzkzXCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6NDgwLFwib2NjdXBhdGlvblwiOlwiXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS80ODBcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9BbmhfbHVvbmc5M1wifX19LHtcImNvbnRlbnRcIjpcIlwiLFwiY3JlYXRlX2RhdGVcIjpcIjIwMTYtMDEtMTNUMjA6MzM6MjMuNjU3NzA5XCIsXCJlZGl0X2RhdGVcIjpudWxsLFwiaWRcIjoxMzAsXCJpc191c2VyX2xvZ2luX2Fuc3dlclwiOmZhbHNlLFwiaXNfdXNlcl9sb2dpbl9mb2xsb3dcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fdm90ZVwiOmZhbHNlLFwibGF0ZXN0X2Fuc3dlclwiOntcImNvbnRlbnRcIjpcIkhlbGxvIGhhbyBcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTI5VDAxOjM4OjQyLjM0ODkwMVwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcInF1ZXN0aW9uXCI6XCJRVXlwaVpMUWhhWUdhaHhLY25tMnJwcFE3ODZcIixcInVuaXF1ZV9pZFwiOlwiQU5IY3ozMjhXd0w4ZGpIaGF2aHdnN2trMTcxXCIsXCJ1c2VyX3Bvc3RfZnVsbG5hbWVcIjpcIlRlYW0gVW5pdnRvcFwiLFwidXNlcl9wb3N0X3VzZXJuYW1lXCI6XCJ0ZWFtdW5pdnRvcFwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVeXBpWkxRaGFZR2FoeEtjbm0ycnBwUTc4NlwiLFwidGFnc1wiOltdLFwidGl0bGVcIjpcIkhhbyBhc2tlZC4gXCIsXCJ0b3RhbF9hbnN3ZXJcIjoyLFwidG90YWxfdm90ZVwiOjEsXCJ1bmlxdWVfaWRcIjpcIlFVeXBpWkxRaGFZR2FoeEtjbm0ycnBwUTc4NlwiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS91cGxvYWRfc3RvcmFnZS9waG90by8yMDE1LzEyLzE5L3dpdGhhbF8wMi5qcGdcIixcImZpcnN0X25hbWVcIjpcIkhhb1wiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIld1XCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdXNlci93aXRoYWxfMDJcIixcInRvdGFsX2Fuc3dlcnNcIjowLFwidG90YWxfYXNrZWRfcXVlc3Rpb25zXCI6MSxcInRvdGFsX2ZvbGxvd2luZ19xdWVzdGlvbnNcIjowLFwidXNlcm5hbWVcIjpcIndpdGhhbF8wMlwiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcIlwiLFwiZmFjZWJvb2tfaWRcIjpudWxsLFwiZ2VuZGVyXCI6XCJtXCIsXCJpZFwiOjM0MCxcIm9jY3VwYXRpb25cIjpcIlwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvMzQwXCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIvd2l0aGFsXzAyXCJ9fX0se1wiY29udGVudFwiOlwiSSBkb24ndCBrbm93IGhvdyB0byB1c2UgdGhpcyBhcHAuXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNi0wMS0xMVQxMDoyMDo0MC44ODkxODhcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjgzLFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjp7XCJjb250ZW50XCI6XCJIZWxsbyB3b3JsZCA/Pz9cIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTI5VDAxOjM5OjIxLjA0NzUyMFwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcInF1ZXN0aW9uXCI6XCJRVVVUTlpxVEpCSGFMQmZEcENLNjc5QlczMjNcIixcInVuaXF1ZV9pZFwiOlwiQU5Dbkg2TTZhZTNveGRqeWg1bmZQcnNUNjM4XCIsXCJ1c2VyX3Bvc3RfZnVsbG5hbWVcIjpcIlRlYW0gVW5pdnRvcFwiLFwidXNlcl9wb3N0X3VzZXJuYW1lXCI6XCJ0ZWFtdW5pdnRvcFwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVVVROWnFUSkJIYUxCZkRwQ0s2NzlCVzMyM1wiLFwidGFnc1wiOltdLFwidGl0bGVcIjpcIkhvdyB0byB1c2UgdGhpcyBhcHA/XCIsXCJ0b3RhbF9hbnN3ZXJcIjozLFwidG90YWxfdm90ZVwiOjIsXCJ1bmlxdWVfaWRcIjpcIlFVVVROWnFUSkJIYUxCZkRwQ0s2NzlCVzMyM1wiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS91cGxvYWRfc3RvcmFnZS9waG90by8yMDE1LzEyLzA2L2hhbmd0cmFuMTM1LmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiSGFpbGV5XCIsXCJpc19hY3RpdmF0ZWRcIjpmYWxzZSxcImxhc3RfbmFtZVwiOlwiVHJhblwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIvaGFuZ3RyYW4xMzVcIixcInRvdGFsX2Fuc3dlcnNcIjowLFwidG90YWxfYXNrZWRfcXVlc3Rpb25zXCI6MSxcInRvdGFsX2ZvbGxvd2luZ19xdWVzdGlvbnNcIjowLFwidXNlcm5hbWVcIjpcImhhbmd0cmFuMTM1XCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiR3JhcGhpYyBEZXNpZ25lciBhbmQgUGhvdG9ncmFwaGVyXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6MzMxLFwib2NjdXBhdGlvblwiOlwiQ29sdW1iaWEgQ29sbGVnZSBDaGljYWdvXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS8zMzFcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9oYW5ndHJhbjEzNVwifX19LHtcImNvbnRlbnRcIjpcIkhlbGxvXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNS0xMi0yNVQwOTo0OToyMy4yMTc2MTRcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjc4LFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjp7XCJjb250ZW50XCI6XCJIZWxsbyB3b3JsZCBcIixcImNyZWF0ZV9kYXRlXCI6XCIyMDE2LTAyLTI5VDAwOjMyOjE0LjM5ODEzOFwiLFwiZWRpdF9kYXRlXCI6bnVsbCxcInF1ZXN0aW9uXCI6XCJRVWs3enJlZXVFbkc5NlFuQWhuTXg0cG04NzZcIixcInVuaXF1ZV9pZFwiOlwiQU5XMnNHWTVaQ3N4RFd5TWVoQUVKNjVFMDEzXCIsXCJ1c2VyX3Bvc3RfZnVsbG5hbWVcIjpcIlRlYW0gVW5pdnRvcFwiLFwidXNlcl9wb3N0X3VzZXJuYW1lXCI6XCJ0ZWFtdW5pdnRvcFwiLFwidm90ZXNcIjowfSxcInF1ZXN0aW9uX3R5cGVcIjpcIm5vcm1hbFwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3F1ZXN0aW9uL1FVazd6cmVldUVuRzk2UW5BaG5NeDRwbTg3NlwiLFwidGFnc1wiOltdLFwidGl0bGVcIjpcIkhlbGxvXCIsXCJ0b3RhbF9hbnN3ZXJcIjoyLFwidG90YWxfdm90ZVwiOjEsXCJ1bmlxdWVfaWRcIjpcIlFVazd6cmVldUVuRzk2UW5BaG5NeDRwbTg3NlwiLFwidXNlcl9sb2dpbl9hbnN3ZXJfY29udGVudFwiOm51bGwsXCJ1c2VyX2xvZ2luX2Fuc3dlcl91bmlxdWVfaWRcIjpudWxsLFwidXNlcl9wb3N0XCI6e1wiYXZhdGFyXCI6XCJodHRwczovL3VuaXZ0b3AuczMuYW1hem9uYXdzLmNvbS9kZWZhdWx0L2ltZy91c2VyL21hbGVfaWNvbi5wbmdcIixcImZpcnN0X25hbWVcIjpcIllpbmdzaGlcIixcImlzX2FjdGl2YXRlZFwiOmZhbHNlLFwibGFzdF9uYW1lXCI6XCJaaGFuZ1wiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3VzZXIveWluZ3NoaXpoYW5nN1wiLFwidG90YWxfYW5zd2Vyc1wiOjAsXCJ0b3RhbF9hc2tlZF9xdWVzdGlvbnNcIjoxLFwidG90YWxfZm9sbG93aW5nX3F1ZXN0aW9uc1wiOjAsXCJ1c2VybmFtZVwiOlwieWluZ3NoaXpoYW5nN1wiLFwidXNlcnByb2ZpbGVcIjp7XCJiaW9cIjpcIlwiLFwiZmFjZWJvb2tfaWRcIjpudWxsLFwiZ2VuZGVyXCI6XCJtXCIsXCJpZFwiOjM1NyxcIm9jY3VwYXRpb25cIjpcIlwiLFwicmVzb3VyY2VfdXJpXCI6XCIvYXBpL3YxL3Byb2ZpbGUvMzU3XCIsXCJ1c2VyXCI6XCIvYXBpL3YxL3VzZXIveWluZ3NoaXpoYW5nN1wifX19LHtcImNvbnRlbnRcIjpcIlF1ZXN0aW9uIGRldGFpbHMgLi4uXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNS0xMi0xOFQyMjo1NzoyMC45ODIxOThcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjUxLFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjpudWxsLFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVV3UTVxVHhVcnR1SGc1RzdMNVdaaUFIMzgzXCIsXCJ0YWdzXCI6W3tcImlkXCI6MSxcInJlc291cmNlX3VyaVwiOlwiL2FwaS92MS90YWcvVEdIWW5MS1Z0YlFDZFBRWUNIc1A0a1BGOTMxXCIsXCJ1bmlxdWVfaWRcIjpcIlRHSFluTEtWdGJRQ2RQUVlDSHNQNGtQRjkzMVwiLFwidmFsdWVcIjpcInVuaXRlZHN0YXRlc1wifV0sXCJ0aXRsZVwiOlwiaG93IGNhbiAgI3VuaXRlZHN0YXRlc1wiLFwidG90YWxfYW5zd2VyXCI6MCxcInRvdGFsX3ZvdGVcIjoyLFwidW5pcXVlX2lkXCI6XCJRVXdRNXFUeFVydHVIZzVHN0w1V1ppQUgzODNcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNS8xMi8xOS9hbmd1eWVuLmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiQW5oXCIsXCJpc19hY3RpdmF0ZWRcIjpmYWxzZSxcImxhc3RfbmFtZVwiOlwiTmd1eWVuXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdXNlci9hbmd1eWVuXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MCxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjEsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJhbmd1eWVuXCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6MzQ2LFwib2NjdXBhdGlvblwiOlwiXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS8zNDZcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9hbmd1eWVuXCJ9fX0se1wiY29udGVudFwiOlwiXCIsXCJjcmVhdGVfZGF0ZVwiOlwiMjAxNS0xMi0xN1QxNDo1ODoxNC42NzI4MjVcIixcImVkaXRfZGF0ZVwiOm51bGwsXCJpZFwiOjQ5LFwiaXNfdXNlcl9sb2dpbl9hbnN3ZXJcIjpmYWxzZSxcImlzX3VzZXJfbG9naW5fZm9sbG93XCI6ZmFsc2UsXCJpc191c2VyX2xvZ2luX3ZvdGVcIjpmYWxzZSxcImxhdGVzdF9hbnN3ZXJcIjpudWxsLFwicXVlc3Rpb25fdHlwZVwiOlwibm9ybWFsXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcXVlc3Rpb24vUVUyS3hoRG5RWVk0eTlteHI3aU5TZ2ZINzEzXCIsXCJ0YWdzXCI6W10sXCJ0aXRsZVwiOlwiSGkhISBXb3VsZCBsb3ZlIHRvIGxlYXJuIG1vcmUgYWJvdXQgdGhlIGNvLW9wIHByb2dyYW1zIGF0IERyZXhlbCBhbmQgTm9ydGhlYXN0ZXJuIVwiLFwidG90YWxfYW5zd2VyXCI6MCxcInRvdGFsX3ZvdGVcIjoyLFwidW5pcXVlX2lkXCI6XCJRVTJLeGhEblFZWTR5OW14cjdpTlNnZkg3MTNcIixcInVzZXJfbG9naW5fYW5zd2VyX2NvbnRlbnRcIjpudWxsLFwidXNlcl9sb2dpbl9hbnN3ZXJfdW5pcXVlX2lkXCI6bnVsbCxcInVzZXJfcG9zdFwiOntcImF2YXRhclwiOlwiaHR0cHM6Ly91bml2dG9wLnMzLmFtYXpvbmF3cy5jb20vdXBsb2FkX3N0b3JhZ2UvcGhvdG8vMjAxNi8wMi8xMC9sbmhnaWFuZzkxLmpwZ1wiLFwiZmlyc3RfbmFtZVwiOlwiQ2hlbHNlYVwiLFwiaXNfYWN0aXZhdGVkXCI6ZmFsc2UsXCJsYXN0X25hbWVcIjpcIlZvXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvdXNlci9sbmhnaWFuZzkxXCIsXCJ0b3RhbF9hbnN3ZXJzXCI6MCxcInRvdGFsX2Fza2VkX3F1ZXN0aW9uc1wiOjIsXCJ0b3RhbF9mb2xsb3dpbmdfcXVlc3Rpb25zXCI6MCxcInVzZXJuYW1lXCI6XCJsbmhnaWFuZzkxXCIsXCJ1c2VycHJvZmlsZVwiOntcImJpb1wiOlwiXCIsXCJmYWNlYm9va19pZFwiOm51bGwsXCJnZW5kZXJcIjpcIm1cIixcImlkXCI6MzM5LFwib2NjdXBhdGlvblwiOlwiXCIsXCJyZXNvdXJjZV91cmlcIjpcIi9hcGkvdjEvcHJvZmlsZS8zMzlcIixcInVzZXJcIjpcIi9hcGkvdjEvdXNlci9sbmhnaWFuZzkxXCJ9fX1dfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ2FqYXgvcmVxdWVzdCcsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFqYXhPcHRpb25zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiAnaHR0cDovL2RldmVsb3BtZW50LnVuaXZ0b3AuY29tL2FwaS92MS9xdWVzdGlvbj9mb3JtYXQ9anNvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge2xpbWl0OiAxMH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYoJ3N1Y2Nlc3MnID09PSBkYXRhLnN0YXR1cyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vcmVuZGVyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBodG1sID0gc3dpZy5yZW5kZXIoX3RoaXMudGVtcGxhdGVzLnRhYiwge2xvY2Fsczoge3F1ZXN0aW9uczogZGF0YS5vYmplY3RzfX0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0Lmh0bWwoaHRtbCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0LmRhdGEoJ2xvYWRpbmctc3RhdHVzJywgJ3N1Y2Nlc3MnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGFyZ2V0Lmh0bWwoXCJUaGVyZSdzIGFuIGVycm9yIHdoaWxlIGZldGNoaW5nIHRoZSBjb250ZW50XCIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkbWVudS5jaGlsZHJlbigpLmZpcnN0KCkudHJpZ2dlcignY2xpY2snKTtcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgX3RoaXMuZGF0YSA9IHt9O1xyXG4gICAgICAgIF90aGlzLnRlbXBsYXRlcyA9IHt9O1xyXG5cclxuICAgICAgICBfdGhpcy5oYW5kbGVGb3JtU2VhcmNoKCk7XHJcbiAgICAgICAgX3RoaXMuaGFuZGxlVGFicygpO1xyXG4gICAgfVxyXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gcGFnZVF1ZXN0aW9uczsiXX0=
