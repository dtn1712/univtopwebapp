import sys,os, shutil

PROJECT_PATH = os.path.abspath(__file__ + "/../../")
PROJECT_NAME = "univtop"

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

YUI_COMPRESSOR = PROJECT_NAME + "/libs/compressor/yuicompressor-2.4.8.jar"
HTML_COMPRESSOR = PROJECT_NAME  + "/libs/compressor/htmlcompressor-1.5.3.jar"
COMPRESSOR = {"html": HTML_COMPRESSOR, "css": YUI_COMPRESSOR, "js": YUI_COMPRESSOR}

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

def compress(in_files, out_file, in_type='js', verbose=False,
             temp_file='.temp'):
    temp = open(temp_file, 'w')
    for f in in_files:
        fh = open(f)
        data = fh.read() + '\n'
        fh.close()

        temp.write(data)

        print ' + %s' % f
    temp.close()

    options = ['-o "%s"' % out_file,
               '--type %s' % in_type]

    if in_type == "html":
        options.append("--compress-js")
        options.append("--compress-css")

    if verbose:
        options.append('-v')

    cmd = 'java -jar "%s" %s "%s"' % (COMPRESSOR[in_type],' '.join(options),temp_file)
    os.system(cmd)

    org_size = os.path.getsize(temp_file)
    new_size = os.path.getsize(out_file)

    print '=> %s' % out_file
    print 'Original: %.2f kB' % (org_size / 1024.0)
    print 'Compressed: %.2f kB' % (new_size / 1024.0)
    print 'Reduction: %.1f%%' % (float(org_size - new_size) / org_size * 100)
    print ''


def listFiles(rootdir,fileList):
    for root, subFolders, files in os.walk(rootdir):
        for file in files:
            f = os.path.join(root,file)
            fileList.append(f)


def copyRecursive(src,dest):
    if os.path.exists(dest):
        shutil.rmtree(dest)
    shutil.copytree(src,dest)


def readCatalogue(list_file,path,catalogue_type):
    catalogue_path = path
    catalogue_file = catalogue_type[0].upper() + "_" + CATALOGUE
    if catalogue_file not in path:
        catalogue_path = path + catalogue_file if path[len(path)-1] == "/" else path + "/" + catalogue_file
    f = open(catalogue_path,"r")
    for filename in f:
        if len(filename.replace("\n","")) != 0 and filename.startswith(COMMENT_CHARACTER) == False:
            list_file.append(path + filename.replace("\n","").strip())


# def setupFiles(list_file,path,static_type):
#     if os.path.exists(path + CATALOGUE):
#         readCatalogue(list_file,path,static_type)
#     else:
#         new_list_file = []
#         files = os.listdir(path)
#         for filename in files:
#             new_list_file.append(path + filename)
#         new_list_file.sort()
#         list_file += new_list_file


def cleanFolder():
    js_dest = PROJECT_PATH + "/assets/static/js/prod/"

    for filename in os.listdir(js_dest):
        if "js" in filename:
            os.remove(js_dest + filename)

    css_dest = PROJECT_PATH + "/assets/static/css/prod/"

    css_cleanup_dest = css_dest + "stylesheets/"
    for filename in os.listdir(css_cleanup_dest):
        if "css" in filename:
            os.remove(css_cleanup_dest + filename)


def setupStatic(static_type, built_version_id):

    js_dest = PROJECT_PATH + "/assets/static/js/prod/"


    common_scripts_src = PROJECT_PATH + "/assets/static/js/"
    scripts = []
    readCatalogue(scripts,common_scripts_src,static_type)
    common_scripts_out = PROJECT_PATH + "/assets/static/js/prod/" + PROJECT_NAME + ".script." + built_version_id + ".min.js"
    if not os.path.exists(PROJECT_PATH + "/assets/static/js/prod/"):
        os.makedirs(PROJECT_PATH + "/assets/static/js/prod/")
    compress(scripts,common_scripts_out,"js",False)

    # # Minify the javascript of each app
    # js_src = PROJECT_PATH + "/assets/static/js/apps/" + static_type + "/"
    # files = os.listdir(js_src)
    # files.sort()
    # for filename in files:
        # if ".js" not in filename:

            # dir_app = js_src + filename + "/"
            # if not os.path.exists(dir_app + "prod"):
            #     os.makedirs(dir_app + "prod")

            # subapp_plugin_scripts_out = None
            # if os.path.exists(dir_app + "plugins"):
            #     subapp_plugin_files = os.listdir(dir_app + "plugins")
            #     if len(subapp_plugin_files) > 0:
            #         subapp_plugin_scripts = []
            #         subapp_plugin_files.sort()
            #         for plugin_file in subapp_plugin_files:
            #             subapp_plugin_scripts.append(dir_app + "plugins/" + plugin_file)
            #         subapp_plugin_scripts_out = dir_app + "prod" + "/" + PROJECT_NAME + ".script.plugins.app.min.js"
            #         compress(subapp_plugin_scripts,subapp_plugin_scripts_out,"js",False)


            # subapp_scripts = [
            #     common_scripts_out,
            #     dir_app + "function.js",
            #     dir_app + "ajax.js",
            #     dir_app + "main.js",
            # ]

            # if subapp_plugin_scripts_out != None:
            #     subapp_scripts.insert(2,subapp_plugin_scripts_out)

            # subapp_scripts_out = dir_app + "prod/" + PROJECT_NAME + ".script." + built_version_id + ".min.js"
            # compress(subapp_scripts, subapp_scripts_out, 'js', False)

        # app_name = filename.replace(".js","")
        # app_scripts = [
        #     common_scripts_out,
        #     js_src + filename
        # ]

        # app_scripts_out = PROJECT_PATH + "/assets/static/js/prod/" + PROJECT_NAME + "." + app_name + ".script." + built_version_id + ".min.js"
        # compress(app_scripts, app_scripts_out, 'js', False)


    css_dest = PROJECT_PATH + "/assets/static/css/prod/"

    # Copy css resource
    css_resources_src = PROJECT_PATH + "/assets/static/css/resources/"
    resources_files = os.listdir(css_resources_src)
    for filename in resources_files:
        copyRecursive(css_resources_src + filename,css_dest + filename)

    # Minify the css file in catalogue 
    css_common_src = PROJECT_PATH + "/assets/static/css/"
    style_css = []
    readCatalogue(style_css,css_common_src,static_type)

    style_css_out = css_dest + "stylesheets/styles.min.css"
    if not os.path.exists(css_dest + "stylesheets/"):
        os.makedirs(css_dest + "stylesheets")
    compress(style_css, style_css_out, 'css')

    # List all processing css file
    css_subapp_src = PROJECT_PATH + "/assets/static/css/apps/" + static_type + "/"
    subapp_files = os.listdir(css_subapp_src)

    # Minify the css for each sub app combine with the global stylesheet
    for filename in subapp_files:
        subapp_css = [style_css_out,css_subapp_src + filename]
        subapp_css_out = css_dest + "stylesheets/" + filename[0:filename.rfind(".")] + "." + static_type + "." + built_version_id + ".min.css"
        compress(subapp_css, subapp_css_out, "css")


def main():
    print "...RUNNING SETUP ASSETS SCRIPT..."
    try:
        stage = sys.argv[1]
        built_version_id = sys.argv[2]
        if stage == "prod" or stage == "beta":
            cleanFolder()
            setupStatic("non_responsive",built_version_id)
            setupStatic("responsive",built_version_id)
            print "Setup assests successfully"
        else:
            print "No need to compress asset in the development mode"
    except Exception:
        print "Setup assets error"
        raise

if __name__ == "__main__":
    main()
