from django.conf.urls import *

from univtop.apps.member.views import ProfileView

urlpatterns = patterns('univtop.apps.member.views',
    url(r"^$", ProfileView.as_view(), name='profile'),
)
