from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, TemplateView
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.utils import timezone
from django.core import serializers

from univtop.apps.app_views import AppBaseView

import logging, json, datetime

logger = logging.getLogger(__name__)

APP_NAME = "member"


class ProfileView(AppBaseView,TemplateView):
	app_name = APP_NAME
	template_name = "profile"

	#@method_decorator(login_required)
	def dispatch(self, *args, **kwargs):
		return super(ProfileView, self).dispatch(*args, **kwargs)