from django.views.generic.base import TemplateResponseMixin, ContextMixin
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView

from univtop.apps.app_helper import get_template_path, handle_request_get_message
from univtop.apps.app_helper import capitalize_first_letter, get_user_login_object
from univtop.apps.app_helper import get_base_template_path
from univtop.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

import django_mobile

class AppBaseView(TemplateResponseMixin,ContextMixin):
	sub_path = "/page/"

	def get_context_data(self, **kwargs):
		context = super(AppBaseView, self).get_context_data(**kwargs)
		context['app_name'] = self.app_name
		context['page_type'] = self.app_name + "_" + self.template_name

		data = { 
			'user_login': get_user_login_object(self.request), 
			'site_name': capitalize_first_letter(SITE_NAME) 
		}
		context['request_message'] =  handle_request_get_message(self.request,data)
		context['template_type'] = "non_responsive"
		flavour = django_mobile.get_flavour(self.request)
		if flavour != None:
			context['template_type'] = "non_responsive" if flavour == "full" else "responsive"
	
		context['app_base_template'] = get_base_template_path(flavour,self.app_name)

		return context

	def get_template_names(self):
		return [get_template_path(self.app_name,self.template_name,django_mobile.get_flavour(self.request),self.sub_path)]
