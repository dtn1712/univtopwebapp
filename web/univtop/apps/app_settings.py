from univtop.settings import SITE_NAME, DEFAULT_TEMPLATE_FILE_EXTENSION

import datetime

LOGO_ICON_URL = "https://univtop.s3.amazonaws.com/img/ico/favicon.ico"

DEFAULT_LATITUDE = 47.616000
DEFAULT_LONGITUDE = -122.322464
DEFAULT_CITY = "Seattle"
DEFAULT_COUNTRY = "United States"
DEFAULT_COUNTRY_CODE = "US"
DEFAULT_TIMEZONE = "America/Los_Angeles"

SITE_DATA = {
  "SITE_NAME" : SITE_NAME,
  "SITE_DESCRIPTION": SITE_NAME[0].upper() + SITE_NAME[1:].lower() + " is a platform for international student to connect and share their experience",
  "TEMPLATE_FILE_EXTENSION": DEFAULT_TEMPLATE_FILE_EXTENSION,
  "DEFAULT_TIMEZONE": DEFAULT_TIMEZONE
}

KEYWORDS_URL = [
	'admin','signup','login','password',"accounts"
	'logout','confirm_email','searXch','settings',
	'buzz','messages',"about",'api','asset','photo',
	'feeds','friends'
]

MODEL_KEY_LIST = [
  "EM","OR", "PJ", "PA", "IV", "PT", "CM", "CN", "RP", "NF", "MG", "FD", "DI", "UN"
]

MODEL_KEY_MAP = {
  "email": "EM",
  "question": "QU",
  "answer": "AN",
  "tag": "TG",
  "school": "SC",
  "photo": "PT",
  "comment": "CM",
  "report": "RP",
  "notification": "NF",
  "vote": "VT",
  "default_image": "DI",
}

MODEL_KEY_REVERSE_MAP = {
  "QU": "question",
  "AN": "answer",
  "TG": "tag",
  "SC": "school",
  "PT": "photo",
  "CM": "comment",
  "RP": "report",
  "NF": "notification",
  "DI": "default_image",
}

UNSPECIFIED_MODEL_KEY = "UN"


DEFAULT_IMAGE_PATH_MAPPING = {
  # User Image
  "default_male_icon": "default/img/user/male_icon.png",
  "default_female_icon": "default/img/user/female_icon.png",
  "default_cover_picture": 'default/img/user/cover_picture.png',
  "default_project_picture": 'default/img/user/project_picture.png',
  "default_project_activity_picture": 'default/img/user/project_activity_picture.png',
}


DEFAULT_IMAGE_UNIQUE_ID = {
  "default_male_icon": "DIBHgn2pXkaWLAYgpRsQGTo3088",
  "default_female_icon": "DIJE4S63KnuKozEq4BsC2PH4019",
  "default_cover_picture": 'DInXsK7dXR9BGXpmKR9Mhd6D124',
  "default_project_picture": 'DInXsK7dXR9BGXpmKR9Mhd7B89',
  "default_project_activity_picture": 'DInXsK7dXR9BGXpmKR9Mhd2M33',
}

MESSAGE_SNIPPET_TEMPLATE =  { 
  # Auth app
  "signup_success": "messages/apps/auth/signup_success.html",
  "confirm_email_success": "messages/apps/auth/confirm_email_success.html",
  "confirm_email_asking": "messages/apps/auth/confirm_email_asking.html",
  "resend_confirm_email_success": "messages/apps/auth/resend_confirm_email_success.html",
  "resend_confirm_email_error": "messages/apps/auth/resend_confirm_email_error.html",
  "change_password_success": "messages/apps/auth/change_password_success.html",
  "social_login_error": "messages/apps/auth/social_login_error.html",
  "reset_password_success": "messages/apps/auth/reset_password_success.html",
}

HTML_SNIPPET_TEMPLATE = {
  

}

EMAIL_SUBJECT_SNIPPET_TEMPLATE = {
  "basic": "emails/apps/main/basic_subject.txt",
  "confirmation_link": "emails/apps/auth/confirmation_link_subject.txt",
  "confirmation_link_signup": "emails/apps/auth/confirmation_link_signup_subject.txt",
  "confirmation_passcode": "emails/apps/auth/confirmation_passcode_subject.txt",
  "confirmation_passcode_signup": "emails/apps/auth/confirmation_passcode_signup_subject.txt",
  "password_reset": "emails/apps/auth/password_reset_subject.txt",
}

EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE = {
  "basic": "emails/apps/main/basic_content.html",
  "confirmation_link": "emails/apps/auth/confirmation_link_content.html",
  "confirmation_link_signup": "emails/apps/auth/confirmation_link_signup_content.html",
  "confirmation_passcode": "emails/apps/auth/confirmation_passcode_content.html",
  "confirmation_passcode_signup": "emails/apps/auth/confirmation_passcode_signup_content.html",
  "password_reset": "emails/apps/auth/password_reset_content.html",
}

EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE = {
  "basic": "emails/apps/main/basic_content.txt",
  "confirmation_link": "emails/apps/auth/confirmation_link_content.txt",
  "confirmation_link_signup": "emails/apps/auth/confirmation_link_signup_content.txt",
  "confirmation_passcode": "emails/apps/auth/confirmation_passcode_content.txt",
  "confirmation_passcode_signup": "emails/apps/auth/confirmation_passcode_signup_content.txt",
  "password_reset": "emails/apps/auth/password_reset_content.txt",
}

NOTIFICATION_SNIPPET_TEMPLATE = {
  "one_answer" : "notifications/apps/question/one_answer_for_question.txt",
  "many_answers" : "notifications/apps/question/many_answers_for_questions.txt",
  "notification_tag" : "notifications/apps/tag/notification_tag.txt",
}


