from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from univtop.apps.main.models import *


# class UserProfileAdmin(admin.ModelAdmin):
#     list_display = ['user','avatar','privacy_status']

# class QuestionAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','title','content','question_type','user_post']

# class AnswerAdmin(admin.ModelAdmin):
# 	list_display = ['unique_id','content','question','user_post','create_date','edit_date']

# class TagAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','value']

# class UserModelAdmin(UserAdmin):
#     inlines = UserAdmin.inlines + [ApiKeyInline]

# class VoteAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','user_vote','vote_type',"object_id"]

# class ReportAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','report_type','object_id','user_post','date','report_content']

# class CommentAdmin(admin.ModelAdmin):
# 	list_display = ['unique_id','comment_type','object_id','user_post','content','create_date','edit_date']

# class PhotoAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','caption','image','thumbnail','upload_time','user_post','photo_type','object_unique_id']

# class NotificationAdmin(admin.ModelAdmin):
#     list_display = ['unique_id','content','status','notify_to','user_post','date','notification_type']


# admin.site.register(UserProfile, UserProfileAdmin)
# admin.site.register(Question ,QuestionAdmin)
# admin.site.register(Tag ,TagAdmin)
# admin.site.register(Answer ,AnswerAdmin)
# admin.site.register(Vote ,VoteAdmin)
# admin.site.register(Report ,ReportAdmin)
# admin.site.register(Comment ,CommentAdmin)
# admin.site.register(Photo, PhotoAdmin)
# admin.site.register(Notification, NotificationAdmin)



