from django.conf.urls import *
from django.views.generic import RedirectView

from univtop.apps.about.views import ContactView, AboutView

urlpatterns = patterns('univtop.apps.about.views',
	url(r'^contact$', ContactView.as_view(), name='contact'),
	url(r'^contact/$', RedirectView.as_view(url='/about/contact',permanent=False)),

    url(r"^$", AboutView.as_view(), name='about'),
)
