from django.conf.urls import *

from univtop.apps.question.views import ListQuestionView

urlpatterns = patterns('univtop.apps.question.views',
    url(r"^$", ListQuestionView.as_view(), name='list_question'),
)
