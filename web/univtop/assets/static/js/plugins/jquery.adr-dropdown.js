'use strict';

(function ($) {
    /**
     * jQuery.ADRDropdown
     * @param options
     * @returns {$.fn.ADRDropdown}
     * @constructor
     */
    $.fn.ADRDropdown = function (options) {
        var defaultSettings = {
            textReplace: false
        };

        var settings = $.extend({},defaultSettings, options);

        this.each(function(){
            var $this = $(this),
                $window = $(window)
            ;

            $this.on('click',function(event){
                $this.toggleClass('open');

                var $target = $(event.target);
                if($.contains($this[0],event.target)){
                    var $text = $this.children('.text:first');
                    //check if it is a button
                    var $textButton = $text.children('.button:first');

                    if(settings.textReplace){
                        if($textButton.length > 0){
                            $textButton.html($target.text());
                        } else {
                            $text.html($target.text());
                        }
                    }
                } else {
                    $this.removeClass('open');
                }

                if($this.hasClass('open')){
                    setTimeout(function(){
                        $window.one('click',function(){
                            $this.removeClass('open');
                        });
                    });
                }
            });
        });

        return this;
    };

}(jQuery));
