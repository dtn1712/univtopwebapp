(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var ADRSandbox = require('./../core/sandbox'),
    ADRApplication = new scaleApp.Core(ADRSandbox)
    ;

ADRApplication.use(scaleApp.plugins.ls);
ADRApplication.use(scaleApp.plugins.util);
ADRApplication.use(scaleApp.plugins.submodule, {
    inherit: true,             // use all plugins from the parent's Core
    use: ['ls','submodule', 'util'],        // use some additional plugins
    useGlobalMediator: true,   // emit and receive all events from the parent's Core
});

module.exports = ADRApplication;
},{"./../core/sandbox":2}],2:[function(require,module,exports){
var ADRSandbox = function(core, instanceId, options, moduleId) {

    // define your API
    this.namespace = "ADR";

    // e.g. provide the Mediator methods 'on', 'emit', etc.
    core._mediator.installTo(this);

    // ... or define your custom communication methods
    this.myEmit = function(channel, data){
        core.emit(channel + '/' + instanceId, data);
    };

    // maybe you'd like to expose the instance ID
    this.id = instanceId;

    return this;
};

module.exports = ADRSandbox;
},{}],3:[function(require,module,exports){
'use strict';

(function ($) {
    /**
     * jQuery.ADRDropdown
     * @param options
     * @returns {$.fn.ADRDropdown}
     * @constructor
     */
    $.fn.ADRDropdown = function (options) {
        var defaultSettings = {
            textReplace: false
        };

        var settings = $.extend({},defaultSettings, options);

        this.each(function(){
            var $this = $(this),
                $window = $(window)
            ;

            $this.on('click',function(event){
                $this.toggleClass('open');

                var $target = $(event.target);
                if($.contains($this[0],event.target)){
                    var $text = $this.children('.text:first');
                    //check if it is a button
                    var $textButton = $text.children('.button:first');

                    if(settings.textReplace){
                        if($textButton.length > 0){
                            $textButton.html($target.text());
                        } else {
                            $text.html($target.text());
                        }
                    }
                } else {
                    $this.removeClass('open');
                }

                if($this.hasClass('open')){
                    setTimeout(function(){
                        $window.one('click',function(){
                            $this.removeClass('open');
                        });
                    });
                }
            });
        });

        return this;
    };

}(jQuery));

},{}],4:[function(require,module,exports){
/**
 * Some in-house plugins/libraries
 */
require('./includes/jquery.adr-dropdown');

//temporarily expose Application to global for debugging purpose
var Application = require('./core/application');

/**
 * register modules
 */

if ($('.adr.header').length) {
    Application.register('header', require('./modules/header'), data.header);
}

Application.register('footer', require('./modules/footer'));
Application.register('modal', require('./modules/modal'));
Application.register('window', require('./modules/window'));

if ($('.page-home').length) {
    Application.register('pageHome',require('./pages/home'));
}

if($('.page-contact').length){
    Application.register('pageContact', require('./pages/contact'));
}

if($('.page-questions').length){
    Application.register('pageQuestions', require('./pages/questions'));
}

Application.start();

window.UnivtopApplication = Application;
},{"./core/application":1,"./includes/jquery.adr-dropdown":3,"./modules/footer":5,"./modules/header":6,"./modules/modal":7,"./modules/window":8,"./pages/contact":9,"./pages/home":10,"./pages/questions":11}],5:[function(require,module,exports){
/**
 *  scripts for footer
 * @todo initFormNewsletter
 * @todo handleIconScrollToTop
 */
var footer = function (sandbox) {
    var _this = this;

    /**
     * @module footer/init
     * @param options
     */
    _this.init = function(options){
    };

    /**
     * @module footer/destroy
     */
    _this.destroy = function(){

    };

    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = footer;
},{}],6:[function(require,module,exports){
/**
 * @section header
 */
/**
 *  scripts for header
 * @todo initHeaderSegmentChooseLocation
 * @todo initPopupOrderTracking
 * @todo initPopupRegisterLogin
 * @todo initPopupCart
 * @todo initHeaderSegmentUserInfo (for already logged user)
 * @done initStickyHeader
 */
var header = function (sandbox) {
    var _this = this;

    /**
     * @module header
     * @function init
     * @param {Object} options
     */
    _this.init = function(options){

    };

    _this.destroy = function(){};


    return {
        init: _this.init,
        destroy: _this.destroy
    };
};

module.exports = header;
},{}],7:[function(require,module,exports){
'use strict';

var moduleModal = function(sandbox){
    var _this = this;

    _this.hideAllModals = function(){
        bootbox.hideAll();
    }

    _this.showModalLogin = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Log in',
                subtitle: 'Welcome back!',
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_login__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_login__form__input_username">Username</label>
                        <input type="text" id="modal_login__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_login__form__input_password">Password</label>
                        <input type="password" id="modal_login__form__input_password" name="password"/>
                        <div class="text-right"><a data-target="modal-forgot-password">Forgot your password?</a></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Log In!</button>
                    </div>
                    <div class="field">
                        <p>
                            Don't have an account?<br/>
                            <a data-target="modal-sign-up">Sign Up</a>
                        </p>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-login',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_login__form');

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                debug: true,
                rules: {
                    "username": "required",
                    "password": {
                        required: true
                    }
                },
                messages: {
                    username: "Please specify your name",
                    "password": {
                        required: "Please input your password"
                    }
                },
                errorContainer: '#modal_login__form .fields .field.error',
                errorLabelContainer: '#modal_login__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function() { alert("Submitted!") }

            });
        });
    };

    _this.showModalForgotPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_forgot_password__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_forgot_password__form__input_email">Type your email here</label>
                        <input type="text" id="modal_forgot_password__form__input_email" name="email"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    _this.showModalSignUp = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_sign_up__form">
                <div class="inline fields">
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_first_name">First Name</label>
                        <input type="text" id="modal_sign_up__form__input_first_name" name="firstName"/>
                    </div>
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_last_name">Last Name</label>
                        <input type="text" id="modal_sign_up__form__input_last_name" name="lastName"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_sign_up__form__input_email">Email</label>
                        <input type="text" id="modal_sign_up__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_username">Username</label>
                        <input type="text" id="modal_sign_up__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password">Password</label>
                        <input type="password" id="modal_sign_up__form__input_password" name="password"/>
                        <div class="text-right">Already on Univtop? <a data-target="modal-login">Sign In</a></div>
                    </div>
                </div>

                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Let's go!</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    sandbox.on('modal/login/show', function(){
        _this.hideAllModals();
        _this.showModalLogin();
    });

    sandbox.on('modal/forgotPassword/show', function(){
        _this.hideAllModals();
        _this.showModalForgotPassword();
    });

    sandbox.on('modal/signUp/show', function(){
        _this.hideAllModals();
        _this.showModalSignUp();
    });

    _this.init = function(data){
        _this.data = {};
        _this.data.titleTemplate = multiline(function(){/*!@preserve
             <div class="title">{{title}}</div>
             <div class="subtitle">{{subtitle}}</div>
         */console.log});
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleModal;
},{}],8:[function(require,module,exports){
'use strict';

var moduleWindow = function(sandbox){
    var _this = this;

    _this.init = function(data){
        var $body = $('body');

        $body
            .on('click', '[data-target=modal-login]', function(){
                sandbox.emit('modal/login/show');
            })
            .on('click', '[data-target=modal-register]', function(){
                sandbox.emit('modal/register/show');
            })
            .on('click', '[data-target=modal-forgot-password]', function(){
                sandbox.emit('modal/forgotPassword/show');
            })
            .on('click', '[data-target=modal-sign-up]', function(){
                sandbox.emit('modal/signUp/show');
            })
            .on('click', '.question [data-action="expand"]', function(event){
                event.preventDefault();

                var $this = $(this),
                    $currentQuestion = $this.closest('.question');

                if($currentQuestion.length){
                    $currentQuestion.attr('data-is-expanded', 1);
                    $this.addClass('hide');
                }
            });
        ;

    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleWindow;
},{}],9:[function(require,module,exports){
'use strict';

var pageContact = function(sandbox){
    var _this = this;

    _this.handleForm = function(){
        var $form = $('#segment_2__form'),
            $listErrors = $form.find('.group.error')
        ;

        $form.on('submit', function(event){
            event.preventDefault();
        });

        $form.validate({
            debug: true,
            rules: {
                "name": "required",
                "email": {
                    required: true,
                    email: true
                },
                "subject": "required",
                "message": {
                    required: true
                }
            },
            messages: {
                name: "Please specify your name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                "subject": "Please specify your subject",
                "message": "Please specify your message"
            },
            errorContainer: '#segment_2__form .group.error',
            errorLabelContainer: '#segment_2__form .group.error .list',
            errorElement: 'div',
            errorClass: 'item',
            //errorPlacement: function(error, element){
            //    $listErrors.html('');
            //    $('<div class="item"></div>').html($(error).text()).appendTo($listErrors);
            //    element.addClass('error');
            //},
            submitHandler: function() { alert("Submitted!") }

        });
    }

    _this.init = function(data){
        _this.handleForm();
    }
    _this.destroy = function(){}
}

module.exports = pageContact;
},{}],10:[function(require,module,exports){
/**
 * @module pageHome
 */
var pageHome = function(sandbox){
    var _this = this;

    /**
     * @module pageHome
     * @function init
     * @param options
     */
    _this.init = function(options){
        _this.objects = {};
        _this.objects.$segment1 = $('#segment_1');
        _this.objects.$segment2 = $('#segment_2');
        _this.objects.$segment1ToggleScrollDown = $('#segment_1__toggle_scroll_down');

        _this.handleAskForm();
        _this.handleSegment1();
    };

    /**
     * @module pageHome
     * @function destroy
     */
    _this.destroy = function(){}

    /**
     * @module pageHome
     * @function handleAskForm
     */
    _this.handleAskForm = function(){
        
    }

    _this.handleSegment1 = function(){
        _this.objects.$segment1.find('[data-toggle=scrollDown]').on('click', function(){
            $("html, body").animate({
                scrollTop: _this.objects.$segment2.offset().top
            }, 300);
        });
    }

    return ({
        init: _this.init,
        destroy: _this.destroy
    })
};

module.exports = pageHome;
},{}],11:[function(require,module,exports){
'use strict';

var pageQuestions = function(sandbox){
    var _this = this;

    _this.handleFormSearch = function(){
        var $input = $('#form_ask_question__input');

        var inputMagicSuggest = $input.magicSuggest({
            data: [
                {
                    name: 'Question 1',
                    href: '/pages/contact-us.html'
                },
                {
                    name: 'Question 2',
                    href: '/pages/question.html'
                }
            ],
            hideTrigger: true,
            selectFirst: true,
            placeholder: '',
            renderer: function(data){
                return '<a href="'+data.href+'">'+data.name+'</a>';
            }
        });

        $(inputMagicSuggest).on('selectionchange', function(){
            var selectedItems = this.getSelection();
            if(selectedItems.length){
                var selectedItem = selectedItems[0];
                if(selectedItem.href){
                    window.location.assign(selectedItem.href);
                }
            }
        });
    }

    _this.handleTabs = function(){
        var $menu = $('#segment_2__menu'),
            $tabs = $('#segment_2__tabs')
        ;

        $menu.on('click', '>.item', function(){
            var $this = $(this),
                $target = $($this.data('target'))
            ;

            $this
                .addClass('active')
                .siblings('.active')
                .removeClass('active')
            ;

            if($target.length){
                $target
                    .addClass('active')
                    .siblings('.active')
                    .removeClass('active')
                ;
            }
        });
    }

    _this.init = function(data){
        _this.handleFormSearch();
        _this.handleTabs();
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = pageQuestions;
},{}]},{},[4])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9jb3JlL2FwcGxpY2F0aW9uLmpzIiwic291cmNlL3NjcmlwdHMvY29yZS9zYW5kYm94LmpzIiwic291cmNlL3NjcmlwdHMvaW5jbHVkZXMvanF1ZXJ5LmFkci1kcm9wZG93bi5qcyIsInNvdXJjZS9zY3JpcHRzL2luZGV4LmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy9mb290ZXIuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2R1bGVzL2hlYWRlci5qcyIsInNvdXJjZS9zY3JpcHRzL21vZHVsZXMvbW9kYWwuanMiLCJzb3VyY2Uvc2NyaXB0cy9tb2R1bGVzL3dpbmRvdy5qcyIsInNvdXJjZS9zY3JpcHRzL3BhZ2VzL2NvbnRhY3QuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9ob21lLmpzIiwic291cmNlL3NjcmlwdHMvcGFnZXMvcXVlc3Rpb25zLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2REE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNsQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDakRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwidmFyIEFEUlNhbmRib3ggPSByZXF1aXJlKCcuLy4uL2NvcmUvc2FuZGJveCcpLFxyXG4gICAgQURSQXBwbGljYXRpb24gPSBuZXcgc2NhbGVBcHAuQ29yZShBRFJTYW5kYm94KVxyXG4gICAgO1xyXG5cclxuQURSQXBwbGljYXRpb24udXNlKHNjYWxlQXBwLnBsdWdpbnMubHMpO1xyXG5BRFJBcHBsaWNhdGlvbi51c2Uoc2NhbGVBcHAucGx1Z2lucy51dGlsKTtcclxuQURSQXBwbGljYXRpb24udXNlKHNjYWxlQXBwLnBsdWdpbnMuc3VibW9kdWxlLCB7XHJcbiAgICBpbmhlcml0OiB0cnVlLCAgICAgICAgICAgICAvLyB1c2UgYWxsIHBsdWdpbnMgZnJvbSB0aGUgcGFyZW50J3MgQ29yZVxyXG4gICAgdXNlOiBbJ2xzJywnc3VibW9kdWxlJywgJ3V0aWwnXSwgICAgICAgIC8vIHVzZSBzb21lIGFkZGl0aW9uYWwgcGx1Z2luc1xyXG4gICAgdXNlR2xvYmFsTWVkaWF0b3I6IHRydWUsICAgLy8gZW1pdCBhbmQgcmVjZWl2ZSBhbGwgZXZlbnRzIGZyb20gdGhlIHBhcmVudCdzIENvcmVcclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEFEUkFwcGxpY2F0aW9uOyIsInZhciBBRFJTYW5kYm94ID0gZnVuY3Rpb24oY29yZSwgaW5zdGFuY2VJZCwgb3B0aW9ucywgbW9kdWxlSWQpIHtcclxuXHJcbiAgICAvLyBkZWZpbmUgeW91ciBBUElcclxuICAgIHRoaXMubmFtZXNwYWNlID0gXCJBRFJcIjtcclxuXHJcbiAgICAvLyBlLmcuIHByb3ZpZGUgdGhlIE1lZGlhdG9yIG1ldGhvZHMgJ29uJywgJ2VtaXQnLCBldGMuXHJcbiAgICBjb3JlLl9tZWRpYXRvci5pbnN0YWxsVG8odGhpcyk7XHJcblxyXG4gICAgLy8gLi4uIG9yIGRlZmluZSB5b3VyIGN1c3RvbSBjb21tdW5pY2F0aW9uIG1ldGhvZHNcclxuICAgIHRoaXMubXlFbWl0ID0gZnVuY3Rpb24oY2hhbm5lbCwgZGF0YSl7XHJcbiAgICAgICAgY29yZS5lbWl0KGNoYW5uZWwgKyAnLycgKyBpbnN0YW5jZUlkLCBkYXRhKTtcclxuICAgIH07XHJcblxyXG4gICAgLy8gbWF5YmUgeW91J2QgbGlrZSB0byBleHBvc2UgdGhlIGluc3RhbmNlIElEXHJcbiAgICB0aGlzLmlkID0gaW5zdGFuY2VJZDtcclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQURSU2FuZGJveDsiLCIndXNlIHN0cmljdCc7XHJcblxyXG4oZnVuY3Rpb24gKCQpIHtcclxuICAgIC8qKlxyXG4gICAgICogalF1ZXJ5LkFEUkRyb3Bkb3duXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAgICogQHJldHVybnMgeyQuZm4uQURSRHJvcGRvd259XHJcbiAgICAgKiBAY29uc3RydWN0b3JcclxuICAgICAqL1xyXG4gICAgJC5mbi5BRFJEcm9wZG93biA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XHJcbiAgICAgICAgdmFyIGRlZmF1bHRTZXR0aW5ncyA9IHtcclxuICAgICAgICAgICAgdGV4dFJlcGxhY2U6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdmFyIHNldHRpbmdzID0gJC5leHRlbmQoe30sZGVmYXVsdFNldHRpbmdzLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgdGhpcy5lYWNoKGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAkd2luZG93ID0gJCh3aW5kb3cpXHJcbiAgICAgICAgICAgIDtcclxuXHJcbiAgICAgICAgICAgICR0aGlzLm9uKCdjbGljaycsZnVuY3Rpb24oZXZlbnQpe1xyXG4gICAgICAgICAgICAgICAgJHRoaXMudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgJHRhcmdldCA9ICQoZXZlbnQudGFyZ2V0KTtcclxuICAgICAgICAgICAgICAgIGlmKCQuY29udGFpbnMoJHRoaXNbMF0sZXZlbnQudGFyZ2V0KSl7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyICR0ZXh0ID0gJHRoaXMuY2hpbGRyZW4oJy50ZXh0OmZpcnN0Jyk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9jaGVjayBpZiBpdCBpcyBhIGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgIHZhciAkdGV4dEJ1dHRvbiA9ICR0ZXh0LmNoaWxkcmVuKCcuYnV0dG9uOmZpcnN0Jyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmKHNldHRpbmdzLnRleHRSZXBsYWNlKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoJHRleHRCdXR0b24ubGVuZ3RoID4gMCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkdGV4dEJ1dHRvbi5odG1sKCR0YXJnZXQudGV4dCgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICR0ZXh0Lmh0bWwoJHRhcmdldC50ZXh0KCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAkdGhpcy5yZW1vdmVDbGFzcygnb3BlbicpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmKCR0aGlzLmhhc0NsYXNzKCdvcGVuJykpe1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHdpbmRvdy5vbmUoJ2NsaWNrJyxmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHRoaXMucmVtb3ZlQ2xhc3MoJ29wZW4nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH07XHJcblxyXG59KGpRdWVyeSkpO1xyXG4iLCIvKipcclxuICogU29tZSBpbi1ob3VzZSBwbHVnaW5zL2xpYnJhcmllc1xyXG4gKi9cclxucmVxdWlyZSgnLi9pbmNsdWRlcy9qcXVlcnkuYWRyLWRyb3Bkb3duJyk7XHJcblxyXG4vL3RlbXBvcmFyaWx5IGV4cG9zZSBBcHBsaWNhdGlvbiB0byBnbG9iYWwgZm9yIGRlYnVnZ2luZyBwdXJwb3NlXHJcbnZhciBBcHBsaWNhdGlvbiA9IHJlcXVpcmUoJy4vY29yZS9hcHBsaWNhdGlvbicpO1xyXG5cclxuLyoqXHJcbiAqIHJlZ2lzdGVyIG1vZHVsZXNcclxuICovXHJcblxyXG5pZiAoJCgnLmFkci5oZWFkZXInKS5sZW5ndGgpIHtcclxuICAgIEFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdoZWFkZXInLCByZXF1aXJlKCcuL21vZHVsZXMvaGVhZGVyJyksIGRhdGEuaGVhZGVyKTtcclxufVxyXG5cclxuQXBwbGljYXRpb24ucmVnaXN0ZXIoJ2Zvb3RlcicsIHJlcXVpcmUoJy4vbW9kdWxlcy9mb290ZXInKSk7XHJcbkFwcGxpY2F0aW9uLnJlZ2lzdGVyKCdtb2RhbCcsIHJlcXVpcmUoJy4vbW9kdWxlcy9tb2RhbCcpKTtcclxuQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3dpbmRvdycsIHJlcXVpcmUoJy4vbW9kdWxlcy93aW5kb3cnKSk7XHJcblxyXG5pZiAoJCgnLnBhZ2UtaG9tZScpLmxlbmd0aCkge1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VIb21lJyxyZXF1aXJlKCcuL3BhZ2VzL2hvbWUnKSk7XHJcbn1cclxuXHJcbmlmKCQoJy5wYWdlLWNvbnRhY3QnKS5sZW5ndGgpe1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VDb250YWN0JywgcmVxdWlyZSgnLi9wYWdlcy9jb250YWN0JykpO1xyXG59XHJcblxyXG5pZigkKCcucGFnZS1xdWVzdGlvbnMnKS5sZW5ndGgpe1xyXG4gICAgQXBwbGljYXRpb24ucmVnaXN0ZXIoJ3BhZ2VRdWVzdGlvbnMnLCByZXF1aXJlKCcuL3BhZ2VzL3F1ZXN0aW9ucycpKTtcclxufVxyXG5cclxuQXBwbGljYXRpb24uc3RhcnQoKTtcclxuXHJcbndpbmRvdy5Vbml2dG9wQXBwbGljYXRpb24gPSBBcHBsaWNhdGlvbjsiLCIvKipcclxuICogIHNjcmlwdHMgZm9yIGZvb3RlclxyXG4gKiBAdG9kbyBpbml0Rm9ybU5ld3NsZXR0ZXJcclxuICogQHRvZG8gaGFuZGxlSWNvblNjcm9sbFRvVG9wXHJcbiAqL1xyXG52YXIgZm9vdGVyID0gZnVuY3Rpb24gKHNhbmRib3gpIHtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIGZvb3Rlci9pbml0XHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcbiAgICB9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQG1vZHVsZSBmb290ZXIvZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9O1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBmb290ZXI7IiwiLyoqXHJcbiAqIEBzZWN0aW9uIGhlYWRlclxyXG4gKi9cclxuLyoqXHJcbiAqICBzY3JpcHRzIGZvciBoZWFkZXJcclxuICogQHRvZG8gaW5pdEhlYWRlclNlZ21lbnRDaG9vc2VMb2NhdGlvblxyXG4gKiBAdG9kbyBpbml0UG9wdXBPcmRlclRyYWNraW5nXHJcbiAqIEB0b2RvIGluaXRQb3B1cFJlZ2lzdGVyTG9naW5cclxuICogQHRvZG8gaW5pdFBvcHVwQ2FydFxyXG4gKiBAdG9kbyBpbml0SGVhZGVyU2VnbWVudFVzZXJJbmZvIChmb3IgYWxyZWFkeSBsb2dnZWQgdXNlcilcclxuICogQGRvbmUgaW5pdFN0aWNreUhlYWRlclxyXG4gKi9cclxudmFyIGhlYWRlciA9IGZ1bmN0aW9uIChzYW5kYm94KSB7XHJcbiAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQG1vZHVsZSBoZWFkZXJcclxuICAgICAqIEBmdW5jdGlvbiBpbml0XHJcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9O1xyXG5cclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXHJcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxyXG4gICAgfTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gaGVhZGVyOyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBtb2R1bGVNb2RhbCA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5oaWRlQWxsTW9kYWxzID0gZnVuY3Rpb24oKXtcclxuICAgICAgICBib290Ym94LmhpZGVBbGwoKTtcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5zaG93TW9kYWxMb2dpbiA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIHRpdGxlID0gc3dpZy5yZW5kZXIoX3RoaXMuZGF0YS50aXRsZVRlbXBsYXRlLCB7bG9jYWxzOiB7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogJ0xvZyBpbicsXHJcbiAgICAgICAgICAgICAgICBzdWJ0aXRsZTogJ1dlbGNvbWUgYmFjayEnLFxyXG4gICAgICAgICAgICB9fSksXHJcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGlkPVwibW9kYWxfbG9naW5fX2Zvcm1cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF91c2VybmFtZVwiPlVzZXJuYW1lPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9sb2dpbl9fZm9ybV9faW5wdXRfdXNlcm5hbWVcIiBuYW1lPVwidXNlcm5hbWVcIi8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9sb2dpbl9fZm9ybV9faW5wdXRfcGFzc3dvcmRcIj5QYXNzd29yZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX2xvZ2luX19mb3JtX19pbnB1dF9wYXNzd29yZFwiIG5hbWU9XCJwYXNzd29yZFwiLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtcmlnaHRcIj48YSBkYXRhLXRhcmdldD1cIm1vZGFsLWZvcmdvdC1wYXNzd29yZFwiPkZvcmdvdCB5b3VyIHBhc3N3b3JkPzwvYT48L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdFwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaW5saW5lIGZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVwidW5pdnRvcCBidXR0b25cIiB0eXBlPVwic3VibWl0XCI+TG9nIEluITwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIERvbid0IGhhdmUgYW4gYWNjb3VudD88YnIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1zaWduLXVwXCI+U2lnbiBVcDwvYT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1sb2dpbicsXHJcbiAgICAgICAgICAgIHRpdGxlOiB0aXRsZSxcclxuICAgICAgICAgICAgbWVzc2FnZTogbWVzc2FnZSxcclxuICAgICAgICAgICAgb25Fc2NhcGU6IHRydWUsXHJcbiAgICAgICAgICAgIGJhY2tkcm9wOiB0cnVlXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRkaWFsb2cub24oJ3Nob3duLmJzLm1vZGFsJywgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgdmFyICRmb3JtID0gJGRpYWxvZy5maW5kKCcjbW9kYWxfbG9naW5fX2Zvcm0nKTtcclxuXHJcbiAgICAgICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICRmb3JtLnZhbGlkYXRlKHtcclxuICAgICAgICAgICAgICAgIGRlYnVnOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgcnVsZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICBcInVzZXJuYW1lXCI6IFwicmVxdWlyZWRcIixcclxuICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbWVzc2FnZXM6IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VybmFtZTogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG5hbWVcIixcclxuICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiUGxlYXNlIGlucHV0IHlvdXIgcGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNtb2RhbF9sb2dpbl9fZm9ybSAuZmllbGRzIC5maWVsZC5lcnJvcicsXHJcbiAgICAgICAgICAgICAgICBlcnJvckxhYmVsQ29udGFpbmVyOiAnI21vZGFsX2xvZ2luX19mb3JtIC5maWVsZHMgLmZpZWxkLmVycm9yIC5saXN0JyxcclxuICAgICAgICAgICAgICAgIGVycm9yRWxlbWVudDogJ2RpdicsXHJcbiAgICAgICAgICAgICAgICBlcnJvckNsYXNzOiAnaXRlbScsXHJcblxyXG4gICAgICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oKSB7IGFsZXJ0KFwiU3VibWl0dGVkIVwiKSB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3RoaXMuc2hvd01vZGFsRm9yZ290UGFzc3dvcmQgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciB0aXRsZSA9IHN3aWcucmVuZGVyKF90aGlzLmRhdGEudGl0bGVUZW1wbGF0ZSwge2xvY2Fsczoge1xyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdGb3Jnb3QgeW91ciBwYXNzd29yZD8nXHJcbiAgICAgICAgICAgIH19KSxcclxuICAgICAgICAgICAgbWVzc2FnZVRlbXBsYXRlID0gbXVsdGlsaW5lKGZ1bmN0aW9uKCl7LyohQHByZXNlcnZlXHJcbiAgICAgICAgICAgIDxmb3JtIGNsYXNzPVwidW5pdnRvcCBmb3JtXCIgaWQ9XCJtb2RhbF9mb3Jnb3RfcGFzc3dvcmRfX2Zvcm1cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybV9faW5wdXRfZW1haWxcIj5UeXBlIHlvdXIgZW1haWwgaGVyZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwibW9kYWxfZm9yZ290X3Bhc3N3b3JkX19mb3JtX19pbnB1dF9lbWFpbFwiIG5hbWU9XCJlbWFpbFwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJlcnJvciBmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGlzdFwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkIHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJ1bml2dG9wIGJ1dHRvblwiIHR5cGU9XCJzdWJtaXRcIj5SZXNldCBQYXNzd29yZDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1mb3Jnb3QtcGFzc3dvcmQnLFxyXG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIG9uRXNjYXBlOiB0cnVlLFxyXG4gICAgICAgICAgICBiYWNrZHJvcDogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICRkaWFsb2cuZmluZCgnI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybScpO1xyXG5cclxuICAgICAgICAgICAgJGZvcm0ub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBhbGVydCgyMjIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgX3RoaXMuc2hvd01vZGFsU2lnblVwID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgdGl0bGUgPSBzd2lnLnJlbmRlcihfdGhpcy5kYXRhLnRpdGxlVGVtcGxhdGUsIHtsb2NhbHM6IHtcclxuICAgICAgICAgICAgICAgIHRpdGxlOiAnRm9yZ290IHlvdXIgcGFzc3dvcmQ/J1xyXG4gICAgICAgICAgICB9fSksXHJcbiAgICAgICAgICAgIG1lc3NhZ2VUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICA8Zm9ybSBjbGFzcz1cInVuaXZ0b3AgZm9ybVwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybVwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImlubGluZSBmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2l4IHdpZGUgZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2ZpcnN0X25hbWVcIj5GaXJzdCBOYW1lPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9maXJzdF9uYW1lXCIgbmFtZT1cImZpcnN0TmFtZVwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic2l4IHdpZGUgZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X2xhc3RfbmFtZVwiPkxhc3QgTmFtZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGlkPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfbGFzdF9uYW1lXCIgbmFtZT1cImxhc3ROYW1lXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9lbWFpbFwiPkVtYWlsPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJtb2RhbF9zaWduX3VwX19mb3JtX19pbnB1dF9lbWFpbFwiIG5hbWU9XCJlbWFpbFwiLz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3VzZXJuYW1lXCI+VXNlcm5hbWU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3VzZXJuYW1lXCIgbmFtZT1cInVzZXJuYW1lXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwibW9kYWxfc2lnbl91cF9fZm9ybV9faW5wdXRfcGFzc3dvcmRcIj5QYXNzd29yZDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwicGFzc3dvcmRcIiBpZD1cIm1vZGFsX3NpZ25fdXBfX2Zvcm1fX2lucHV0X3Bhc3N3b3JkXCIgbmFtZT1cInBhc3N3b3JkXCIvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1yaWdodFwiPkFscmVhZHkgb24gVW5pdnRvcD8gPGEgZGF0YS10YXJnZXQ9XCJtb2RhbC1sb2dpblwiPlNpZ24gSW48L2E+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImVycm9yIGZpZWxkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsaXN0XCI+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJmaWVsZHNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmllbGRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cInVuaXZ0b3AgYnV0dG9uXCIgdHlwZT1cInN1Ym1pdFwiPkxldCdzIGdvITwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgKi9jb25zb2xlLmxvZ30pLFxyXG4gICAgICAgICAgICBtZXNzYWdlID0gc3dpZy5yZW5kZXIobWVzc2FnZVRlbXBsYXRlKTtcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIHZhciAkZGlhbG9nID0gYm9vdGJveC5kaWFsb2coe1xyXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdtb2RhbC1mb3Jnb3QtcGFzc3dvcmQnLFxyXG4gICAgICAgICAgICB0aXRsZTogdGl0bGUsXHJcbiAgICAgICAgICAgIG1lc3NhZ2U6IG1lc3NhZ2UsXHJcbiAgICAgICAgICAgIG9uRXNjYXBlOiB0cnVlLFxyXG4gICAgICAgICAgICBiYWNrZHJvcDogdHJ1ZVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkZGlhbG9nLm9uKCdzaG93bi5icy5tb2RhbCcsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkZm9ybSA9ICRkaWFsb2cuZmluZCgnI21vZGFsX2ZvcmdvdF9wYXNzd29yZF9fZm9ybScpO1xyXG5cclxuICAgICAgICAgICAgJGZvcm0ub24oJ3N1Ym1pdCcsIGZ1bmN0aW9uKGV2ZW50KXtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBhbGVydCgyMjIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgc2FuZGJveC5vbignbW9kYWwvbG9naW4vc2hvdycsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgX3RoaXMuaGlkZUFsbE1vZGFscygpO1xyXG4gICAgICAgIF90aGlzLnNob3dNb2RhbExvZ2luKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBzYW5kYm94Lm9uKCdtb2RhbC9mb3Jnb3RQYXNzd29yZC9zaG93JywgZnVuY3Rpb24oKXtcclxuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XHJcbiAgICAgICAgX3RoaXMuc2hvd01vZGFsRm9yZ290UGFzc3dvcmQoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIHNhbmRib3gub24oJ21vZGFsL3NpZ25VcC9zaG93JywgZnVuY3Rpb24oKXtcclxuICAgICAgICBfdGhpcy5oaWRlQWxsTW9kYWxzKCk7XHJcbiAgICAgICAgX3RoaXMuc2hvd01vZGFsU2lnblVwKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgX3RoaXMuZGF0YSA9IHt9O1xyXG4gICAgICAgIF90aGlzLmRhdGEudGl0bGVUZW1wbGF0ZSA9IG11bHRpbGluZShmdW5jdGlvbigpey8qIUBwcmVzZXJ2ZVxyXG4gICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRpdGxlXCI+e3t0aXRsZX19PC9kaXY+XHJcbiAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3VidGl0bGVcIj57e3N1YnRpdGxlfX08L2Rpdj5cclxuICAgICAgICAgKi9jb25zb2xlLmxvZ30pO1xyXG4gICAgfVxyXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gbW9kdWxlTW9kYWw7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIG1vZHVsZVdpbmRvdyA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgdmFyICRib2R5ID0gJCgnYm9keScpO1xyXG5cclxuICAgICAgICAkYm9keVxyXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJ1tkYXRhLXRhcmdldD1tb2RhbC1sb2dpbl0nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9sb2dpbi9zaG93Jyk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbignY2xpY2snLCAnW2RhdGEtdGFyZ2V0PW1vZGFsLXJlZ2lzdGVyXScsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ21vZGFsL3JlZ2lzdGVyL3Nob3cnKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtZm9yZ290LXBhc3N3b3JkXScsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzYW5kYm94LmVtaXQoJ21vZGFsL2ZvcmdvdFBhc3N3b3JkL3Nob3cnKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdjbGljaycsICdbZGF0YS10YXJnZXQ9bW9kYWwtc2lnbi11cF0nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgc2FuZGJveC5lbWl0KCdtb2RhbC9zaWduVXAvc2hvdycpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAub24oJ2NsaWNrJywgJy5xdWVzdGlvbiBbZGF0YS1hY3Rpb249XCJleHBhbmRcIl0nLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgJGN1cnJlbnRRdWVzdGlvbiA9ICR0aGlzLmNsb3Nlc3QoJy5xdWVzdGlvbicpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKCRjdXJyZW50UXVlc3Rpb24ubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICAgICAkY3VycmVudFF1ZXN0aW9uLmF0dHIoJ2RhdGEtaXMtZXhwYW5kZWQnLCAxKTtcclxuICAgICAgICAgICAgICAgICAgICAkdGhpcy5hZGRDbGFzcygnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICA7XHJcblxyXG4gICAgfVxyXG4gICAgX3RoaXMuZGVzdHJveSA9IGZ1bmN0aW9uKCl7fVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaW5pdDogX3RoaXMuaW5pdCxcclxuICAgICAgICBkZXN0cm95OiBfdGhpcy5kZXN0cm95XHJcbiAgICB9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gbW9kdWxlV2luZG93OyIsIid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBwYWdlQ29udGFjdCA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICBfdGhpcy5oYW5kbGVGb3JtID0gZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgJGZvcm0gPSAkKCcjc2VnbWVudF8yX19mb3JtJyksXHJcbiAgICAgICAgICAgICRsaXN0RXJyb3JzID0gJGZvcm0uZmluZCgnLmdyb3VwLmVycm9yJylcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgICRmb3JtLm9uKCdzdWJtaXQnLCBmdW5jdGlvbihldmVudCl7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICRmb3JtLnZhbGlkYXRlKHtcclxuICAgICAgICAgICAgZGVidWc6IHRydWUsXHJcbiAgICAgICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgICAgICBcIm5hbWVcIjogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJlbWFpbFwiOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgZW1haWw6IHRydWVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBcInN1YmplY3RcIjogXCJyZXF1aXJlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJtZXNzYWdlXCI6IHtcclxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBtZXNzYWdlczoge1xyXG4gICAgICAgICAgICAgICAgbmFtZTogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG5hbWVcIixcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IFwiV2UgbmVlZCB5b3VyIGVtYWlsIGFkZHJlc3MgdG8gY29udGFjdCB5b3VcIixcclxuICAgICAgICAgICAgICAgICAgICBlbWFpbDogXCJZb3VyIGVtYWlsIGFkZHJlc3MgbXVzdCBiZSBpbiB0aGUgZm9ybWF0IG9mIG5hbWVAZG9tYWluLmNvbVwiXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgXCJzdWJqZWN0XCI6IFwiUGxlYXNlIHNwZWNpZnkgeW91ciBzdWJqZWN0XCIsXHJcbiAgICAgICAgICAgICAgICBcIm1lc3NhZ2VcIjogXCJQbGVhc2Ugc3BlY2lmeSB5b3VyIG1lc3NhZ2VcIlxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvckNvbnRhaW5lcjogJyNzZWdtZW50XzJfX2Zvcm0gLmdyb3VwLmVycm9yJyxcclxuICAgICAgICAgICAgZXJyb3JMYWJlbENvbnRhaW5lcjogJyNzZWdtZW50XzJfX2Zvcm0gLmdyb3VwLmVycm9yIC5saXN0JyxcclxuICAgICAgICAgICAgZXJyb3JFbGVtZW50OiAnZGl2JyxcclxuICAgICAgICAgICAgZXJyb3JDbGFzczogJ2l0ZW0nLFxyXG4gICAgICAgICAgICAvL2Vycm9yUGxhY2VtZW50OiBmdW5jdGlvbihlcnJvciwgZWxlbWVudCl7XHJcbiAgICAgICAgICAgIC8vICAgICRsaXN0RXJyb3JzLmh0bWwoJycpO1xyXG4gICAgICAgICAgICAvLyAgICAkKCc8ZGl2IGNsYXNzPVwiaXRlbVwiPjwvZGl2PicpLmh0bWwoJChlcnJvcikudGV4dCgpKS5hcHBlbmRUbygkbGlzdEVycm9ycyk7XHJcbiAgICAgICAgICAgIC8vICAgIGVsZW1lbnQuYWRkQ2xhc3MoJ2Vycm9yJyk7XHJcbiAgICAgICAgICAgIC8vfSxcclxuICAgICAgICAgICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24oKSB7IGFsZXJ0KFwiU3VibWl0dGVkIVwiKSB9XHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIF90aGlzLmluaXQgPSBmdW5jdGlvbihkYXRhKXtcclxuICAgICAgICBfdGhpcy5oYW5kbGVGb3JtKCk7XHJcbiAgICB9XHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gcGFnZUNvbnRhY3Q7IiwiLyoqXHJcbiAqIEBtb2R1bGUgcGFnZUhvbWVcclxuICovXHJcbnZhciBwYWdlSG9tZSA9IGZ1bmN0aW9uKHNhbmRib3gpe1xyXG4gICAgdmFyIF90aGlzID0gdGhpcztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEBtb2R1bGUgcGFnZUhvbWVcclxuICAgICAqIEBmdW5jdGlvbiBpbml0XHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uc1xyXG4gICAgICovXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24ob3B0aW9ucyl7XHJcbiAgICAgICAgX3RoaXMub2JqZWN0cyA9IHt9O1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQxID0gJCgnI3NlZ21lbnRfMScpO1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQyID0gJCgnI3NlZ21lbnRfMicpO1xyXG4gICAgICAgIF90aGlzLm9iamVjdHMuJHNlZ21lbnQxVG9nZ2xlU2Nyb2xsRG93biA9ICQoJyNzZWdtZW50XzFfX3RvZ2dsZV9zY3JvbGxfZG93bicpO1xyXG5cclxuICAgICAgICBfdGhpcy5oYW5kbGVBc2tGb3JtKCk7XHJcbiAgICAgICAgX3RoaXMuaGFuZGxlU2VnbWVudDEoKTtcclxuICAgIH07XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIHBhZ2VIb21lXHJcbiAgICAgKiBAZnVuY3Rpb24gZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5kZXN0cm95ID0gZnVuY3Rpb24oKXt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAbW9kdWxlIHBhZ2VIb21lXHJcbiAgICAgKiBAZnVuY3Rpb24gaGFuZGxlQXNrRm9ybVxyXG4gICAgICovXHJcbiAgICBfdGhpcy5oYW5kbGVBc2tGb3JtID0gZnVuY3Rpb24oKXtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5oYW5kbGVTZWdtZW50MSA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgX3RoaXMub2JqZWN0cy4kc2VnbWVudDEuZmluZCgnW2RhdGEtdG9nZ2xlPXNjcm9sbERvd25dJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICBzY3JvbGxUb3A6IF90aGlzLm9iamVjdHMuJHNlZ21lbnQyLm9mZnNldCgpLnRvcFxyXG4gICAgICAgICAgICB9LCAzMDApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoe1xyXG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXHJcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxyXG4gICAgfSlcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gcGFnZUhvbWU7IiwiJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHBhZ2VRdWVzdGlvbnMgPSBmdW5jdGlvbihzYW5kYm94KXtcclxuICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcblxyXG4gICAgX3RoaXMuaGFuZGxlRm9ybVNlYXJjaCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyICRpbnB1dCA9ICQoJyNmb3JtX2Fza19xdWVzdGlvbl9faW5wdXQnKTtcclxuXHJcbiAgICAgICAgdmFyIGlucHV0TWFnaWNTdWdnZXN0ID0gJGlucHV0Lm1hZ2ljU3VnZ2VzdCh7XHJcbiAgICAgICAgICAgIGRhdGE6IFtcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnUXVlc3Rpb24gMScsXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZjogJy9wYWdlcy9jb250YWN0LXVzLmh0bWwnXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdRdWVzdGlvbiAyJyxcclxuICAgICAgICAgICAgICAgICAgICBocmVmOiAnL3BhZ2VzL3F1ZXN0aW9uLmh0bWwnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgIGhpZGVUcmlnZ2VyOiB0cnVlLFxyXG4gICAgICAgICAgICBzZWxlY3RGaXJzdDogdHJ1ZSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6ICcnLFxyXG4gICAgICAgICAgICByZW5kZXJlcjogZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJzxhIGhyZWY9XCInK2RhdGEuaHJlZisnXCI+JytkYXRhLm5hbWUrJzwvYT4nO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICQoaW5wdXRNYWdpY1N1Z2dlc3QpLm9uKCdzZWxlY3Rpb25jaGFuZ2UnLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICB2YXIgc2VsZWN0ZWRJdGVtcyA9IHRoaXMuZ2V0U2VsZWN0aW9uKCk7XHJcbiAgICAgICAgICAgIGlmKHNlbGVjdGVkSXRlbXMubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgIHZhciBzZWxlY3RlZEl0ZW0gPSBzZWxlY3RlZEl0ZW1zWzBdO1xyXG4gICAgICAgICAgICAgICAgaWYoc2VsZWN0ZWRJdGVtLmhyZWYpe1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5hc3NpZ24oc2VsZWN0ZWRJdGVtLmhyZWYpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgX3RoaXMuaGFuZGxlVGFicyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyICRtZW51ID0gJCgnI3NlZ21lbnRfMl9fbWVudScpLFxyXG4gICAgICAgICAgICAkdGFicyA9ICQoJyNzZWdtZW50XzJfX3RhYnMnKVxyXG4gICAgICAgIDtcclxuXHJcbiAgICAgICAgJG1lbnUub24oJ2NsaWNrJywgJz4uaXRlbScsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAkdGFyZ2V0ID0gJCgkdGhpcy5kYXRhKCd0YXJnZXQnKSlcclxuICAgICAgICAgICAgO1xyXG5cclxuICAgICAgICAgICAgJHRoaXNcclxuICAgICAgICAgICAgICAgIC5hZGRDbGFzcygnYWN0aXZlJylcclxuICAgICAgICAgICAgICAgIC5zaWJsaW5ncygnLmFjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgIDtcclxuXHJcbiAgICAgICAgICAgIGlmKCR0YXJnZXQubGVuZ3RoKXtcclxuICAgICAgICAgICAgICAgICR0YXJnZXRcclxuICAgICAgICAgICAgICAgICAgICAuYWRkQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICAgICAgLnNpYmxpbmdzKCcuYWN0aXZlJylcclxuICAgICAgICAgICAgICAgICAgICAucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpXHJcbiAgICAgICAgICAgICAgICA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBfdGhpcy5pbml0ID0gZnVuY3Rpb24oZGF0YSl7XHJcbiAgICAgICAgX3RoaXMuaGFuZGxlRm9ybVNlYXJjaCgpO1xyXG4gICAgICAgIF90aGlzLmhhbmRsZVRhYnMoKTtcclxuICAgIH1cclxuICAgIF90aGlzLmRlc3Ryb3kgPSBmdW5jdGlvbigpe31cclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIGluaXQ6IF90aGlzLmluaXQsXHJcbiAgICAgICAgZGVzdHJveTogX3RoaXMuZGVzdHJveVxyXG4gICAgfVxyXG59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHBhZ2VRdWVzdGlvbnM7Il19
$(function(){
	
})