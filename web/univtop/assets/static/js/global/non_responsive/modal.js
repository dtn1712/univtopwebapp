'use strict';

var moduleModal = function(sandbox){
    var _this = this;

    _this.hideAllModals = function(){
        bootbox.hideAll();
    }

    _this.showModalLogin = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Log in',
                subtitle: 'Welcome back!',
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_login__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_login__form__input_username">Username</label>
                        <input type="text" id="modal_login__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_login__form__input_password">Password</label>
                        <input type="password" id="modal_login__form__input_password" name="password"/>
                        <div class="text-right"><a data-target="modal-forgot-password">Forgot your password?</a></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Log In!</button>
                    </div>
                    <div class="field">
                        <p>
                            Don't have an account?<br/>
                            <a data-target="modal-sign-up">Sign Up</a>
                        </p>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-login',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_login__form');

            $form.on('submit', function(event){
                event.preventDefault();
            });

            $form.validate({
                debug: true,
                rules: {
                    "username": "required",
                    "password": {
                        required: true
                    }
                },
                messages: {
                    username: "Please specify your name",
                    "password": {
                        required: "Please input your password"
                    }
                },
                errorContainer: '#modal_login__form .fields .field.error',
                errorLabelContainer: '#modal_login__form .fields .field.error .list',
                errorElement: 'div',
                errorClass: 'item',

                submitHandler: function() { alert("Submitted!") }

            });
        });
    };

    _this.showModalForgotPassword = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_forgot_password__form">
                <div class="fields">
                    <div class="field">
                        <label for="modal_forgot_password__form__input_email">Type your email here</label>
                        <input type="text" id="modal_forgot_password__form__input_email" name="email"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field text-center">
                        <button class="univtop button" type="submit">Reset Password</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    _this.showModalSignUp = function(){
        var title = swig.render(_this.data.titleTemplate, {locals: {
                title: 'Forgot your password?'
            }}),
            messageTemplate = multiline(function(){/*!@preserve
            <form class="univtop form" id="modal_sign_up__form">
                <div class="inline fields">
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_first_name">First Name</label>
                        <input type="text" id="modal_sign_up__form__input_first_name" name="firstName"/>
                    </div>
                    <div class="six wide field">
                        <label for="modal_sign_up__form__input_last_name">Last Name</label>
                        <input type="text" id="modal_sign_up__form__input_last_name" name="lastName"/>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label for="modal_sign_up__form__input_email">Email</label>
                        <input type="text" id="modal_sign_up__form__input_email" name="email"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_username">Username</label>
                        <input type="text" id="modal_sign_up__form__input_username" name="username"/>
                    </div>
                    <div class="field">
                        <label for="modal_sign_up__form__input_password">Password</label>
                        <input type="password" id="modal_sign_up__form__input_password" name="password"/>
                        <div class="text-right">Already on Univtop? <a data-target="modal-login">Sign In</a></div>
                    </div>
                </div>

                <div class="fields">
                    <div class="error field">
                        <div class="list"></div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <button class="univtop button" type="submit">Let's go!</button>
                    </div>
                </div>
            </form>
            */console.log}),
            message = swig.render(messageTemplate);
        ;

        var $dialog = bootbox.dialog({
            className: 'modal-forgot-password',
            title: title,
            message: message,
            onEscape: true,
            backdrop: true
        });

        $dialog.on('shown.bs.modal', function(){
            var $form = $dialog.find('#modal_forgot_password__form');

            $form.on('submit', function(event){
                event.preventDefault();
                alert(222);
            });
        });
    };

    sandbox.on('modal/login/show', function(){
        _this.hideAllModals();
        _this.showModalLogin();
    });

    sandbox.on('modal/forgotPassword/show', function(){
        _this.hideAllModals();
        _this.showModalForgotPassword();
    });

    sandbox.on('modal/signUp/show', function(){
        _this.hideAllModals();
        _this.showModalSignUp();
    });

    _this.init = function(data){
        _this.data = {};
        _this.data.titleTemplate = multiline(function(){/*!@preserve
             <div class="title">{{title}}</div>
             <div class="subtitle">{{subtitle}}</div>
         */console.log});
    }
    _this.destroy = function(){}

    return {
        init: _this.init,
        destroy: _this.destroy
    }
}

module.exports = moduleModal;