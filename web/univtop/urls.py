from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView

from univtop.apps.main.views import DownloadView
from univtop.apps.member.views import ProfileView

from univtop import settings 


urlpatterns = patterns('',
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),

    # Auth
    # url(r'^account/', include("univtop.apps.auth.urls")),
    # url(r'^oauth2/', include('provider.oauth2.urls', namespace = 'oauth2')),

    # Haystack app URL
    url(r'^search/', include("univtop.apps.search.urls")),

    # univtop URL
    url(r'^about/', include('univtop.apps.about.urls')),
    url(r'^question/', include('univtop.apps.question.urls')),
    url(r'^download$', DownloadView.as_view(), name='download'),

    url(r'^user/(?P<username>\w+)',ProfileView.as_view(),name='profile'),
    url(r'^user/(?P<username>\w+)/',include('univtop.apps.member.urls')),

    # Media URL
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),

    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^$', include('univtop.apps.main.urls')),

)
