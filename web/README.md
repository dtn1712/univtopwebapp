# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version: 1.0

### Before Setup ###

* OS: Unix-based (Linux and/or OS X). No Windows support.

* Required libraries: 
    + [python-pip](https://pypi.python.org/pypi/pip)
        - Linux: `sudo apt-get install python-pip`.
        - Mac: `sudo easy_install pip`.
    + [virtualenv](https://virtualenv.pypa.io/en/latest/): `sudo pip install virtualenv`.

* Configuration: View directory `univtop/config`. There are common configurations for all stages and separate configuration for each stage (dev and prod).  

* Dependencies: View folder `reqs`. There are common dependencies and also dependencies for dev stage and prod stage.

### Setup ###

1. Create virtual environment for the project: `virtualenv venv-univtop`.

2. Source the virtual environment: `source venv-univtop/bin/activate`.

3. Install dependencies: `pip install -r reqs/dev.txt`. 

    **Note**: You might need to install python-dev and libxml to be able to install dependency lxml.

    + Linux: `sudo apt-get install libxml2-dev libxslt1-dev python-dev zlib1g-dev libssl-dev libmemcached-dev`.
    + Mac: `brew install python libxml2 libmemcached`.
        - If you encounter error " 'libmemcached/memcached.h' file not found ", try to install pylibmc separately: `pip install pylibmc --install-option="--with-libmemcached=/usr/local/Cellar/libmemcached/<version_number>/"`.
        - If you encounter error " 'libxml/xmlversion.h' file not found ", try `xcode-select --install` to install or upgrade command line tool for Xcode.

    Use Google if you failed to install some dependencies. The problems are often just missing some library packages.

4. Setup database.
    + Create `dev` folder in `univtop/db` if it is not created yet (`mkdir univtop/db/dev`). 
    + Create the database: `python manage.py syncdb`.
    + Run the migration: `python manage.py migrate allauth.socialaccount && python manage.py migrate allauth.socialaccount.providers.facebook &&  python manage.py migrate provider.oauth2   &&  python manage.py migrate djcelery && python manage.py migrate tastypie && python manage.py migrate kombu.transport.django  && python manage.py migrate push_notifications  && python manage.py migrate main`. 

For the migrate command, you can view the "migration" line in `Procfile`.

5. Build the configuration: `python build.py dev`. The `build.py` script is written to ease the process of adding support script. It basically run all the script file in the folder `univtop/scripts`. You can always add your own script in the file CATALOGUE. Currently, the scripts contain code to build the final `settings.py` file (combine `common.py` configuration and the stage configuration file), and also script for dev ops and minimize static file.

6. Install elastic search. Download and follow the instruction to run it here. It is pretty straightforward: https://www.elastic.co/downloads/elasticsearch

7. Run the server.
    + For local dev, `python manage.py runserver`.
    + For beta testing, push it to heroku.
    + For prod, follow [this guide](http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/).

8. Set up push notification file for iOS

Set up push notification, create Pem (certificate file) for iOS
http://www.raywenderlich.com/32960/apple-push-notification-services-in-ios-6-tutorial-part-1

Maintaining and signing key
https://developer.apple.com/library/ios/documentation/IDEs/Conceptual/AppDistributionGuide/MaintainingCertificates/MaintainingCertificates.html

Make unencrypted pem file.
 openssl pkcs12 -in cert.p12 -out cert.pem  -nodes 

pass for pass phrase : univtop4success
9. Run elastic in local host to be able for searching
/usr/local/Cellar/elasticsearch/1.5.0/bin/elasticsearch ( depends on your elastic version)

10. Rebuild index for elastic search
python manage.py rebuild_index

11. Run redis ( autocomplete service - important feature such as autocomplete)
cd redis-3.0.1/src
./redis-server

12. Run Celery in local ( deliver cron task service - important feature: sending email , search) :
celery -A univtop worker -l info

13. South
Auto database
python manage.py schemamigration main --auto
Delete ghost migration
python manage.py migrate main --delete-ghost-migrations

14. 
psql --host=<DB instance endpoint> --port=<port> --username=<master user name> --password --dbname=<database name>
type to command line: psql --host=univtop.cwhcne9ckyew.us-west-1.rds.amazonaws.com --port=5432 --username=univtop_db_admin --password --dbname=univtop
pass: univtop4success

15.
When pull the code in the server, make the running file executable by: sudo chmod +x -R bin/